{
    Edit Lyrics form of Nocturnal Player

    Nocturnal Player is a freeware audio player for Linux with focus on 
    simplicity and functionality that an audiophile may need in everyday usage.
    Copyright (C) 2015-2016 NoSt

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
}

unit Edit_Lyrics_Form;

{$mode objfpc}{$H+}

interface

uses
  Classes, ComCtrls, Controls, Dialogs, FileUtil, Forms, Graphics,
  NocturnalButton, StdCtrls, SysUtils;

type

  { TEditLyricsForm }

  TEditLyricsForm = class(TForm)
    FileList: TListView;
    LyricsMemo: TMemo;
    OKButton: TNocturnalButton;
    CancelButton: TNocturnalButton;
    ApplyButton: TNocturnalButton;
    procedure ApplyButtonClick(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure FileListSelectItem(Sender: TObject; Item: TListItem;
      {%H-}Selected: Boolean);
    procedure FormClose(Sender: TObject; var {%H-}CloseAction: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure LyricsMemoChange(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
    procedure UpdateLanguage;
    procedure UpdateTheme;
  private
    { private declarations }
    StoredLyrics: String;
    // Write the changed lyrics
    function WriteLyrics: Boolean;
  public
    { public declarations }
  end;

var
  EditLyricsForm: TEditLyricsForm;

implementation

uses
  AACfile, APEtag, FileNameConst, FLACfile, ID3v2, Lyrics_Form,
  Nocturnal_Language, Nocturnal_Playback, Nocturnal_Settings, Nocturnal_Themes,
  OggVorbis, Please_Wait_Form;

{$R *.lfm}

{ TEditLyricsForm }

// Write the changed lyrics
function TEditLyricsForm.WriteLyrics: Boolean;
var
  AACTag: TAACfile;
  APETag: TAPEtag;
  Count1: Integer;
  FLACTag: TFLACfile;
  ID3v2Tag: TID3v2;
  Msg: String;
  OGGVorbisTag: TOggVorbis;
  Written: Boolean;
begin
  Result := False;
  Written := True;
  with FileList do
       begin
         // Check if any of the tags has been changed
         for Count1 := 0 to Items.Count - 1 do
             if Items[Count1].SubItems[2] = '1' then
                begin
                  Application.ProcessMessages;
                  // Display the "Please Wait" form
                  Msg := GetMessageText('writelyrics') + ': ' +
                  Items[Count1].SubItems[1];
                  ShowPleaseWait(Msg, pbstNormal, Count1 + 1, Items.Count);
                  // Check the file extension
                  case UpperCase(ExtractFileExt(Items[Count1].SubItems[1])) of
                     '.AAC', '.M4A', '.MP4':
                               begin
                                 AACTag := TAACfile.Create;
                                 try
                                   AACTag.ReadFromFile(WideString(
                                   Items[Count1].SubItems[1]));
                                   // Store the lyrics
                                   AACTag.ID3v2.Lyric :=
                                   Items[Count1].SubItems[0];
                                   // Attempt to save the tags to the file
                                   if not AACTag.ID3v2.SaveToFile(
                                   Items[Count1].SubItems[1]) then
                                      begin
                                        ShowMessage(GetMessageText(
                                        'couldnotwritetofile') +
                                        Items[Count1].Caption);
                                        Written := Written and False
                                      end
                                   else
                                       begin
                                         StoredLyrics := '';
                                         Items[Count1].SubItems[2] := '';
                                         Written := Written and True
                                       end
                                 finally
                                   AACTag.Free
                                 end
                               end;
                     '.APE':
                               begin
                                 APETag := TAPEtag.Create;
                                 try
                                   APETag.ReadFromFile(WideString(
                                   Items[Count1].SubItems[1]));
                                   // Store the lyrics
                                   APETag.AppendField('LYRICS',
                                   Items[Count1].SubItems[0]);
                                   // Attempt to save the tags to the file
                                   if not APETag.WriteTagInFile(WideString(
                                   Items[Count1].SubItems[1])) then
                                      begin
                                        ShowMessage(GetMessageText(
                                        'couldnotwritetofile') +
                                        Items[Count1].Caption);
                                        Written := Written and False
                                      end
                                   else
                                       begin
                                         StoredLyrics := '';
                                         Items[Count1].SubItems[2] := '';
                                         Written := Written and True
                                       end
                                 finally
                                   APETag.Free
                                 end
                               end;
                     '.FLAC':
                               begin
                                 FLACTag := TFLACfile.Create;
                                 try
                                   FLACTag.ReadFromFile(
                                   Items[Count1].SubItems[1]);
                                   // Store the lyrics
                                   FLACTag.Lyrics := Items[Count1].SubItems[0];
                                   // Attempt to save the tags to the file
                                   if not FLACTag.SaveToFile(
                                   Items[Count1].SubItems[1]) then
                                      begin
                                        ShowMessage(GetMessageText(
                                        'couldnotwritetofile') +
                                        Items[Count1].Caption);
                                        Written := Written and False
                                      end
                                   else
                                       begin
                                         StoredLyrics := '';
                                         Items[Count1].SubItems[2] := '';
                                         Written := Written and True
                                       end
                                 finally
                                   FLACTag.Free
                                 end
                               end;
                     '.MP3':
                               begin
                                 ID3v2Tag := TID3v2.Create;
                                 try
                                   ID3v2Tag.ReadFromFile(
                                   Items[Count1].SubItems[1]);
                                   // Store the lyrics
                                   ID3v2Tag.Lyric := Items[Count1].SubItems[0];
                                   // Attempt to save the tags to the file
                                   if not ID3v2Tag.SaveToFile(
                                   Items[Count1].SubItems[1]) then
                                      begin
                                        ShowMessage(GetMessageText(
                                        'couldnotwritetofile') +
                                        Items[Count1].Caption);
                                        Written := Written and False
                                      end
                                   else
                                       begin
                                         StoredLyrics := '';
                                         Items[Count1].SubItems[2] := '';
                                         Written := Written and True
                                       end
                                 finally
                                   ID3v2Tag.Free
                                 end
                               end;
                     '.OGG':
                               begin
                                 OGGVorbisTag := TOggVorbis.Create;
                                 try
                                   OGGVorbisTag.ReadFromFile(WideString(
                                   Items[Count1].SubItems[1]));
                                   // Store the lyrics
                                   OGGVorbisTag.Lyrics :=
                                   Items[Count1].SubItems[0];
                                   // Attempt to save the tags to the file
                                   if not OGGVorbisTag.SaveTag(WideString(
                                   Items[Count1].SubItems[1])) then
                                      begin
                                        ShowMessage(GetMessageText(
                                        'couldnotwritetofile') +
                                        Items[Count1].Caption);
                                        Written := Written and False
                                      end
                                   else
                                       begin
                                         StoredLyrics := '';
                                         Items[Count1].SubItems[2] := '';
                                         Written := Written and True
                                       end
                                 finally
                                   OGGVorbisTag.Free
                                 end
                               end
                     end
                end
       end;
  if Written then
     LyricsForm.LyricsMemo.Text := FileList.Items[Count1].SubItems[0];
  Result := Written;
  // Hide the "Please Wait" form
  if PleaseWaitForm.Visible then
     PleaseWaitForm.Hide
end;

// On clicking "Apply" button
procedure TEditLyricsForm.ApplyButtonClick(Sender: TObject);
begin
  if WriteLyrics then
     ApplyButton.Enabled := False
end;

// On clicking "Cancel" button
procedure TEditLyricsForm.CancelButtonClick(Sender: TObject);
var
  Cap, Msg: String;
begin
  if ApplyButton.Enabled then
     begin
       Cap := GetMessageCaption('discardchanges');
       Msg := GetMessageText('discardchanges');
       if MessageDlg(Cap, Msg, mtWarning, [mbYes, mbNo], 0) = mrYes then
          EditLyricsForm.Close
     end
  else
      EditLyricsForm.Close
end;

// On selecting a filelist item
procedure TEditLyricsForm.FileListSelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
begin
  LyricsMemo.Text := GetLyrics(Item.SubItems[1]);
  StoredLyrics := LyricsMemo.Text;
  LyricsMemo.Enabled := True
end;

// On closing "Edit Lyrics" form
procedure TEditLyricsForm.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
  // On close write the position and the size of the form into the settings file
  if Height <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'EDIT_LYRICS_FORM_HEIGHT'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'EDIT_LYRICS_FORM_HEIGHT',
     IntToStr(Height));
  if Left <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'EDIT_LYRICS_FORM_LEFT'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'EDIT_LYRICS_FORM_LEFT',
     IntToStr(Left));
  if Top <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'EDIT_LYRICS_FORM_TOP'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'EDIT_LYRICS_FORM_TOP',
     IntToStr(Top));
  if Width <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'EDIT_LYRICS_FORM_WIDTH'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'EDIT_LYRICS_FORM_WIDTH',
     IntToStr(Width))
end;

// On showing the form
procedure TEditLyricsForm.FormShow(Sender: TObject);
var
  ELHeight, ELLeft, ELTop, ELWidth: Integer;
begin
  // Load Lyrics form display settings
  ELHeight := StrToIntDef(ReadFromSettingsFile(
  DefaultSettingsFileName, 'EDIT_LYRICS_FORM_HEIGHT'), 0);
  ELLeft := StrToIntDef(ReadFromSettingsFile(
  DefaultSettingsFileName, 'EDIT_LYRICS_FORM_LEFT'), 0);
  ELTop := StrToIntDef(ReadFromSettingsFile(
  DefaultSettingsFileName, 'EDIT_LYRICS_FORM_TOP'), 0);
  ELWidth := StrToIntDef(ReadFromSettingsFile(
  DefaultSettingsFileName, 'EDIT_LYRICS_FORM_WIDTH'), 0);
  if ELHeight <> 0 then
     Height := ELHeight;
  if ELLeft <> 0 then
     Left := ELLeft;
  if ELTop <> 0 then
     Top := ELTop;
  if ELWidth <> 0 then
     Width := ELWidth;
  UpdateLanguage;
  UpdateTheme;
  LyricsMemo.Clear;
  LyricsMemo.Enabled := False
end;

// On changing the lyrics
procedure TEditLyricsForm.LyricsMemoChange(Sender: TObject);
begin
  if LyricsMemo.Text <> StoredLyrics then
     begin
       FileList.Selected.SubItems[0] := LyricsMemo.Text;
       FileList.Selected.SubItems[2] := '1';
       ApplyButton.Enabled := True
     end
  else
      begin
        FileList.Selected.SubItems[0] := LyricsMemo.Text;
        FileList.Selected.SubItems[2] := '';
        ApplyButton.Enabled := False
      end
end;

// On clicking "OK" button
procedure TEditLyricsForm.OKButtonClick(Sender: TObject);
var
  Cap, Msg: String;
begin
  if ApplyButton.Enabled then
     begin
       Cap := GetMessageCaption('applychanges');
       Msg := GetMessageText('applychanges');
       if MessageDlg(Cap, Msg, mtWarning, [mbYes, mbNo], 0) = mrYes then
          begin
            if WriteLyrics then
               begin
                 ApplyButton.Enabled := False;
                 EditLyricsForm.Close
               end
          end
     end
  else EditLyricsForm.Close
end;

// Update the interface language
procedure TEditLyricsForm.UpdateLanguage;
begin
  Caption := GetFormCaption('edit_lyrics_form');
  ApplyButton.Caption := GetCommonControlCaption('applybutton');
  CancelButton.Caption := GetCommonControlCaption('cancelbutton');
  OKButton.Caption := GetCommonControlCaption('okbutton')
end;

// Update the interface theme
procedure TEditLyricsForm.UpdateTheme;
begin
  Color := AppTheme.Colour1;
  if AppTheme.FontName <> '' then
    Font.Name := AppTheme.FontName
  else
    Font.Name := 'default';
  Font.Color := AppTheme.FontColour1;
  ApplyButton.BackgroundColor := AppTheme.Colour1;
  ApplyButton.Color := AppTheme.Colour2;
  ApplyButton.Font.Color := AppTheme.FontColour2;
  ApplyButton.Font.Name := Font.Name;
  ApplyButton.HoverColor := AppTheme.Colour1;
  ApplyButton.HoverFontColor := AppTheme.FontColour1;
  CancelButton.BackgroundColor := AppTheme.Colour1;
  CancelButton.Color := AppTheme.Colour2;
  CancelButton.Font.Color := AppTheme.FontColour2;
  CancelButton.Font.Name := Font.Name;
  CancelButton.HoverColor := AppTheme.Colour1;
  CancelButton.HoverFontColor := AppTheme.FontColour1;
  FileList.Color := AppTheme.Colour2;
  FileList.Font.Color := AppTheme.FontColour2;
  FileList.Font.Name := Font.Name;
  LyricsMemo.Color := AppTheme.Colour2;
  LyricsMemo.Font.Color := AppTheme.FontColour2;
  LyricsMemo.Font.Name := Font.Name;
  OKButton.BackgroundColor := AppTheme.Colour1;
  OKButton.Color := AppTheme.Colour2;
  OKButton.Font.Color := AppTheme.FontColour2;
  OKButton.Font.Name := Font.Name;
  OKButton.HoverColor := AppTheme.Colour1;
  OKButton.HoverFontColor := AppTheme.FontColour1
end;

end.

