{
    About form of Nocturnal Player

    Nocturnal Player is a freeware audio player for Linux with focus on 
    simplicity and functionality that an audiophile may need in everyday usage.
    Copyright (C) 2015-2017 NoSt

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
}

unit About_Form;

{$mode objfpc}{$H+}

interface

uses
  Classes, Controls, Dialogs, FileUtil, Forms, Graphics, StdCtrls, SysUtils;

type

  { TAboutForm }

  TAboutForm = class(TForm)
    AppNameLabel: TLabel;
    CreatedByLabel: TLabel;
    BASSLabel: TLabel;
    ID3v2Label: TLabel;
    Label1: TLabel;
    TagsLabel: TLabel;
    UsesLabel: TLabel;
    LastUpdateLabel: TLabel;
    procedure FormClose(Sender: TObject; var {%H-}CloseAction: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure UpdateLanguage;
    procedure UpdateTheme;
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  AboutForm: TAboutForm;

implementation

uses
  FileNameConst, Main_Form, Nocturnal_Language, Nocturnal_Settings,
  Nocturnal_Themes;

{$R *.lfm}

{ TAboutForm }

// On closing the About form
procedure TAboutForm.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  // On close write the position and the size of the form into the settings file
  if Height <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'ABOUT_FORM_HEIGHT'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'ABOUT_FORM_HEIGHT',
     IntToStr(Height));
  if Left <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'ABOUT_FORM_LEFT'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'ABOUT_FORM_LEFT',
     IntToStr(Left));
  if Top <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'ABOUT_FORM_TOP'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'ABOUT_FORM_TOP',
     IntToStr(Top));
  if Width <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'ABOUT_FORM_WIDTH'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'ABOUT_FORM_WIDTH',
     IntToStr(Width))
end;

// On showing the About form
procedure TAboutForm.FormShow(Sender: TObject);
var
  AHeight, ALeft, ATop, AWidth: Integer;
begin
  // Load About form display settings
  AHeight := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'ABOUT_FORM_HEIGHT'), 0);
  ALeft := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'ABOUT_FORM_LEFT'), 0);
  ATop := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'ABOUT_FORM_TOP'), 0);
  AWidth := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'ABOUT_FORM_WIDTH'), 0);
  if AHeight <> 0 then
     Height := AHeight;
  if ALeft <> 0 then
     Left := ALeft;
  if ATop <> 0 then
     Top := ATop;
  if AWidth <> 0 then
     Width := AWidth;
  UpdateLanguage;
  UpdateTheme
end;

// Update the interface language
procedure TAboutForm.UpdateLanguage;
begin
  Caption := GetFormCaption('about_form');
  AppNameLabel.Caption := MainForm.Caption;
  CreatedByLabel.Caption := GetFormControlText('about_form', 'createdbylabel',
  'caption') + ' NoSt (g0atfly@gmail.com)';
  LastUpdateLabel.Caption := GetFormControlText('about_form', 'lastupdatelabel',
  'caption') + ' 23.05.2018';
  UsesLabel.Caption := GetFormControlText('about_form', 'useslabel', 'caption')
end;

// Update the interface theme
procedure TAboutForm.UpdateTheme;
begin
  Color := AppTheme.Colour1;
  Font.Color := AppTheme.FontColour1;
  if Font.Name <> '' then
     Font.Name := AppTheme.FontName
  else
    Font.Name := 'default'
end;

end.

