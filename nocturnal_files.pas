{
    Files-related utilities for Nocturnal Player

    Nocturnal Player is a freeware audio player for Linux with focus on
    simplicity and functionality that an audiophile may need in everyday usage.
    Copyright (C) 2015-2016 NoSt

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
}

unit Nocturnal_Files;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

// Change the invalid characters in the file name to the set character
// FileName - the name of the file
// ReplacementChar - the character to replace invalid characters with
function ChangeInvalidChar(FileName: String;
  ReplacementChar: Char): String;
// Decode the renaming pattern
// Tags - values of the file tags
// RenamingPattern - the pattern of renaming the file
// FileName - the name of the file
function DecodeRenamingPattern(const Tags: TStrings;
  RenamingPattern: String; FileName: String): String;
// Transliteration of the unicode file name
// FileName - the name of the file
function FileNameTransliteration(const FileName: String): String;
// Get the file path only
// FullFileName - the name of the file including the path
function GetFilePath(const FullFileName: String): String;
// Get the file name only
// FullFileName - the name of the file including the path
function GetShortFileName(const FullFileName: String): String;
// Trims the file name to 254 characters
// FileName - the name of the file
function TrimFileName(const FileName: String): String;

implementation

uses
  LazUTF8;

// Change the invalid characters in the file name to the set character
// FileName - the name of the file
// ReplacementChar - the character to replace invalid characters with
function ChangeInvalidChar(FileName: String;
  ReplacementChar: Char): String;
var
  Count1: Byte;
  Count2: Integer;
const
  InvalidCharsNumber = 13;
  InvalidChars: Array[1..InvalidCharsNumber] of Char = ('/', '\', '?', '<', '>', '*', '|' ,'[',
  ']', '{', '}', '"', ':');
begin
  if ReplacementChar <> '' then
     // If invalid replacement character is specified, character "_" is used
     for Count1 := 1 to InvalidCharsNumber do
        if ReplacementChar = InvalidChars[Count1] then
           begin
             ReplacementChar := '_';
             Break
           end
  // If replacement character is not specified, character "_" is used too
  else
      ReplacementChar := '_';
  if FileName[1] = '-' then
     if ReplacementChar <> '-' then
        FileName[1] := ReplacementChar
     else
       FileName[1] := '_';
  // Check all the characters in the string
  for Count2 := 1 to UTF8Length(FileName) do
     // If invalid character is found, replace it and go to the next one
     for Count1 := 1 to InvalidCharsNumber do
        if FileName[Count2] = InvalidChars[Count1] then
           begin
             FileName[Count2] := ReplacementChar;
             Break
           end;
  Result := FileName
end;

// Decode the renaming pattern
// Tags - values of the file tags
// RenamingPattern - the pattern of renaming the file
// FileName - the name of the file
function DecodeRenamingPattern(const Tags: TStrings;
  RenamingPattern: String; FileName: String): String;
var
  CodeLength, CodePos, FileNameLength: Integer;
  CodeString: String;
  Count: Byte;
const
  Codes: Array[1..13] of String = ('%TITLE', '%SUBTITLE', '%ARTIST',
  '%ALBUMARTIST', '%ALBUM', '%YEAR', '%TRACK', '%DISC', '%GENRE', '%COMMENT',
  '%COMPOSER', '%COPYRIGHT', '%FILENAME');
begin
  // If the pattern is not empty
  if (RenamingPattern <> '') then
     begin
       // Check if any of the codes is present in the pattern
       for Count := 1 to 8 do
          begin
               CodePos := UTF8Pos(Codes[Count], RenamingPattern);
               // If present, remove the code and insert the corresponding value
               if CodePos > 0 then
                  begin
                    // Excluding file name and if tag is not empty
                    if (Count < 13) then
                       begin
                         CodeString := Tags[Count - 1]
                       end
                    else
                        begin
                             FileNameLength := UTF8Length(FileName) - 4;
                             CodeString := Copy(FileName, 0, FileNameLength)
                        end;
                    CodeLength := UTF8Length(Codes[Count]);
                    UTF8Delete(RenamingPattern, CodePos, CodeLength);
                    UTF8Insert(CodeString, RenamingPattern, CodePos)
                  end
          end;
       RenamingPattern := ChangeInvalidChar(RenamingPattern, '_');
       RenamingPattern := RenamingPattern + ExtractFileExt(FileName);
       if TrimFileName(RenamingPattern) <> '' then
          Result := TrimFileName(RenamingPattern)
       else
         Result := FileName
     end
  // Else display the file name
  else
    Result := FileName
end;

// Transliteration of the unicode file name
// FileName - the name of the file
function FileNameTransliteration(const FileName: String): String;
const
  CharCount = 129;
var
  Count1: Integer;
  Count2: Byte;
  TempFileName: String;
const
  CharsToChange: Array[1..CharCount] of String = ('А', 'Б', 'В', 'Г', 'Д', 'Е',
  'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У',
  'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ь', 'Ы', 'Ъ', 'Э', 'Ю', 'Я', 'а', 'б', 'в',
  'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р',
  'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ь', 'ы', 'ъ', 'э', 'ю', 'я',
  'À', 'Á', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í',
  'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý',
  'Þ', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì',
  'í', 'î', 'ï', 'ð', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü',
  'ý', 'þ', 'ÿ');
  CharsToWrite: Array[1..CharCount] of String = ('A', 'B', 'V', 'G', 'D', 'E',
  'Yo', 'Zh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U',
  'F', 'Kh', 'Ts', 'Ch', 'Sh', 'Sch', '''', 'Y', '', 'E','Yu', 'Ya', 'a', 'b',
  'v', 'g', 'd', 'e', 'yo', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p',
  'r', 's', 't', 'u', 'f', 'kh', 'ts', 'ch', 'sh', 'sch', '''', 'y', '', 'e',
  'yu', 'ya', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'Ae', 'C', 'E', 'E', 'E', 'E',
  'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U',
  'U', 'Y', 'P', 'ss', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e',
  'e', 'i', 'i', 'i', 'i', 'd', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u',
  'u', 'u', 'y', 'p', 'y');
begin
  TempFileName := FileName;
  for Count1 := 1 to UTF8Length(TempFileName) do
      for Count2 := 1 to CharCount do
          if UTF8Copy(TempFileName, Count1, 1) = CharsToChange[Count2] then
             begin
               UTF8Delete(TempFileName, Count1, 1);
               UTF8Insert(CharsToWrite[Count2], TempFileName, Count1)
             end;
  Result := TempFileName
end;

// Get the file path only
// FullFileName - the name of the file including the path
function GetFilePath(const FullFileName: String): String;
var
  Count: Integer;
begin
  Count := UTF8Length(FullFileName);
  while FullFileName[Count] <> '/' do
        Dec(Count);
  Result := Copy(FullFileName, 0, Count)
end;

// Get the file name only
// FullFileName - the name of the file including the path
function GetShortFileName(const FullFileName: String): String;
var
  Count: Integer;
begin
  if FullFileName <> '' then
     begin
       if UTF8Pos('CD Track #', FullFileName) = 0 then
          begin
            Count := Length(FullFileName);
            while FullFileName[Count] <> '/' do
              Dec(Count);
            Result :=
            Copy(FullFileName, Count + 1, Length(FullFileName) - Count)
          end
       else
         Result := FullFileName
     end
  else
      Result := ''
end;

// Trims the file name to 254 characters
// FileName - the name of the file
function TrimFileName(const FileName: String): String;
var
  Ext, TempFileName: String;
begin
  Result := '';
  // Trim the file name
  TempFileName := UTF8Trim(FileName);
  // Extract the file extension
  Ext := ExtractFileExt(TempFileName);
  // Delete the file extension
  UTF8Delete(TempFileName, UTF8Length(TempFileName) - UTF8Length(Ext) + 1,
  UTF8Length(Ext));
  // Remove the spaces or the dots in the beginning of the file name
  if (UTF8Length(TempFileName) > 1) then
     while ((TempFileName[1] = ' ') or (TempFileName[1] = '.')) and
     (UTF8Length(TempFileName) > 1) do
           UTF8Delete(TempFileName, 1, 1);
  // If the file name is longer than 254 characters, trim it
  if (UTF8Length(TempFileName) + UTF8Length(Ext)) > 254 then
     TempFileName := UTF8Copy(TempFileName, 1, 254 - UTF8Length(Ext)) + Ext
  else
      TempFileName := TempFileName + Ext;
  // If the resulting file name is valid, set it as the output
  if ((UTF8Length(TempFileName) - UTF8Length(Ext)) > 0) and
  (TempFileName[1] <> ' ') and (TempFileName[1] <> '.') then
     Result := TempFileName
end;

end.

