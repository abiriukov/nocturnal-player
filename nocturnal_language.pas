{
    Language-related utilities for Nocturnal Player

    Nocturnal Player is a freeware audio player for Linux with focus on
    simplicity and functionality that an audiophile may need in everyday usage.
    Copyright (C) 2015-2016 NoSt

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
}

unit Nocturnal_Language;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type
  // The language type
  TLanguage = record
    Code: String[3];
    Name: String;
    Author: String
  end;

// Get common control caption from the current language file
// Control - the control name
function GetCommonControlCaption(Control: String): String;

// Get form caption from the current language file
// Form - the form name
function GetFormCaption(Form: String): String;

// Get form control text from the current language file
// Form - the form name
// Control - the control name
// Attrib - the attribute
function GetFormControlText(Form, Control, Attrib: String): String;

// Get the language from its code
// Code - the code of the language consisting of three symbols
function GetLanguageFromCode(const Code: String): TLanguage;

// Gets the language from its name string
// Name - the name of the language
function GetLanguageFromName(const Name: String): TLanguage;

// Get message captionfrom the current language file
// Msg - the message
function GetMessageCaption(Msg: String): String;

// Get message text from the current language file
// Msg - the message
function GetMessageText(Msg: String): String;

// Get tag text from the current language file
// Tag - the tag
function GetTagText(Tag: String): String;

// Checks if a file is a valid XML language file and returns boolean result
// FileName - the name of the file
function IsValidXMLLanguageFile(const FileName: String): Boolean;

// Gets all the available language
procedure GetAllLanguages;

var
  // All the languages
  AllLanguages: Array of TLanguage;
  // Current language
  AppLanguage: TLanguage;

implementation

uses
  Dialogs, FileNameConst, LazUTF8, laz2_DOM, laz2_XMLRead, Math;

// Get common control caption from the current language file
// Control - the control name
function GetCommonControlCaption(Control: String): String;
var
  Doc: TXMLDocument;
  FileName: String;
  Node: TDOMNode;
begin
  Result := '';
  FileName := LangPath + LowerCase(AppLanguage.Code) + '.xml';
  if not FileExists(FileName) then
    Exit;
  Doc := TXMLDocument.Create;
  try
    ReadXMLFile(Doc, FileName);
    Node := Doc.DocumentElement.FindNode('common');
    if Node <> nil then
      Node := Node.FindNode(Control)
    else
      Exit;
    if (Node <> nil) and Node.HasAttributes then
      Node := Node.Attributes.GetNamedItem('caption')
    else
      Exit;
    if Node <> nil then
      Result := Node.NodeValue;
  finally
    Doc.Free
  end
end;

// Get form caption from the current language file
// Form - the form name
function GetFormCaption(Form: String): String;
var
  Doc: TXMLDocument;
  FileName: String;
  Node: TDOMNode;
begin
  Result := '';
  FileName := LangPath + LowerCase(AppLanguage.Code) + '.xml';
  if not FileExists(FileName) then
    Exit;
  Doc := TXMLDocument.Create;
  try
    ReadXMLFile(Doc, FileName);
    Node := Doc.DocumentElement.FindNode(Form);
    if (Node <> nil) and Node.HasAttributes then
      Node := Node.Attributes.GetNamedItem('caption')
    else
      Exit;
    if Node <> nil then
      Result := Node.NodeValue
  finally
    Doc.Free
  end
end;

// Get form control text from the current language file
// Form - the form name
// Control - the control name
// Attrib - the attribute
function GetFormControlText(Form, Control, Attrib: String): String;
var
  Doc: TXMLDocument;
  FileName: String;
  Node: TDOMNode;
begin
  Result := '';
  FileName := LangPath + LowerCase(AppLanguage.Code) + '.xml';
  if not FileExists(FileName) then
    Exit;
  Doc := TXMLDocument.Create;
  try
    ReadXMLFile(Doc, FileName);
    Node := Doc.DocumentElement.FindNode(Form);
    if (Node <> nil) then
      Node := Node.FindNode(Control)
    else
      Exit;
    if (Node <> nil) and Node.HasAttributes then
      Node := Node.Attributes.GetNamedItem(Attrib)
    else
      Exit;
    if (Node <> nil) then
      Result := Node.NodeValue
  finally
    Doc.Free
  end
end;

// Get the language from its code
// Code - the code of the language consisting of three symbols
function GetLanguageFromCode(const Code: String): TLanguage;
var
  Doc: TXMLDocument;
  FileName: String;
  Node: TDOMNode;
  TempLang: TLanguage;
begin
  FileName := LangPath + LowerCase(Code) + '.xml';
  if not FileExists(FileName) or not IsValidXMLLanguageFile(FileName) then
    Exit;
  Doc := TXMLDocument.Create;
  try
    ReadXMLFile(Doc, FileName);
    Node := Doc.FindNode('language');
    TempLang.Code := Node.Attributes.GetNamedItem('code').NodeValue;
    TempLang.Name := Node.Attributes.GetNamedItem('name').NodeValue;
    TempLang.Author := Node.Attributes.GetNamedItem('author').NodeValue;
    Result := TempLang
  finally
    Doc.Free
  end
end;

// Gets the language from its name string
// Name - the name of the language
function GetLanguageFromName(const Name: String): TLanguage;
var
  AuthorString, NameString, SearchString, TempString: String;
  Iterator, Len, Pos: Integer;
begin
  Result.Name := '';
  // Get selected language name and author
  TempString := Name;
  SearchString := Format(' (%s: ', [GetCommonControlCaption('author')]);
  Pos := UTF8Pos(SearchString, TempString) + UTF8Length(SearchString);
  Len := UTF8Length(TempString) - Pos;
  AuthorString := UTF8Copy(TempString, Pos, Len);
  Pos := UTF8Pos(SearchString, TempString);
  Len := UTF8Length(TempString) - Pos + 1;
  UTF8Delete(TempString, Pos, Len);
  NameString := TempString;
  // Get the corresponding language
  if Length(AllLanguages) > 0 then
     for Iterator := 0 to Length(AllLanguages) - 1 do
         if (AllLanguages[Iterator].Name = NameString) and
           (AllLanguages[Iterator].Author = AuthorString) then
              Result := AllLanguages[Iterator]
end;

// Get message captionfrom the current language file
// Msg - the message
function GetMessageCaption(Msg: String): String;
var
  Doc: TXMLDocument;
  FileName: String;
  Node: TDOMNode;
begin
  Result := '';
  FileName := LangPath + LowerCase(AppLanguage.Code) + '.xml';
  if not FileExists(FileName) then
    Exit;
  Doc := TXMLDocument.Create;
  try
    ReadXMLFile(Doc, FileName);
    Node := Doc.DocumentElement.FindNode('messages');
    if Node <> nil then
      Node := Node.FindNode(Msg)
    else
      Exit;
    if (Node <> nil) and Node.HasAttributes then
      Node := Node.Attributes.GetNamedItem('cap')
    else
      Exit;
    if Node <> nil then
      Result := Node.NodeValue
  finally
    Doc.Free
  end
end;

// Get message text from the current language file
// Msg - the message
function GetMessageText(Msg: String): String;
var
  Doc: TXMLDocument;
  FileName: String;
  Node: TDOMNode;
begin
  Result := '';
  FileName := LangPath + LowerCase(AppLanguage.Code) + '.xml';
  if not FileExists(FileName) then
    Exit;
  Doc := TXMLDocument.Create;
  try
    ReadXMLFile(Doc, FileName);
    Node := Doc.DocumentElement.FindNode('messages');
    if Node <> nil then
      Node := Node.FindNode(Msg)
    else
      Exit;
    if (Node <> nil) and Node.HasAttributes then
      Node := Node.Attributes.GetNamedItem('msg')
    else
      Exit;
    if Node <> nil then
      Result := Node.NodeValue
  finally
    Doc.Free
  end
end;

// Get tag text from the current language file
// Tag - the tag
function GetTagText(Tag: String): String;
var
  Doc: TXMLDocument;
  FileName: String;
  Node: TDOMNode;
begin
  Result := '';
  FileName := LangPath + LowerCase(AppLanguage.Code) + '.xml';
  if not FileExists(FileName) then
    Exit;
  Doc := TXMLDocument.Create;
  try
    ReadXMLFile(Doc, FileName);
    Node := Doc.DocumentElement.FindNode('tag_names');
    if Node <> nil then
      Node := Node.FindNode(Tag)
    else
      Exit;
    if Node <> nil then
      Result := Node.TextContent
  finally
    Doc.Free
  end
end;

// Checks if a file is a valid XML language file and returns boolean result
// FileName - the name of the file
function IsValidXMLLanguageFile(const FileName: String): Boolean;
const
  XMLString = '<?xml version="1.0"?>';
  LangString = '<language';
  EndLangString = '</language>';
var
  CharBuffer: Array[0..511] of Char;
  Count: Integer;
  InputStream: TFileStream;
  Iterator: Integer;
  TempString: String;
begin
  Result := False;
  if not FileExists(FileName) then
    Exit;
  InputStream := TFileStream.Create(FileName, fmOpenRead);
  try
    InputStream.Position := 0;
    TempString := '';
    while InputStream.Position < InputStream.Size do
      begin
        Count :=
        Min(SizeOf(CharBuffer), InputStream.Size - InputStream.Position);
        InputStream.ReadBuffer(CharBuffer, Count);
        for Iterator := 0 to Count - 1 do
            TempString += CharBuffer[Iterator]
      end;
    if (Copy(TempString, 0, Length(XMLString)) = XMLString) and
    (Pos(LangString, TempString) <> 0) and
    (Pos(EndLangString, TempString) <> 0) then
    Result := True
  finally
    InputStream.Free
  end
end;

// Gets all the available languages
procedure GetAllLanguages;
var
  Count: Integer;
  Doc: TXMLDocument;
  FileName: String;
  Node: TDOMNode;
  SearchRec: TSearchRec;
begin
  SetLength(AllLanguages, 0);
  Count := 0;
  if FindFirst(LangPath + '*.xml', faAnyFile and
  not faDirectory, SearchRec) = 0 then
     begin
       repeat
         FileName := LangPath + SearchRec.Name;
         if IsValidXMLLanguageFile(FileName) then
           begin
             Doc := TXMLDocument.Create;
             try
               ReadXMLFile(Doc, FileName);
               Node := Doc.DocumentElement;
               if (Node.NodeName = 'language') and Node.HasAttributes then
                 begin
                   Inc(Count);
                   SetLength(AllLanguages, Count);
                   AllLanguages[Count - 1].Code :=
                   Node.Attributes.GetNamedItem('code').NodeValue;
                   AllLanguages[Count - 1].Name :=
                   Node.Attributes.GetNamedItem('name').NodeValue;
                   AllLanguages[Count - 1].Author :=
                   Node.Attributes.GetNamedItem('author').NodeValue
                 end;
             finally
               Doc.Free
             end
           end
       until FindNext(SearchRec) <> 0;
       FindClose(SearchRec)
     end
end;

end.

