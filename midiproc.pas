{
    MIDI files processing utilities for Nocturnal Player

    Nocturnal Player is a freeware audio player for Linux with focus on
    simplicity and functionality that an audiophile may need in everyday usage.
    Copyright (C) 2015-2016 NoSt

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
}

unit MIDIProc;

{$mode objfpc}{$H+}

interface

uses
  BASS, BASSMIDI, Classes, SysUtils;

// Process the MIDI mark text
function ProcessMarkText(InputText, MarkText: String): String;
// MIDI synconization end
// Handle - syncronization handle
// Stream - playback stream
// Data - data to syncronize
// User - a pointer to the object where to store the data
procedure MIDIEndSync({%H-}Handle: HSYNC; {%H-}Stream: HSTREAM;
  {%H-}Data: DWORD; User: Pointer); cdecl;
// MIDI lyrics syncronization
// Handle - syncronization handle
// Stream - playback stream
// Data - data to syncronize
// User - a pointer to the object where to store the data
procedure MIDILyricSync({%H-}Handle: HSYNC; Stream: HSTREAM; Data: DWORD;
  User: Pointer); cdecl;
// MIDI text syncronization
// Handle - syncronization handle
// Stream - playback stream
// Data - data to syncronize
// User - a pointer to the object where to store the data
procedure MIDITextSync({%H-}Handle: HSYNC; Stream: HSTREAM; Data: DWORD;
  User: Pointer); cdecl;

implementation

uses
  LazUTF8, StdCtrls;

// Process the MIDI mark text
function ProcessMarkText(InputText, MarkText: String): String;
var
  TempStringList: TStringList;
begin
  case MarkText[1] of
    '@': Result := InputText;
    '\': Result := UTF8Copy(MarkText, 2, Pred(UTF8Length(MarkText)));
    '/':
        begin
         TempStringList  := TStringList.Create;
         try
           TempStringList.Text := InputText;
           TempStringList.Add(UTF8Copy(MarkText, 2,
           Pred(UTF8Length(MarkText))));
           while (TempStringList.Count > 3) do
             TempStringList.Delete(0);
           Result := Trim(TempStringList.Text)
         finally
           TempStringList.Free
         end
        end
  else
    Result := InputText + MarkText
  end
end;

// MIDI synconization end
// Handle - syncronization handle
// Stream - playback stream
// Data - data to syncronize
// User - a pointer to the object where to store the data
procedure MIDIEndSync(Handle: HSYNC; Stream: HSTREAM; Data: DWORD;
  User: Pointer); cdecl;
begin
  TMemo(User).Text := ''
end;

// MIDI lyrics syncronization
// Handle - syncronization handle
// Stream - playback stream
// Data - data to syncronize
// User - a pointer to the object where to store the data
procedure MIDILyricSync(Handle: HSYNC; Stream: HSTREAM; Data: DWORD;
  User: Pointer); cdecl;
var
  Mark: BASS_MIDI_MARK;
begin
  Mark.text := '';
  BASS_MIDI_StreamGetMark(Stream, BASS_SYNC_MIDI_LYRIC, Data, Mark);
  TMemo(User).Text  := ProcessMarkText(TMemo(User).Text, Mark.text)
end;

// MIDI text syncronization
// Handle - syncronization handle
// Stream - playback stream
// Data - data to syncronize
// User - a pointer to the object where to store the data
procedure MIDITextSync(Handle: HSYNC; Stream: HSTREAM; Data: DWORD;
  User: Pointer); cdecl;
var
  Mark: BASS_MIDI_MARK;
begin
  Mark.text := '';
  BASS_MIDI_StreamGetMark(Stream, BASS_SYNC_MIDI_TEXT, Data, Mark);
  TMemo(User).Text  := ProcessMarkText(TMemo(User).Text, Mark.text)
end;

end.

