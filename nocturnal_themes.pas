{
    Theme-related utilities for Nocturnal Player

    Nocturnal Player is a freeware audio player for Linux with focus on
    simplicity and functionality that an audiophile may need in everyday usage.
    Copyright (C) 2015-2016 NoSt

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
}

unit Nocturnal_Themes;

{$mode objfpc}{$H+}

interface

uses
    Graphics;

type
  // Theme type containing the colour settings
  TTheme = Record
      Name: String;
      Version: String;
      Author: String;
      Colour1: TColor;
      Colour2: TColor;
      FontName: String;
      FontColour1: TColor;
      FontColour2: TColor
  end;

// Gets the theme from its name string
// Name - the name of the theme
function GetThemeFromName(const Name: String): TTheme;

// Checks if a file is a valid XML theme file and returns boolean result
// FileName - the name of the file
function IsValidXMLThemeFile(const FileName: String): Boolean;

// Gets all the available themes
procedure GetAllThemes;

var
  // All the themes
  AllThemes: Array of TTheme;
  // Current theme
  AppTheme: TTheme;

implementation

uses
    Classes, FileNameConst, LazFileUtils, LazUTF8, LazUTF8Classes, laz2_DOM,
    laz2_XMLRead, Math, Nocturnal_Language, SysUtils;

// Gets the theme from its name string
// Name - the name of the theme
function GetThemeFromName(const Name: String): TTheme;
var
  AuthorString, NameString, SearchString, TempString, VersionString: String;
  Iterator, Len, Pos: Integer;
begin
  Result.Name := '';
  // Get selected theme name, version and author
  TempString := Name;
  SearchString := Format(' (%s: ', [GetCommonControlCaption('author')]);
  Pos := UTF8Pos(SearchString, TempString) + UTF8Length(SearchString);
  Len := UTF8Length(TempString) - Pos;
  AuthorString := UTF8Copy(TempString, Pos, Len);
  Pos := UTF8Pos(SearchString, TempString);
  Len := UTF8Length(TempString) - Pos + 1;
  UTF8Delete(TempString, Pos, Len);
  Pos := UTF8Pos(' v.', TempString) + UTF8Length(' v.');
  Len := UTF8Length(TempString) - Pos + 1;
  VersionString := UTF8Copy(TempString, Pos, Len);
  Pos := UTF8Pos(' v.', TempString);
  Len := UTF8Length(TempString) - Pos + 1;
  UTF8Delete(TempString, Pos, Len);
  NameString := TempString;
  // Get the corresponding theme
  if Length(AllThemes) > 0 then
     for Iterator := 0 to Length(AllThemes) - 1 do
         if (AllThemes[Iterator].Name = NameString) and
           (AllThemes[Iterator].Version = VersionString) and
           (AllThemes[Iterator].Author = AuthorString) then
              Result := AllThemes[Iterator]
end;

// Checks if a file is a valid XML theme file and returns boolean result
// FileName - the name of the file
function IsValidXMLThemeFile(const FileName: String): Boolean;
const
  XMLString = '<?xml version="1.0"?>';
  ThemesString = '<themes>';
  EndThemesString = '</themes>';
var
  CharBuffer: Array[0..511] of Char;
  Count, Iterator: Integer;
  InputStream: TFileStreamUTF8;
  TempString: String;
begin
  Result := False;
  if not FileExistsUTF8(FileName) then
    Exit;
  InputStream := TFileStreamUTF8.Create(FileName, fmOpenRead);
  try
    InputStream.Position := 0;
    TempString := '';
    while InputStream.Position < InputStream.Size do
      begin
        Count :=
        Min(SizeOf(CharBuffer), InputStream.Size - InputStream.Position);
        InputStream.ReadBuffer(CharBuffer{%H-}, Count);
        for Iterator := 0 to Count - 1 do
          TempString += CharBuffer[Iterator]
      end;
    if (Copy(TempString, 0, Length(XMLString)) = XMLString) and
    (Pos(ThemesString, TempString) <> 0) and
    (Pos(EndThemesString, TempString) <> 0) then
      Result := True;
  finally
    InputStream.Free
  end
end;

// Gets all the available themes
procedure GetAllThemes;
var
  Child, Node: TDOMNode;
  Count, Iterator1, Iterator2: Integer;
  Doc: TXMLDocument;
  FileName, NodeString: String;
  SearchRec: TSearchRec;
begin
  SetLength(AllThemes, 0);
  Count := 0;
  if FindFirstUTF8(ThemePath + '*.xml', faAnyFile and
  not faDirectory, SearchRec) = 0 then
     begin
       repeat
         FileName := ThemePath + SearchRec.Name;
         if IsValidXMLThemeFile(FileName) then
           begin
             Doc := TXMLDocument.Create;
             try
               ReadXMLFile(Doc, FileName);
               Node := Doc.DocumentElement;
               if (Node.NodeName = 'themes') and Node.HasChildNodes then
                 begin
                   for Iterator1 := 0 to Node.ChildNodes.Length - 1 do
                     begin
                       Child := Node.ChildNodes.Item[Iterator1];
                       if Child.HasAttributes then
                         begin
                           Inc(Count);
                           SetLength(AllThemes, Count);
                           for Iterator2 := 0 to Child.Attributes.Length - 1 do
                               begin
                                 NodeString :=
                                 Child.Attributes.Item[Iterator2].NodeValue;
                                 case Child.Attributes.Item[Iterator2].
                                 NodeName of
                                      'name':
                                        AllThemes[Count - 1].Name := NodeString;
                                      'version':
                                        AllThemes[Count - 1].Version :=
                                        NodeString;
                                      'author':
                                        AllThemes[Count - 1].Author :=
                                        NodeString;
                                      'colour1':
                                        AllThemes[Count - 1].Colour1 :=
                                        StringToColorDef(NodeString, clDefault);
                                      'colour2':
                                        AllThemes[Count - 1].Colour2 :=
                                        StringToColorDef(NodeString, clDefault);
                                      'fontname':
                                        AllThemes[Count - 1].FontName :=
                                        NodeString;
                                      'fontcolour1':
                                        AllThemes[Count - 1].FontColour1 :=
                                        StringToColorDef(NodeString, clDefault);
                                      'fontcolour2':
                                        AllThemes[Count - 1].FontColour2 :=
                                        StringToColorDef(NodeString, clDefault)
                                 end
                               end
                         end
                     end
                 end
             finally
               Doc.Free
             end
           end
       until FindNextUTF8(SearchRec) <> 0;
       FindCloseUTF8(SearchRec)
     end
end;

end.

