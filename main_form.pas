{
    Main form of Nocturnal Player

    Nocturnal Player is a freeware audio player for Linux with focus on 
    simplicity and functionality that an audiophile may need in everyday usage.
    Copyright (C) 2015-2017 NoSt

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
}

unit Main_Form; { TODO -oNoSt -cFeatures : Add internet playback availability }

{$mode objfpc}{$H+}

interface

uses
  BASS, BGRABitmap, BGRABitmapTypes, Classes, ComCtrls, Controls, Dialogs,
  ExtCtrls, FakeActiveX, Forms, Graphics, LazHelpHTML, LCLType, Menus,
  Nocturnal_Playback, Nocturnal_Settings, Nocturnal_Themes, NocturnalButton,
  NocturnalPlaybar, NocturnalPlaybackBar, NocturnalVolumebar,
  NocturnalStatusBar, StdCtrls, Buttons, SysUtils, URL_Input_Form, VirtualTrees;

type

  { TMainForm }

  TMainForm = class(TForm)
    AbtItem: TMenuItem;
    AddFilesDialog: TOpenDialog;
    AddFilesItem: TMenuItem;
    BackItem: TMenuItem;
    CollectionButton: TNocturnalArrowButton;
    CollectionTree: TVirtualStringTree;
    CoverImage: TImage;
    CPUUsageLabel: TLabel;
    EditLyricsItem: TMenuItem;
    EditTagsItem: TMenuItem;
    ExitItem: TMenuItem;
    FilesItem: TMenuItem;
    ForwardItem: TMenuItem;
    GetCDDBInfoItem: TMenuItem;
    HelpItem: TMenuItem;
    HTMLBrowserHelpViewer1: THTMLBrowserHelpViewer;
    HTMLHelpDatabase1: THTMLHelpDatabase;
    LoadPlaylistDialog: TOpenDialog;
    LoadPlaylistItem: TMenuItem;
    MainFormMenu: TMainMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    MenuItem10: TMenuItem;
    MenuItem11: TMenuItem;
    MenuItem12: TMenuItem;
    MenuItem13: TMenuItem;
    MuteUnmuteItem: TMenuItem;
    PlayCDButton: TNocturnalButton;
    LyricsButton: TNocturnalButton;
    RemoveFilesButton: TNocturnalMinusButton;
    AddFilesButton: TNocturnalPlusButton;
    StatusBar1: TNocturnalStatusBar;
    PlaybackBar: TNocturnalPlaybackBar;
    PlaybackItem: TMenuItem;
    PlaybackTimer: TTimer;
    Playbar: TNocturnalPlaybar;
    Playlist: TVirtualStringTree;
    PlaylistPopupMenu: TPopupMenu;
    PlayPauseItem: TMenuItem;
    PlaylistHeaderPopupMenu: TPopupMenu;
    ProgramTimer: TTimer;
    RemoveFilesItem: TMenuItem;
    RenameFilesItem: TMenuItem;
    SavePlaylistDialog: TSaveDialog;
    SavePlaylistItem: TMenuItem;
    SettingsItem: TMenuItem;
    StopItem: TMenuItem;
    VolumeBar: TNocturnalVolumebar;
    procedure AboutItemClick(Sender: TObject);
    procedure AddFilesButtonClick(Sender: TObject);
    procedure AddFilesItemClick(Sender: TObject);
    procedure AddURLButtonClick(Sender: TObject);
    procedure BackItemClick(Sender: TObject);
    procedure RemoveFilesButtonClick(Sender: TObject);
    procedure CollectionButtonClick(Sender: TObject);
    procedure CollectionTreeGetHint(Sender: TBaseVirtualTree;
      Node: PVirtualNode; {%H-}Column: TColumnIndex;
      var {%H-}LineBreakStyle: TVTTooltipLineBreakStyle; var HintText: String);
    procedure CollectionTreeGetText(Sender: TBaseVirtualTree;
      Node: PVirtualNode; {%H-}Column: TColumnIndex; {%H-}TextType: TVSTTextType;
      var CellText: String);
    procedure CollectionTreeInitChildren(Sender: TBaseVirtualTree;
      Node: PVirtualNode; var ChildCount: Cardinal);
    procedure CollectionTreeInitNode(Sender: TBaseVirtualTree; {%H-}ParentNode,
      Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
    procedure CoverImageMouseEnter(Sender: TObject);
    procedure CoverImageMouseLeave(Sender: TObject);
    procedure EditLyricsItemClick(Sender: TObject);
    procedure EditTagsItemClick(Sender: TObject);
    procedure ExitItemClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormDropFiles(Sender: TObject; const FileNames: array of String);
    procedure ForwardItemClick(Sender: TObject);
    procedure HelpItemClick(Sender: TObject);
    procedure LoadPlaylistItemClick(Sender: TObject);
    procedure LyricsButtonClick(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure MenuItem4Click(Sender: TObject);
    procedure MenuItem5Click(Sender: TObject);
    procedure MenuItem6Click(Sender: TObject);
    procedure MenuItem7Click(Sender: TObject);
    procedure MenuItem8Click(Sender: TObject);
    procedure MenuItem9Click(Sender: TObject);
    procedure MenuItem10Click(Sender: TObject);
    procedure MenuItem11Click(Sender: TObject);
    procedure MenuItem12Click(Sender: TObject);
    procedure MenuItem13Click(Sender: TObject);
    procedure MuteUnmuteItemClick(Sender: TObject);
    procedure PlaybarMuteUnmuteClick(Sender: TObject);
    procedure PlaybarNextClick(Sender: TObject);
    procedure PlaybarPlayPauseClick(Sender: TObject);
    procedure PlaybarPrevClick(Sender: TObject);
    procedure PlaybarStopClick(Sender: TObject);
    procedure PlaybackBarChange(Sender: TObject);
    procedure PlayCDButtonClick(Sender: TObject);
    procedure PlaybackTimerTimer(Sender: TObject);
    procedure PlaylistAfterCellPaint(Sender: TBaseVirtualTree;
      TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
      const CellRect: TRect);
    procedure PlaylistBeforeCellPaint(Sender: TBaseVirtualTree;
      TargetCanvas: TCanvas; {%H-}Node: PVirtualNode; {%H-}Column: TColumnIndex;
      {%H-}CellPaintMode: TVTCellPaintMode; CellRect: TRect; var {%H-}ContentRect: TRect);
    procedure PlaylistCompareNodes(Sender: TBaseVirtualTree; Node1,
      Node2: PVirtualNode; Column: TColumnIndex; var Result: Integer);
    procedure PlaylistDragDrop(Sender: TBaseVirtualTree; Source: TObject;
      {%H-}DataObject: IDataObject; {%H-}Formats: TFormatArray; {%H-}Shift: TShiftState;
      const {%H-}Pt: TPoint; var {%H-}Effect: LongWord; {%H-}Mode: TDropMode);
    procedure PlaylistDragOver(Sender: TBaseVirtualTree; Source: TObject;
      {%H-}Shift: TShiftState; {%H-}State: TDragState; const {%H-}Pt: TPoint; {%H-}Mode: TDropMode;
      var {%H-}Effect: LongWord; var Accept: Boolean);
    procedure PlaylistGetPopupMenu(Sender: TBaseVirtualTree;
      {%H-}Node: PVirtualNode; {%H-}Column: TColumnIndex; const P: TPoint;
      var {%H-}AskParent: Boolean; var APopupMenu: TPopupMenu);
    procedure PlaylistGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; {%H-}TextType: TVSTTextType; var CellText: String);
    procedure PlaylistHeaderClick(Sender: TVTHeader; Column: TColumnIndex;
      Button: TMouseButton; {%H-}Shift: TShiftState; {%H-}X, {%H-}Y: Integer);
    procedure PlaylistHeaderDraw(Sender: TVTHeader; HeaderCanvas: TCanvas;
      Column: TVirtualTreeColumn; const R: TRect; {%H-}Hover, {%H-}Pressed: Boolean;
      {%H-}DropMark: TVTDropMarkMode);
    procedure PlaylistHeaderPopupMenuPopup(Sender: TObject);
    procedure PlaylistInitNode(Sender: TBaseVirtualTree; {%H-}ParentNode,
      Node: PVirtualNode; var {%H-}InitialStates: TVirtualNodeInitStates);
    procedure PlaylistDblClick(Sender: TObject);
    procedure PlaylistKeyDown(Sender: TObject; var Key: Word;
      {%H-}Shift: TShiftState);
    procedure PlaylistPopupMenuPopup(Sender: TObject);
    procedure PlayPauseItemClick(Sender: TObject);
    procedure ProgramTimerTimer(Sender: TObject);
    procedure RemoveFilesItemClick(Sender: TObject);
    procedure RenameFilesItemClick(Sender: TObject);
    procedure SavePlaylistItemClick(Sender: TObject);
    procedure SettingsItemClick(Sender: TObject);
    procedure StopItemClick(Sender: TObject);
    procedure UpdateLanguage;
    procedure UpdateTheme;
    procedure VolumeBarChange(Sender: TObject);
  private
    { private declarations }
    // The state of the collection tree on start-up
    CollectionTreeVisible: Boolean;
    // Whether a cover image is loaded
    CoverImageLoaded: Boolean;
    // The duration of the current track
    CurrentTrackLength: String[8];
    // Whether floating point PlaybackBar is supported or not
    FloatingPointSupport: Boolean;
    // Hours, minutes, seconds
    HH, MM, SS: LongInt;
    // Sorting order of playlist columns
    Order: Array of Boolean;
    // If the playback bar position is being updated automatically
    PlaybackBarAutoUpdate: Boolean;
    // The duration of all the tracks in the playlist
    TotalTrackLength: String[8];
    // Prepare the string to be displayed in the status bar
    function StatusBarWriteFromFile(const FileName: String): String;
    // Open the add files dialogue
    procedure AddFilesDlg;
    // Display BASS error message
    procedure BASSErrorMessage(const Cap, Msg: String);
    // BASS plugin loading
    procedure BASSPluginLoading(BASSPlugin: String);
    // Check if the compatible BASS version is installed
    procedure CheckBASSVersion;
    // Hide the collection tree
    procedure HideCollection;
    // Initialize BASS library
    procedure InitializeBASS;
    // Load CD track to the playlist
    // TrackNumber - the number of the track
    procedure LoadCDTrack(const TrackNumber: Byte);
    // Mute or unmute the sound
    procedure MuteUnmute;
    // Pause current file playback
    procedure PauseCurrent;
    // "Play"/"Pause"
    procedure PlayPause;
    // Check if the end of the playlist is reached
    procedure PlaylistEndCheck;
    // Move back or forward in the playlist
    procedure PlaylistMove(Count: Integer);
    // Remove files from the playlist
    procedure RemoveFiles(const PlaylistFileName: String);
    // Show the collection tree
    procedure ShowCollection;
  public
    { public declarations }
    // Go back in the playlist
    procedure GoBack;
    // Go forward in the playlist
    procedure GoForward;
    // Load the playlist
    procedure LoadPlaylist(const PlaylistFileName: String);
    // Start/resume current file playback
    procedure PlayCurrent(Restart: Boolean);
    // Stop the playback
    // PositionReset - whether reset the playback position or not
    procedure StopPlayback(const PositionReset: Boolean);
    // Toggles the playlist column visibility
    // MenuItem - the playlist header popup menu item
    procedure TogglePlaylistColumn(MenuItem: TMenuItem);
  end;

const
  // All the valid audio file extensions
  AllowedExt: Array[0..13] of String = ('.AAC', '.AC3', '.APE', '.FLAC',
  '.KAR', '.M4A', '.MID', '.MIDI', '.MP3', '.MP4', '.MPC', '.OGG', '.RMI',
  '.WAV');
  // All the audio file extensions that allow editing lyrics and tags
  EditTagExt: Array[0..6] of String = ('.AAC', '.APE', '.FLAC', '.M4A', '.MP3',
  '.MP4', '.OGG');
  PlaylistHeaderMargin = 2;
  PlaylistHeaderSide = 6;

var
  MainForm: TMainForm;
  CollectionWidth: Integer;
  TotalLengthByte: QWORD;

implementation

uses
  About_Form, BASSCD, BASSMIDI, CD_Form, Collection, Cover_Image_Form,
  Edit_Lyrics_Form, Edit_Tags_Form, FileNameConst, LazUTF8, LCLIntf,
  Lyrics_Form, Math, MIDIProc, Nocturnal_Files, Nocturnal_Language,
  Nocturnal_Misc, Please_Wait_Form, Rename_Files_Form, Settings_Form;

{$R *.lfm}

{ TMainForm }

// Prepare the string to be displayed in the status bar
function TMainForm.StatusBarWriteFromFile(const FileName: String): String;
var
  TempString: String;
begin
  Result := '';
  TempString := FileName;
  // If not a CD track
  if (UTF8Copy(GetShortFileName(TempString), 1, 8) <> 'CD Track') then
     // If not a MIDI file
     if (UpperCase(ExtractFileExt(TempString)) <> '.MID') and
     (UpperCase(ExtractFileExt(TempString)) <> '.MIDI') and
     (UpperCase(ExtractFileExt(TempString)) <> '.RMI') and
     (UpperCase(ExtractFileExt(TempString)) <> '.KAR') then
        begin
             // If the tag exists, write the tag values
             if (GetTags(TempString).Artist <> '') and
             (GetTags(TempString).Title <> '') then
                Result := Format('%s - %s', [GetTags(TempString).Artist,
                GetTags(TempString).Title])
             // If the tag does not exist, write the file name
             else
                Result := GetShortFileName(TempString)
        end
     // If a MIDI file
     else
         begin
          // If the title exists, write the title
          if (String(BASS_ChannelGetTags(PlayerStream, BASS_TAG_MIDI_TRACK))
          <> '') then
             Result := String(BASS_ChannelGetTags(PlayerStream,
             BASS_TAG_MIDI_TRACK))
          // If the tag does not exist, write the file name
          else
             Result := GetShortFileName(TempString)
         end
end;

// On clicking "About" open the "About" menu
procedure TMainForm.AboutItemClick(Sender: TObject);
begin
  AboutForm.ShowModal
end;

// "Add Files" button
procedure TMainForm.AddFilesButtonClick(Sender: TObject);
begin
  AddFilesDlg
end;

// Open the add files dialogue
procedure TMainForm.AddFilesDlg;
begin
  if AddFilesDialog.Execute then
     begin
       AddFilesToPlaylist(AddFilesDialog.Files, DefaultPlaylistFileName);
       LoadPlaylist(DefaultPlaylistFileName)
     end
end;

// "Add Files" menu item
procedure TMainForm.AddFilesItemClick(Sender: TObject);
begin
  AddFilesDlg
end;

// On clicking the "Add URL" button
procedure TMainForm.AddURLButtonClick(Sender: TObject);
begin
  URLInputForm.ShowModal
end;

// "Back" menu item
procedure TMainForm.BackItemClick(Sender: TObject);
begin
  GoBack
end;

// Display BASS error message
procedure TMainForm.BASSErrorMessage(const Cap, Msg: String);
var
  ErrorString: String;
begin
  ErrorString := GetBASSErrorFromCode(BASS_ErrorGetCode);
  MessageDlg(Cap, Format('%s. %s', [Msg, ErrorString]), mtError, [mbOK], 0)
end;

// BASS plugin loading
procedure TMainForm.BASSPluginLoading(BASSPlugin: String);
begin
  if BASS_PluginLoad(PChar(BASSPlugin), 0) = 0 then
     BASSErrorMessage(GetMessageCaption('basspluginloading'),
     GetMessageText('basspluginloading') + ' ' + BASSPlugin)
end;

// On clicking "Collection" button
procedure TMainForm.CollectionButtonClick(Sender: TObject);
begin
  // If the collection tree is not visible, show it
  if not CollectionTree.Visible then
     begin
       CollectionButton.ArrowDirection := adRight;
       ShowCollection
     end
  // If not, hide it
  else
      begin
        CollectionButton.ArrowDirection := adLeft;
        HideCollection
      end
end;

// Get hint text for the track
procedure TMainForm.CollectionTreeGetHint(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex;
  var LineBreakStyle: TVTTooltipLineBreakStyle; var HintText: String);
var
  Level: Integer;
begin
  Level := Sender.GetNodeLevel(Node);
  // For every track display its multimedia info
  if Level = 2 then
     HintText := Format('%s: %s' + #13 + '%s: %s' + #13 +
     '%s: %s' + #13 + '%s: %s' + #13 + '%s: %s' + #13 +
     '%s: %s' + #13 + '%s: %s' + #13 + '%s: %s' + #13 +
     '%s: %s' + #13 + '%s: %s' + #13 + '%s: %s' + #13 +
     '%s: %s' + #13,
     [GetTagText('artist'),
     Artists[Node^.Parent^.Parent^.Index].Albums[Node^.Parent^.Index].
     Tracks[Node^.Index].Artist,
     GetTagText('albumartist'),
     Artists[Node^.Parent^.Parent^.Index].Albums[Node^.Parent^.Index].
     Tracks[Node^.Index].AlbumArtist,
     GetTagText('album'),
     Artists[Node^.Parent^.Parent^.Index].Albums[Node^.Parent^.Index].
     Tracks[Node^.Index].Album,
     GetTagText('year'),
     Artists[Node^.Parent^.Parent^.Index].Albums[Node^.Parent^.Index].
     Tracks[Node^.Index].Year,
     GetTagText('disc'),
     Artists[Node^.Parent^.Parent^.Index].Albums[Node^.Parent^.Index].
     Tracks[Node^.Index].Disc,
     GetTagText('title'),
     Artists[Node^.Parent^.Parent^.Index].Albums[Node^.Parent^.Index].
     Tracks[Node^.Index].Title,
     GetTagText('subtitle'),
     Artists[Node^.Parent^.Parent^.Index].Albums[Node^.Parent^.Index].
     Tracks[Node^.Index].Subtitle,
     GetTagText('track'),
     Artists[Node^.Parent^.Parent^.Index].Albums[Node^.Parent^.Index].
     Tracks[Node^.Index].Track,
     GetTagText('genre'),
     Artists[Node^.Parent^.Parent^.Index].Albums[Node^.Parent^.Index].
     Tracks[Node^.Index].Genre,
     GetTagText('comment'),
     Artists[Node^.Parent^.Parent^.Index].Albums[Node^.Parent^.Index].
     Tracks[Node^.Index].Comment,
     GetTagText('composer'),
     Artists[Node^.Parent^.Parent^.Index].Albums[Node^.Parent^.Index].
     Tracks[Node^.Index].Composer,
     GetTagText('copyright'),
     Artists[Node^.Parent^.Parent^.Index].Albums[Node^.Parent^.Index].
     Tracks[Node^.Index].Copyright])
end;

// Get text for the cell
procedure TMainForm.CollectionTreeGetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: String);
var
  Level: Integer;
  TempAlbum: TAlbum;
  TempArtist: TArtist;
  TempTrack: TTrack;
begin
  Level := Sender.GetNodeLevel(Node);
  case Level of
     0:
       // Artists
       begin
         TempArtist := Artists[Node^.Index];
         CellText := TempArtist.Name
       end;
     1:
       // Albums
       begin
         TempAlbum := Artists[Node^.Parent^.Index].Albums[Node^.Index];
         if (TempAlbum.Year <> '') and (TempAlbum.Name <> '') and
         (TempAlbum.Disc <> '') then
            CellText := Format('%s %s (CD %s)', [TempAlbum.Year,
            TempAlbum.Name, TempAlbum.Disc])
         else
           begin
             if (TempAlbum.Year <> '') and (TempAlbum.Name <> '') then
                CellText := Format('%s %s', [TempAlbum.Year, TempAlbum.Name])
             else
               CellText := TempAlbum.Name
           end
       end;
     2:
       // Tracks
       begin
         TempTrack :=
         Artists[Node^.Parent^.Parent^.Index].Albums[Node^.Parent^.Index].
         Tracks[Node^.Index];
         if (TempTrack.Track <> '') and (TempTrack.Title <> '') and
             (TempTrack.AlbumArtist <> '') and (TempTrack.Artist <> '') and
             (TempTrack.AlbumArtist <> TempTrack.Artist) then
            CellText := Format('%s. %s - %s', [TempTrack.Track,
            TempTrack.Artist, TempTrack.Title])
         else
           begin
             if (TempTrack.Track <> '') and (TempTrack.Title <> '') then
                CellText := Format('%s. %s', [TempTrack.Track, TempTrack.Title])
             else
               CellText := TempTrack.Title
           end
       end
  end
end;

// Initialize the children of the node
procedure TMainForm.CollectionTreeInitChildren(Sender: TBaseVirtualTree;
  Node: PVirtualNode; var ChildCount: Cardinal);

  // Sort all the albums in alphabetical order
  procedure SortAlbums;
  var
    Count1, Count2, Iterator, Min: Integer;
    TempAlbum: TAlbum;
  begin
    Count1 := Length(Artists[Node^.Index].Albums);
    Min := 0;
    for Iterator := 1 to Count1 - 1 do
       if (StrToIntDef(Artists[Node^.Index].Albums[Iterator].Year, 0) <
       StrToIntDef(Artists[Node^.Index].Albums[Min].Year, 0)) or
       (Artists[Node^.Index].Albums[Iterator].Year =
       Artists[Node^.Index].Albums[Min].Year) and
       (Artists[Node^.Index].Albums[Iterator].Name <
       Artists[Node^.Index].Albums[Min].Name) then
          Min := Iterator;
    TempAlbum := Artists[Node^.Index].Albums[0];
    Artists[Node^.Index].Albums[0] := Artists[Node^.Index].Albums[Min];
    Artists[Node^.Index].Albums[Min] := TempAlbum;
    for Iterator := 1 to Count1 - 1 do
       begin
         TempAlbum := Artists[Node^.Index].Albums[Iterator];
         Count2 := Iterator - 1;
         while (StrToIntDef(Artists[Node^.Index].Albums[Count2].Year, 0) >
         StrToIntDef(TempAlbum.Year, 0)) or
         (Artists[Node^.Index].Albums[Count2].Year = TempAlbum.Year) and
         (Artists[Node^.Index].Albums[Count2].Name > TempAlbum.Name) do
           begin
             Artists[Node^.Index].Albums[Count2 + 1] :=
             Artists[Node^.Index].Albums[Count2];
             Dec(Count2)
           end;
         Artists[Node^.Index].Albums[Count2 + 1] := TempAlbum
       end
  end;

  // Sort all the tracks in alphabetical order
  procedure SortTracks;
  var
    Count1, Count2, Iterator, Min: Integer;
    TempTrack: TTrack;
  begin
    Count1 := Length(Artists[Node^.Parent^.Index].Albums[Node^.Index].Tracks);
    Min := 0;
    for Iterator := 1 to Count1 - 1 do
       if (StrToIntDef(Artists[Node^.Parent^.Index].Albums[Node^.Index].
       Tracks[Iterator].Track, 0) < StrToIntDef(Artists[Node^.Parent^.Index].
       Albums[Node^.Index].Tracks[Min].Track, 0)) or
       (Artists[Node^.Parent^.Index].Albums[Node^.Index].Tracks[Iterator].Track =
       Artists[Node^.Parent^.Index].Albums[Node^.Index].Tracks[Min].Track) and
       (Artists[Node^.Parent^.Index].Albums[Node^.Index].Tracks[Iterator].Title
       < Artists[Node^.Parent^.Index].Albums[Node^.Index].Tracks[Min].Title)
       then
          Min := Iterator;
    TempTrack := Artists[Node^.Parent^.Index].Albums[Node^.Index].Tracks[0];
    Artists[Node^.Parent^.Index].Albums[Node^.Index].Tracks[0] :=
    Artists[Node^.Parent^.Index].Albums[Node^.Index].Tracks[Min];
    Artists[Node^.Parent^.Index].Albums[Node^.Index].Tracks[Min] := TempTrack;
    for Iterator := 1 to Count1 - 1 do
       begin
         TempTrack :=
         Artists[Node^.Parent^.Index].Albums[Node^.Index].Tracks[Iterator];
         Count2 := Iterator - 1;
         while (StrToIntDef(Artists[Node^.Parent^.Index].Albums[Node^.Index].
         Tracks[Count2].Track, 0) > StrToIntDef(TempTrack.Track, 0)) or
         (Artists[Node^.Parent^.Index].Albums[Node^.Index].Tracks[Count2].Track
         = TempTrack.Track) and
         (Artists[Node^.Parent^.Index].Albums[Node^.Index].Tracks[Count2].Title
         > TempTrack.Title) do
           begin
             Artists[Node^.Parent^.Index].Albums[Node^.Index].Tracks[Count2 + 1]
             := Artists[Node^.Parent^.Index].Albums[Node^.Index].Tracks[Count2];
             Dec(Count2)
           end;
         Artists[Node^.Parent^.Index].Albums[Node^.Index].Tracks[Count2 + 1] :=
         TempTrack
       end
  end;

  // Get all the albums correspoding to the artist
  function GetLevel1Nodes: Integer;
  var
    Count, Iterator1, Iterator2: Integer;
    Repeated: Boolean;
    TempString: String;
    TempTrack: TTrack;
  begin
    Count := 0;
    if Length(CollectionList) > 0 then
       for Iterator1 := 0 to Length(CollectionList) - 1 do
          begin
            Application.ProcessMessages;
            TempTrack := CollectionList[Iterator1];
            TempString := Artists[Node^.Index].Name;
            if (TempTrack.AlbumArtist = TempString) or
            (TempTrack.Artist = TempString) or
            (TempString = 'Unknown Artist') and
            (TempTrack.AlbumArtist = '') and (TempTrack.Artist = '') then
               begin
                 Repeated := False;
                 if Length(Artists[Node^.Index].Albums) > 0 then
                    begin
                      for Iterator2 := 0 to Count - 1 do
                         Repeated := Repeated or ((TempTrack.Album =
                         Artists[Node^.Index].Albums[Iterator2].Name) or
                         (Artists[Node^.Index].Albums[Iterator2].Name =
                         'Unknown Album') and (TempTrack.Album = '')) and
                         (TempTrack.Disc =
                         Artists[Node^.Index].Albums[Iterator2].Disc) and
                         (TempTrack.Year =
                         Artists[Node^.Index].Albums[Iterator2].Year)
                    end;
                 if not Repeated then
                    begin
                      SetLength(Artists[Node^.Index].Albums, Count + 1);
                      if TempTrack.Album <> '' then
                         Artists[Node^.Index].Albums[Count].Name :=
                         TempTrack.Album
                      else
                        Artists[Node^.Index].Albums[Count].Name :=
                        'Unknown Album';
                      if TempTrack.Disc <> '' then
                         Artists[Node^.Index].Albums[Count].Disc :=
                         TempTrack.Disc
                      else
                        Artists[Node^.Index].Albums[Count].Disc := '';
                      if TempTrack.Year <> '' then
                         Artists[Node^.Index].Albums[Count].Year :=
                         TempTrack.Year
                      else
                        Artists[Node^.Index].Albums[Count].Year := '';
                      Inc(Count)
                    end
               end
          end;
    if Length(Artists[Node^.Index].Albums) > 1 then
       SortAlbums;
    Result := Count
  end;

  // Get all the tracks corresponding to the album
  function GetLevel2Nodes: Integer;
  var
    Count, Iterator: Integer;
    TempString1, TempString2, TempString3, TempString4: String;
    TempTrack: TTrack;
  begin
    Count := 0;
    if Length(CollectionList) > 0 then
       for Iterator := 0 to Length(CollectionList) - 1 do
          begin
            Application.ProcessMessages;
            TempTrack := CollectionList[Iterator];
            TempString1 := Artists[Node^.Parent^.Index].Name;
            TempString2 :=
            Artists[Node^.Parent^.Index].Albums[Node^.Index].Name;
            TempString3 :=
            Artists[Node^.Parent^.Index].Albums[Node^.Index].Year;
            TempString4 :=
            Artists[Node^.Parent^.Index].Albums[Node^.Index].Disc;
            if ((TempTrack.AlbumArtist = TempString1) or
            (TempTrack.Artist = TempString1) or
            (TempString1 = 'Unknown Artist') and (TempTrack.AlbumArtist = '')
            and (TempTrack.Artist = '')) and
            ((TempTrack.Album = TempString2) or
            (TempString2 = 'Unknown Album') and (TempTrack.Album = '')) and
            (TempTrack.Year = TempString3) and
            (TempTrack.Disc = TempString4) then
               begin
                 SetLength(
                 Artists[Node^.Parent^.Index].Albums[Node^.Index].Tracks,
                 Count + 1);
                 Artists[Node^.Parent^.Index].Albums[Node^.Index].
                 Tracks[Count] := TempTrack;
                 Inc(Count)
               end
          end;
    if Length(Artists[Node^.Parent^.Index].Albums[Node^.Index].Tracks) > 1 then
       SortTracks;
    Result := Count
  end;

var
  Level: Integer;
begin
  Level := Sender.GetNodeLevel(Node);
  case Level of
     0: ChildCount := GetLevel1Nodes;
     1: ChildCount := GetLevel2Nodes
  end
end;

// Initialize the node
procedure TMainForm.CollectionTreeInitNode(Sender: TBaseVirtualTree;
  ParentNode, Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
begin
  if Sender.GetNodeLevel(Node) < 2 then
     Include(InitialStates, ivsHasChildren)
end;

// Check if the compatible BASS version is installed
procedure TMainForm.CheckBASSVersion;
var
  Cap, Msg: String;
begin
  if not (HIWORD(BASS_GetVersion) = BASSVERSION) then
     begin
       Cap := GetMessageCaption('bassversioncheck');
       Msg := GetMessageText('bassversioncheck');
       if MessageDlg(Cap, Msg, mtWarning, [mbYes, mbNo], 0) = mrNo then
          Application.Terminate
     end
end;

// When the mouse enters the area of the cover image
procedure TMainForm.CoverImageMouseEnter(Sender: TObject);
begin
  // If there is an image being displayed, then show it in full size
  if CoverImageLoaded then
     begin
       CoverImageForm.CoverImage.Picture := CoverImage.Picture;
       if (CoverImageForm.CoverImage.Picture.Height <= MainForm.Height) and
       (CoverImageForm.CoverImage.Picture.Width <= MainForm.Width) then
         CoverImageForm.CoverImage.AutoSize := True
       else
          begin
            CoverImageForm.CoverImage.AutoSize := False;
            CoverImageForm.CoverImage :=
            ResizeCoverImage(CoverImageForm.CoverImage, MainForm)
          end;
       CoverImageForm.Position := poMainFormCenter;
       CoverImageForm.ShowOnTop
     end
end;

// When the mouse leaves the area of the cover image
procedure TMainForm.CoverImageMouseLeave(Sender: TObject);
begin
  // If there is an image being displayed then close the form showing it
  // in full size
  if CoverImageLoaded then
     CoverImageForm.Close
end;

// On clicking "Edit Lyrics" open the "Lyrics" form
procedure TMainForm.EditLyricsItemClick(Sender: TObject);
var
  FileListItem: TListItem;
  TempNode: PVirtualNode;
  TempString: String;
begin
  EditLyricsForm.FileList.Clear;
  // Check all the selected items
  TempNode := Playlist.GetFirstSelected(False);
  if TempNode <> nil then
     // Fill out the file list
     with EditLyricsForm.FileList do
          begin
           BeginUpdate;
           repeat
             FileListItem := Items.Add;
             TempString := Tracks[TempNode^.Index].FileName;
             // Lyrics
             FileListItem.SubItems.Add(GetLyrics(TempString));
             // Full path to the file
             FileListItem.SubItems.Add(TempString);
             // Unchanged value
             FileListItem.SubItems.Add('');
             // File name
             FileListItem.Caption := GetShortFileName(TempString);
             TempNode := Playlist.GetNextSelected(TempNode, False)
	   until TempNode = nil;
           EndUpdate
          end;
  EditLyricsForm.Show
end;

// On clicking "Edit Tags" open the "Edit Tags" form
procedure TMainForm.EditTagsItemClick(Sender: TObject);
var
  FileListItem: TListItem;
  TempNode: PVirtualNode;
begin
  EditTagsForm.FileList.Clear;
  // Check all the selected items
  TempNode := Playlist.GetFirstSelected(False);
  if TempNode <> nil then
     begin
       // Fill out the file list
       with EditTagsForm.FileList do
            begin
             BeginUpdate;
             repeat
               FileListItem := Items.Add;
               // Add filelist subitems
               if Tracks[TempNode^.Index].Title =
               GetShortFileName(Tracks[TempNode^.Index].FileName) then
                  FileListItem.SubItems.Add('')
               else
                 FileListItem.SubItems.Add(Tracks[TempNode^.Index].Title);
               FileListItem.SubItems.Add(Tracks[TempNode^.Index].Subtitle);
               FileListItem.SubItems.Add(Tracks[TempNode^.Index].Artist);
               FileListItem.SubItems.Add(Tracks[TempNode^.Index].AlbumArtist);
               FileListItem.SubItems.Add(Tracks[TempNode^.Index].Album);
               FileListItem.SubItems.Add(Tracks[TempNode^.Index].Year);
               FileListItem.SubItems.Add(Tracks[TempNode^.Index].Track);
               FileListItem.SubItems.Add(Tracks[TempNode^.Index].Disc);
               FileListItem.SubItems.Add(Tracks[TempNode^.Index].Genre);
               FileListItem.SubItems.Add(Tracks[TempNode^.Index].Comment);
               FileListItem.SubItems.Add(Tracks[TempNode^.Index].Composer);
               FileListItem.SubItems.Add(Tracks[TempNode^.Index].Copyright);
               FileListItem.SubItems.Add(Tracks[TempNode^.Index].FileName);
               // Unchanged value
               FileListItem.SubItems.Add('');
               // File name
               FileListItem.Caption :=
               GetShortFileName(Tracks[TempNode^.Index].FileName);
               TempNode := Playlist.GetNextSelected(TempNode, False)
	     until TempNode = nil;
             EndUpdate
            end
     end;
  EditTagsForm.Show
end;

// "Exit" menu item
procedure TMainForm.ExitItemClick(Sender: TObject);
begin
  MainForm.Close
end;

// On main form activation
procedure TMainForm.FormActivate(Sender: TObject);
begin
  UpdateLanguage;
  UpdateTheme;
  // Initialize the collection
  if Length(CollectionList) > 0 then
     CollectionButton.Visible := True
  else
      begin
        CollectionButton.Visible := False;
        if CollectionTree.Visible then
           HideCollection
      end;
  // Change the arrow button state depending on the collection tree visibility
  if CollectionTree.Visible then
     CollectionButton.ArrowDirection := adRight
  else
    CollectionButton.ArrowDirection := adLeft
end;

// On main form creation
procedure TMainForm.FormCreate(Sender: TObject);
var
  AuthorString, CodeString, NameString, TempString, VersionString: String;
  CDNFO: BASS_CD_INFO;
  DriveSet: Boolean;
  Iterator, MainHeight, MainLeft, MainTop, MainWidth, PlaylistColTag,
    PlaylistColWidth: Integer;
begin
  // Check the installed BASS version
  CheckBASSVersion;
  // Assigning the paths
  ProgramPath := GetEnvironmentVariable('HOME') + '/.config/nocturnal-player';
  if not DirectoryExists(ProgramPath) then
     CreateDir(ProgramPath);
  DefaultCollectionFileName := ProgramPath + '/collection';
  DefaultPlaylistFileName := ProgramPath + '/playlist';
  DefaultScannedFileName := ProgramPath + '/scanned';
  DefaultSettingsFileName := ProgramPath + '/settings';
  // Create the settings file
  CreateSettingsFile(DefaultSettingsFileName);
  // Load main form display settings
  MainHeight := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'MAIN_FORM_HEIGHT'), 0);
  MainLeft := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'MAIN_FORM_LEFT'), 0);
  MainTop := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'MAIN_FORM_TOP'), 0);
  MainWidth := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'MAIN_FORM_WIDTH'), 0);
  if MainHeight <> 0 then
     Height := MainHeight;
  if MainLeft <> 0 then
     Left := MainLeft;
  if MainTop <> 0 then
     Top := MainTop;
  if MainWidth <> 0 then
     Width := MainWidth;
  // Set the font settings
  Font.CharSet := 4; // UNICODE
  Font.Quality := fqAntialiased;
  Font.Size := 13;
  Font.Style := [];
  // Get all the available languages
  GetAllLanguages;
  // Set the language
  CodeString := ReadFromSettingsFile(DefaultSettingsFileName, 'LANGUAGE');
  if CodeString = '' then
     begin
       WriteToSettingsFile(DefaultSettingsFileName, 'LANGUAGE', 'ENG');
       AppLanguage := GetLanguageFromCode('ENG')
     end
  else
      AppLanguage := GetLanguageFromCode(CodeString);
  UpdateLanguage;
  UpdateTheme;
  // Set the node height
  CollectionTree.DefaultNodeHeight := Max(20, Canvas.TextHeight('0'));
  Playlist.DefaultNodeHeight := Max(20, Canvas.TextHeight('0'));
  // Set the playlist columns
  for Iterator := 0 to Playlist.Header.Columns.Count - 1 do
     begin
       PlaylistColWidth := StrToIntDef(ReadFromSettingsFile(
       DefaultSettingsFileName, Format('PLAYLIST_COL%d_WIDTH', [Iterator])), 0);
       if PlaylistColWidth <> 0 then
          Playlist.Header.Columns[Iterator].Width := PlaylistColWidth
       else
         Playlist.Header.Columns[Iterator].Width :=
         Canvas.TextWidth(Playlist.Header.Columns[Iterator].Text) +
         PlaylistHeaderMargin + PlaylistHeaderSide;
       if ReadFromSettingsFile(DefaultSettingsFileName,
        Format('PLAYLIST_COL%d_VISIBLE', [Iterator])) = 'OFF' then
            Playlist.Header.Columns[Iterator].Options :=
            Playlist.Header.Columns[Iterator].Options - [coVisible];
       PlaylistColTag := StrToIntDef(ReadFromSettingsFile(
       DefaultSettingsFileName, Format('PLAYLIST_COL%d_TAG', [Iterator])), -1);
       if PlaylistColTag >= 0 then
          Playlist.Header.Columns[Iterator].Tag := PlaylistColTag
       else
         Playlist.Header.Columns[Iterator].Tag := Iterator
     end;
  // Get all the available themes
  GetAllThemes;
  // Set the theme
  NameString := ReadFromSettingsFile(DefaultSettingsFileName, 'THEME_NAME');
  VersionString :=
  ReadFromSettingsFile(DefaultSettingsFileName, 'THEME_VERSION');
  AuthorString :=
  ReadFromSettingsFile(DefaultSettingsFileName, 'THEME_AUTHOR');
  if NameString = '' then
     begin
       WriteToSettingsFile(DefaultSettingsFileName, 'THEME_NAME', 'Nocturnal');
       NameString := 'Nocturnal';
       WriteToSettingsFile(DefaultSettingsFileName, 'THEME_VERSION', '1.0');
       VersionString := '1.0';
       WriteToSettingsFile(DefaultSettingsFileName, 'THEME_AUTHOR', 'NoSt');
       AuthorString := 'NoSt'
     end;
  if Length(AllThemes) > 0 then
     for Iterator := 0 to Length(AllThemes) - 1 do
        if (AllThemes[Iterator].Name = NameString) and
        (AllThemes[Iterator].Version = VersionString) and
        (AllThemes[Iterator].Author = AuthorString) then
           AppTheme := AllThemes[Iterator];
  // Set the soundfont
  SFPath := ReadFromSettingsFile(DefaultSettingsFileName, 'SOUNDFONT');
  if SFPath = '' then
     begin
       WriteToSettingsFile(DefaultSettingsFileName, 'SOUNDFONT', ChoriumPath);
       SFPath := ChoriumPath
     end;
  CoverImageLoaded := False;
  SetLength(Artists, 0);
  SetLength(CollectionList, 0);
  SetLength(Order, Playlist.Header.Columns.Count);
  SetLength(Tracks, 0);
  // Prepare the playbar
  Playbar.OnMuteUnmuteClick := @PlaybarMuteUnmuteClick;
  Playbar.OnNextClick := @PlaybarNextClick;
  Playbar.OnPlayPauseClick := @PlaybarPlayPauseClick;
  Playbar.OnPrevClick := @PlaybarPrevClick;
  Playbar.OnStopClick := @PlaybarStopClick;
  // Prepare the playlist
  Playlist.NodeDataSize := SizeOf(TTrack);
  Playlist.RootNodeCount := Length(Tracks);
  // Set the form caption
  Caption := Application.Title + ' ' + ResourceVersionInfo;
  // Prepare About form
  Application.CreateForm(TAboutForm, AboutForm);
  AboutForm.Visible := False;
  // Prepare CD Selection form
  Application.CreateForm(TCDForm, CDForm);
  CDForm.Visible := False;
  // Prepare Cover Image form
  Application.CreateForm(TCoverImageForm, CoverImageForm);
  CoverImageForm.Visible := False;
  // Prepare Edit Lyrics form
  Application.CreateForm(TEditLyricsForm, EditLyricsForm);
  EditLyricsForm.Visible := False;
  // Prepare Edit Tags form
  Application.CreateForm(TEditTagsForm, EditTagsForm);
  EditTagsForm.Visible := False;
  // Prepare Lyrics form
  Application.CreateForm(TLyricsForm, LyricsForm);
  LyricsForm.Visible := False;
  // Prepare Please Wait form
  Application.CreateForm(TPleaseWaitForm, PleaseWaitForm);
  PleaseWaitForm.Visible := False;
  // Prepare Rename Files form
  Application.CreateForm(TRenameFilesForm, RenameFilesForm);
  RenameFilesForm.Visible := False;
  // Prepare Settings form
  Application.CreateForm(TSettingsForm, SettingsForm);
  SettingsForm.Visible := False;
  // Prepare URL Input form
  Application.CreateForm(TURLInputForm, URLInputForm);
  URLInputForm.Visible := False;
  TempPlayerVolume := 0;
  // If there is not volume value stored in the settings file, then write it
  if ReadFromSettingsFile(DefaultSettingsFileName, 'VOLUME') = '' then
     begin
       PlayerVolume := 1;
       WriteToSettingsFile(DefaultSettingsFileName, 'VOLUME',
       FloatToStr(PlayerVolume))
     end
  else
      // Set the player volume
      PlayerVolume :=
      StrToFloatDef(ReadFromSettingsFile(DefaultSettingsFileName, 'VOLUME'), 0);
  VolumeBar.Position := Round(PlayerVolume * 100);
  // Check the floating point playback support
  FloatingPointSupport := FloatingPointCheck;
  // Load the equalizer settings
  case ReadFromSettingsFile(DefaultSettingsFileName, 'EQ_STATE') of
       'ON': EQOn := True;
       'OFF': EQOn := False
       else
         begin
           WriteToSettingsFile(DefaultSettingsFileName, 'EQ_STATE', 'OFF');
           EQOn := False
         end
  end;
  Equalizer.fBandwidth := StrToFloatDef(ReadFromSettingsFile(
  DefaultSettingsFileName, 'EQ_BANDWIDTH'), 0);
  if (Equalizer.fBandwidth < 1) or  (Equalizer.fBandwidth > 36) then
     begin
       Equalizer.fBandwidth := 12;
       WriteToSettingsFile(DefaultSettingsFileName, 'EQ_BANDWIDTH',
       FloatToStr(Equalizer.fBandwidth))
     end;
  Equalizer.fCenter := StrToFloatDef(ReadFromSettingsFile(
  DefaultSettingsFileName, 'EQ_CENTER'), -1);
  if (Equalizer.fCenter < 0) or (Equalizer.fCenter > 22050) then
     begin
       Equalizer.fCenter := 22050;
       WriteToSettingsFile(DefaultSettingsFileName, 'EQ_CENTER',
       FloatToStr(Equalizer.fCenter))
     end;
  Equalizer.fGain := StrToFloatDef(ReadFromSettingsFile(
  DefaultSettingsFileName, 'EQ_GAIN'), -16);
  if (Equalizer.fGain < -15) or (Equalizer.fGain > 15) then
     begin
       Equalizer.fGain := 0;
       WriteToSettingsFile(DefaultSettingsFileName, 'EQ_GAIN',
       FloatToStr(Equalizer.fGain))
     end;
  PlaylistPosition := -1;
  // BASS initialization
  InitializeBASS;
  // Load AAC plugin
  BASSPluginLoading(LibsPath + 'libbass_aac.so');
  // Load AC3 plugin
  BASSPluginLoading(LibsPath + 'libbass_ac3.so');
  // Load APE plugin
  BASSPluginLoading(LibsPath + 'libbass_ape.so');
  // Load FLAC plugin
  BASSPluginLoading(LibsPath + 'libbassflac.so');
  // Load MIDI plugin
  BASSPluginLoading(LibsPath + 'libbassmidi.so');
  // Load MPC plugin
  BASSPluginLoading(LibsPath + 'libbass_mpc.so');
  DriveSet := False;
  CDNFO.product := nil;
  TempString := '';
  // Check all the available CD drives
  for Iterator := 0 to 255 do
     begin
       if BASS_CD_GetInfo(Iterator, CDNFO) then
          begin
            CDForm.CDDriveSelection.Items.Add(Format('%d: %s %s', [Iterator,
            CDNFO.vendor, CDNFO.product]));
            if Iterator = 0 then
                  TempString := IntToStr(Iterator)
               else
                  if not DriveSet then
                     begin
                       TempString := IntToStr(Iterator);
                       SetDrive := Iterator;
                       DriveSet := True
                     end
          end
     end;
  if TempString = '' then
     PlayCDButton.Enabled := False
  else
     begin
       PlayCDButton.Enabled := True;
       if ReadFromSettingsFile(DefaultSettingsFileName, 'CD_DRIVE') = '' then
          // Write the default CD drive to the settings file
          WriteToSettingsFile(DefaultSettingsFileName, 'CD_DRIVE', TempString)
     end;
  // Load the playlist from file
  LoadPlaylist(DefaultPlaylistFileName);
  // Initialize the collection
  InitializeCollection;
  // Load the collection tree visibility state
  case ReadFromSettingsFile(DefaultSettingsFileName, 'COLLECTION_VISIBLE') of
       'ON': CollectionTreeVisible := True;
       'OFF': CollectionTreeVisible := False
       else
         WriteToSettingsFile(DefaultSettingsFileName, 'COLLECTION_VISIBLE',
         'OFF')
  end;
  // Prepare the collection
  if CollectionTreeVisible then
     CollectionTree.Visible := True;
  LyricsButton.Visible := False;
  StatusBar1.Panels[0].Width := Max(StatusBar1.Canvas.TextWidth(
  GetFormControlText('main_form', 'statusbar', 'playstate_paused')),
  StatusBar1.Canvas.TextWidth(
  GetFormControlText('main_form', 'statusbar', 'playstate_playing')));
  StatusBar1.Panels[2].Width :=
  StatusBar1.Canvas.TextWidth('00:00:00/00:00:00');
  StatusBar1.Panels[3].Width := StatusBar1.Canvas.TextWidth(TotalTrackLength)
end;

// On main form destruction
procedure TMainForm.FormDestroy(Sender: TObject);
var
  Iterator: Integer;
begin
  // On exit write the position and the size of the form into the settings file
  if Height <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'MAIN_FORM_HEIGHT'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'MAIN_FORM_HEIGHT',
     IntToStr(Height));
  if Left <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'MAIN_FORM_LEFT'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'MAIN_FORM_LEFT',
     IntToStr(Left));
  if Top <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'MAIN_FORM_TOP'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'MAIN_FORM_TOP',
     IntToStr(Top));
  if Width <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'MAIN_FORM_WIDTH'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'MAIN_FORM_WIDTH',
     IntToStr(Width));
  for Iterator := 0 to Playlist.Header.Columns.Count - 1 do
     begin
       if Playlist.Header.Columns[Iterator].Width <> StrToIntDef(
       ReadFromSettingsFile(DefaultSettingsFileName,
       Format('PLAYLIST_COL%d_WIDTH', [Iterator])), 0) then
          WriteToSettingsFile(DefaultSettingsFileName,
          Format('PLAYLIST_COL%d_WIDTH', [Iterator]),
          IntToStr(Playlist.Header.Columns[Iterator].Width));
       if coVisible in Playlist.Header.Columns[Iterator].Options then
          WriteToSettingsFile(DefaultSettingsFileName,
          Format('PLAYLIST_COL%d_VISIBLE', [Iterator]), 'ON')
       else
         WriteToSettingsFile(DefaultSettingsFileName,
          Format('PLAYLIST_COL%d_VISIBLE', [Iterator]), 'OFF');
       WriteToSettingsFile(DefaultSettingsFileName, Format('PLAYLIST_COL%d_TAG',
       [Playlist.Header.Columns[Iterator].Position]),
       IntToStr(Playlist.Header.Columns[Iterator].Tag))
     end;
  // Write the player volume value to the settings file
  WriteToSettingsFile(DefaultSettingsFileName, 'VOLUME',
  FloatToStr(PlayerVolume));
  // Write the collection tree visibility state to the settings file
  if CollectionTree.Visible then
     WriteToSettingsFile(DefaultSettingsFileName, 'COLLECTION_VISIBLE', 'ON')
  else
    WriteToSettingsFile(DefaultSettingsFileName, 'COLLECTION_VISIBLE', 'OFF');
  // If the CD/DVD drive is locked, unlock it
  if BASS_CD_DoorIsLocked(SetDrive) then
     BASS_CD_Door(SetDrive, BASS_CD_DOOR_UNLOCK);
  SetLength(AllLanguages, 0);
  SetLength(AllThemes, 0);
  SetLength(Artists, 0);
  SetLength(CollectionList, 0);
  SetLength(Order, 0);
  SetLength(Tracks, 0);
  BASS_Free
end;

// On dropping the file on form
procedure TMainForm.FormDropFiles(Sender: TObject;
  const FileNames: Array of String);
var
  Count: Integer;
  Receiver: TControl;
  TempString: String;
  TempStrings: TStringList;

  // Scan inside the directory
  procedure ScanDir(const Directory: String);
  var
    SearchRec: TSearchRec;
  begin
    // Check, if the directory exists
    if not DirectoryExists(Directory) then
       Exit;
    // Check, if there is any file in the directory
    if FindFirst(Directory + '/*', faAnyFile, SearchRec) = 0 then
       repeat
         Application.ProcessMessages;
         // If not a directory
         if not (SearchRec.Attr and faDirectory > 0) then
            begin
              for TempString in AllowedExt do
                 // If a file has an allowed extension, then add it to the list
                 if UpperCase(ExtractFileExt(SearchRec.Name)) =
                 TempString then
                    begin
                      TempStrings.Add(Directory + '/' + SearchRec.Name);
                      Break
                    end
            end
         // If a directory
         else
           begin
             // Scan inside the directory
             if SearchRec.Name[1] <> '.' then
                ScanDir(Directory + '/' + SearchRec.Name)
           end
       until FindNext(SearchRec) <> 0;
    FindClose(SearchRec)
  end;

  // Sort the files in aplabetical order
  procedure SortTempStrings;
  var
    Count1, Count2, Iterator, Min: Integer;
  begin
    Count1 := TempStrings.Count;
    Min := 0;
    for Iterator := 1 to Count1 - 1 do
       if TempStrings[Iterator] < TempStrings[Min] then
          Min := Iterator;
    TempString := TempStrings[0];
    TempStrings[0] := TempStrings[Min];
    TempStrings[Min] := TempString;
    for Iterator := 1 to Count1 - 1 do
       begin
         TempString := TempStrings[Iterator];
         Count2 := Iterator - 1;
         while TempStrings[Count2] > TempString do
           begin
             TempStrings[Count2 + 1] := TempStrings[Count2];
             Dec(Count2)
           end;
         TempStrings[Count2 + 1] := TempString
       end
  end;

begin
  // Check if the files are dropped onto the playlist
  Receiver := FindControlAtPosition(Mouse.CursorPos, False);
  if Receiver = Playlist then
     begin
       // If one Nocturnal Player playlist file is dropped
       if (Length(FileNames) = 1) and
       (UpperCase(ExtractFileExt(FileNames[0])) = '.NPL') then
          // Load that playlist
          LoadPlaylist(FileNames[0])
       // If several files or one non-playlist file are dropped
       else
          begin
            TempStrings := TStringList.Create;
            // Check all the file names
            for Count := Low(FileNames) to High(FileNames) do
               begin
                 Application.ProcessMessages;
                 if DirectoryExists(FileNames[Count]) then
                    ScanDir(FileNames[Count])
                 else
                   for TempString in AllowedExt do
                      // If a file has an allowed extension,
                      // then add it to the list
                      if UpperCase(ExtractFileExt(FileNames[Count])) =
                      TempString then
                         begin
                           TempStrings.Add(FileNames[Count]);
                           Break
                         end
               end;
            // Sort the strings before adding them to the playlist
            SortTempStrings;
            // Add all the available files to the playlist
            AddFilesToPlaylist(TempStrings, DefaultPlaylistFileName);
            TempStrings.Free;
            LoadPlaylist(DefaultPlaylistFileName)
          end
     end
end;

// "Forward" menu item
procedure TMainForm.ForwardItemClick(Sender: TObject);
begin
  GoForward
end;

// Go back in the playlist
procedure TMainForm.GoBack;
begin
  if (BASS_ChannelIsActive(PlayerStream) = BASS_ACTIVE_PLAYING) or
  (BASS_ChannelIsActive(PlayerStream) = BASS_ACTIVE_PAUSED) then
     PlaylistMove(-1)
end;

// Go forward in the playlist
procedure TMainForm.GoForward;
begin
  if (BASS_ChannelIsActive(PlayerStream) = BASS_ACTIVE_PLAYING) or
  (BASS_ChannelIsActive(PlayerStream) = BASS_ACTIVE_PAUSED) then
     PlaylistMove(1)
end;

// "Help" menu item
procedure TMainForm.HelpItemClick(Sender: TObject);
begin
  OpenURL('file://' + HelpPath)
end;

// Hide the collection tree
procedure TMainForm.HideCollection;
begin
  Width := Width - CollectionTree.Width;
  Left := Left + CollectionTree.Width;
  CollectionWidth := CollectionTree.Width;
  CollectionTree.Width := 1;
  CollectionTree.Visible := False
end;

// Initialize BASS library
procedure TMainForm.InitializeBASS;
begin
  // Initialization of BASS
  if not BASS_Init(-1, 44100, 0, nil, nil) then
     begin
       BASSErrorMessage(GetMessageCaption('bassinit'),
       GetMessageText('bassinit'));
       Application.Terminate
     end;
  // Set the buffer
  BASS_SetConfig(BASS_CONFIG_BUFFER, 1000);
  // Volume state
  if PlayerVolume > 0 then
     begin
       Playbar.MuteUnmuteState := musUnmute;
       MuteUnmuteItem.Caption :=
       GetFormControlText('main_form', 'muteunmuteitem', 'caption_mute')
     end
  else
      begin
        Playbar.MuteUnmuteState := musMute;
        MuteUnmuteItem.Caption :=
        GetFormControlText('main_form', 'muteunmuteitem', 'caption_unmute')
      end
end;

// Load CD track to the playlist
// TrackNumber - the number of the track
procedure TMainForm.LoadCDTrack(const TrackNumber: Byte);
const
  LineBreak = #13#10;
  TitleAlbumSeparator = ' / ';
var
  CDDBInfo, TempString: String;
  TempInt, TempPos1, TempPos2, TrackPos: Integer;
  TempStream: HSTREAM;
  TempTrack: TTrack;
begin
  // Initial tag values
  with TempTrack do
       begin
        FileName := Format('CD%d', [TrackNumber + 1]);
        Artist := '';
        Title := '';
        Album := '';
        Year := '';
        Comment := '';
        Genre := '';
        Composer := '';
        Copyright := '';
        Subtitle := '';
        AlbumArtist := '';
        Track := IntToStr(TrackNumber + 1);
        Disc := ''
       end;
  // Get the track's position in the playlist
  TrackPos := Length(Tracks) - 1;
  // Get the track's info
  TempStream :=
  BASS_CD_StreamCreate(SetDrive, TrackNumber, BASS_STREAM_DECODE);
  try
    // Get CDDB disc info
    CDDBInfo := String(BASS_CD_GetID(SetDrive, BASS_CDID_CDDB_READ + 0));
    // Add the track length to the total length
    if TempStream <> 0 then
       TotalLengthByte := TotalLengthByte +
       BASS_ChannelGetLength(TempStream, BASS_POS_BYTE)
  finally
    BASS_StreamFree(TempStream)
  end;
  // Search for the disc artist/title
  TempString := 'DTITLE=';
  TempPos1 := UTF8Pos(TempString, CDDBInfo);
  if TempPos1 > 0 then
     begin
       TempPos2 :=
       UTF8Pos(TitleAlbumSeparator, CDDBInfo, TempPos1);
       TempPos1 += UTF8Length(TempString);
       TempTrack.Artist := UTF8Copy(CDDBInfo, TempPos1,
       TempPos2 - TempPos1);
       TempPos1 := UTF8Pos(LineBreak, CDDBInfo, TempPos2);
       TempPos2 += UTF8Length(TitleAlbumSeparator);
       TempTrack.Album := UTF8Copy(CDDBInfo,
       TempPos2, TempPos1 - TempPos2)
     end;
  // Search for the disc year
  TempString := 'DYEAR=';
  TempPos1 := UTF8Pos(TempString, CDDBInfo);
  if TempPos1 > 0 then
     begin
       TempPos2 := UTF8Pos(LineBreak, CDDBInfo, TempPos1);
       TempPos1 += UTF8Length(TempString);
       TempTrack.Year := UTF8Copy(CDDBInfo, TempPos1,
       TempPos2 - TempPos1)
     end;
  // Search for the disc genre
  TempString := 'DGENRE=';
  TempPos1 := UTF8Pos(TempString, CDDBInfo);
  if TempPos1 > 0 then
     begin
       TempPos2 := UTF8Pos(LineBreak, CDDBInfo, TempPos1);
       TempPos1 += UTF8Length(TempString);
       TempTrack.Genre := UTF8Copy(CDDBInfo, TempPos1,
       TempPos2 - TempPos1)
     end;
  // Search for the track title
  TempString := Format('TTITLE%d=', [TrackNumber]);
  TempPos1 := UTF8Pos(TempString, CDDBInfo);
  if TempPos1 > 0 then
     begin
       TempPos2 := UTF8Pos(LineBreak, CDDBInfo, TempPos1);
       TempPos1 += UTF8Length(TempString);
       TempTrack.Title := UTF8Copy(CDDBInfo, TempPos1,
       TempPos2 - TempPos1)
     end;
  // Search for the track comment
  TempString := Format('EXTT%d=', [TrackNumber]);
  TempPos1 := UTF8Pos(TempString, CDDBInfo);
  if TempPos1 > 0 then
     begin
       TempPos2 := UTF8Pos(LineBreak, CDDBInfo, TempPos1);
       TempPos1 += UTF8Length(TempString);
       TempTrack.Comment := UTF8Copy(CDDBInfo, TempPos1,
       TempPos2 - TempPos1)
     end;
  Tracks[TrackPos] := TempTrack;
  TempInt := StrToIntDef(TempTrack.Track, 0);
  if TempInt > 0 then
     begin
       if (TempInt < 10) and (TempTrack.Track[1] <> '0') then
          Tracks[TrackPos].Track := '0' + TempTrack.Track
       else
         Tracks[TrackPos].Track := TempTrack.Track
     end
  else
      Tracks[TrackPos].Track := '';
  TempInt := StrToIntDef(TempTrack.Disc, 0);
  if TempInt > 0 then
     begin
       if (TempInt < 10) and (TempTrack.Disc[1] <> '0') then
          Tracks[TrackPos].Disc := '0' + TempTrack.Disc
       else
         Tracks[TrackPos].Disc := TempTrack.Disc
     end
  else
      Tracks[TrackPos].Disc := ''
end;

// Load the playlist
procedure TMainForm.LoadPlaylist(const PlaylistFileName: String);
var
  Count, TempInt, TempLength: Integer;
  PlaylistFile: TFileStream;
  TempStream: HSTREAM;
  TempString: String;
begin
  // If the playlist file exists, then open it
  if FileExists(PlaylistFileName) then
     PlaylistFile := TFileStream.Create(PlaylistFileName, fmOpenRead)
  // If not, create new playlist file
  else
    PlaylistFile := TFileStream.Create(PlaylistFileName, fmCreate);
  try
    TotalLengthByte := 0;
    // If the playlist file is not empty
    if PlaylistFile.Size > 0 then
       with Playlist do
          begin
            BeginUpdate;
            // Clear the playlist
            Count := 0;
            Playlist.RootNodeCount := 0;
            // While the end of file is not reached
            while PlaylistFile.Position < PlaylistFile.Size do
              begin
                Application.ProcessMessages;
                TempString := PlaylistFile.ReadAnsiString;
                // Display the info of all the tracks
                if TempString <> '' then
                   begin
                     SetLength(Tracks, Count + 1);
                     // If not a CD
                     if UTF8Copy(TempString, 0, 2) <> 'CD' then
                        begin
                          if not IsURL(TempString) then
                             TempStream :=
                             BASS_StreamCreateFile(False,
                             PChar(TempString), 0, 0, BASS_STREAM_DECODE)
                          else
                            TempStream :=
                            BASS_StreamCreateURL(PChar(TempString), 0, 0,
                            nil, nil);
                          if TempStream <> 0 then
                             begin
                               // Get the track length
                               TempLength :=
                               BASS_ChannelGetLength(TempStream, BASS_POS_BYTE);
                               if TempLength > -1 then
                                  // Increase the total length
                                  TotalLengthByte :=
                                  TotalLengthByte + TempLength
                               else
                                 BASSErrorMessage(
                                 GetMessageCaption('couldnotgetlength'),
                                 GetMessageText('couldnotgetlength') + #13 +
                                 '[' + TempString + ']')
                             end
                          else
                              BASSErrorMessage(
                              GetMessageCaption('couldnotcreatestream'),
                              GetMessageText('countnotcreatestream') + #13 +
                              '[' + TempString + ']');
                          Tracks[Count] := GetTags(TempString);
                          TempInt := StrToIntDef(Tracks[Count].Track, 0);
                          if TempInt > 0 then
                             begin
                               if (TempInt < 10) and
                               (Tracks[Count].Track[1] <> '0') then
                                  Tracks[Count].Track :=
                                  '0' + Tracks[Count].Track
                             end
                          else
                              Tracks[Count].Track := '';
                          TempInt := StrToIntDef(Tracks[Count].Disc, 0);
                          if TempInt > 0 then
                             begin
                               if (TempInt < 10) and
                               (Tracks[Count].Disc[1] <> '0') then
                                  Tracks[Count].Disc := '0' + Tracks[Count].Disc
                             end
                          else
                              Tracks[Count].Disc := ''
                        end
                     // If a CD
                     else
                         begin
                           LoadCDTrack(StrToIntDef(UTF8Copy(TempString, 3,
                           UTF8Length(TempString) - 3), -1))
                         end;
                     Inc(Count)
                   end
              end;
              EndUpdate;
              Playlist.RootNodeCount := Length(Tracks)
          end;
    // Get the full playlist duration
    if TotalLengthByte > 0 then
       TotalTrackLength := GetLengthFromByte(TempStream, TotalLengthByte)
    else
      TotalTrackLength := '';
    BASS_StreamFree(TempStream)
  finally
    PlaylistFile.Free
  end;
  // Display the duration of all the tracks in the playlist
  StatusBar1.Panels[3].Text := TotalTrackLength
end;

// "Load Playlist" menu item
procedure TMainForm.LoadPlaylistItemClick(Sender: TObject);
var
  DefPlaylistFile, InputPlaylistFile: TFileStream;
  TempString: String;
begin
  if LoadPlaylistDialog.Execute then
     begin
       DefPlaylistFile :=
       TFileStream.Create(DefaultPlaylistFileName, fmOpenWrite);
       try
         InputPlaylistFile :=
         TFileStream.Create(LoadPlaylistDialog.FileName, fmOpenRead);
         try
           // Add all the selected files to the playlist
           while InputPlaylistFile.Position < InputPlaylistFile.Size do
              begin
               TempString := InputPlaylistFile.ReadAnsiString;
               DefPlaylistFile.WriteAnsiString(TempString)
              end;
         finally
           InputPlaylistFile.Free
         end
       finally
         DefPlaylistFile.Free
       end;
       LoadPlaylist(DefaultPlaylistFileName)
     end
end;

// On clicking the "Lyrics" button
procedure TMainForm.LyricsButtonClick(Sender: TObject);
begin
  if LyricsForm.Showing then
     LyricsForm.Hide
  else
    LyricsForm.Show
end;

procedure TMainForm.MenuItem1Click(Sender: TObject);
begin
  TogglePlaylistColumn(MenuItem1)
end;

procedure TMainForm.MenuItem2Click(Sender: TObject);
begin
  TogglePlaylistColumn(MenuItem2)
end;

procedure TMainForm.MenuItem3Click(Sender: TObject);
begin
  TogglePlaylistColumn(MenuItem3)
end;

procedure TMainForm.MenuItem4Click(Sender: TObject);
begin
  TogglePlaylistColumn(MenuItem4)
end;

procedure TMainForm.MenuItem5Click(Sender: TObject);
begin
  TogglePlaylistColumn(MenuItem5)
end;

procedure TMainForm.MenuItem6Click(Sender: TObject);
begin
  TogglePlaylistColumn(MenuItem6)
end;

procedure TMainForm.MenuItem7Click(Sender: TObject);
begin
  TogglePlaylistColumn(MenuItem7)
end;

procedure TMainForm.MenuItem8Click(Sender: TObject);
begin
  TogglePlaylistColumn(MenuItem8)
end;

procedure TMainForm.MenuItem9Click(Sender: TObject);
begin
  TogglePlaylistColumn(MenuItem9)
end;

procedure TMainForm.MenuItem10Click(Sender: TObject);
begin
  TogglePlaylistColumn(MenuItem10)
end;

procedure TMainForm.MenuItem11Click(Sender: TObject);
begin
  TogglePlaylistColumn(MenuItem11)
end;

procedure TMainForm.MenuItem12Click(Sender: TObject);
begin
  TogglePlaylistColumn(MenuItem12)
end;

procedure TMainForm.MenuItem13Click(Sender: TObject);
begin
  TogglePlaylistColumn(MenuItem13)
end;

// Mute or unmute the sound
procedure TMainForm.MuteUnmute;
begin
  // If the volume is not muted, then mute it
  if PlayerVolume > 0 then
     begin
       TempPlayerVolume := PlayerVolume;
       PlayerVolume := 0;
       VolumeBar.Position := 0;
       Playbar.MuteUnmuteState := musMute;
       MuteUnmuteItem.Caption :=
       GetFormControlText('main_form', 'muteunmuteitem', 'caption_unmute')
     end
  // If the volume is muted, then unmute it
  else
    begin
      if TempPlayerVolume > 0 then
         PlayerVolume := TempPlayerVolume
      else
        PlayerVolume := 100;
      Playbar.MuteUnmuteState := musUnmute;
      MuteUnmuteItem.Caption :=
       GetFormControlText('main_form', 'muteunmuteitem', 'caption_mute')
    end;
  // Change the volume bar
  VolumeBar.Position := Round(PlayerVolume * 100);
  // Set the volume
  if PlayerStream <> 0 then
     BASS_ChannelSetAttribute(PlayerStream, BASS_ATTRIB_VOL, PlayerVolume)
end;

// "Mute"/"Unmute" menu item
procedure TMainForm.MuteUnmuteItemClick(Sender: TObject);
begin
  MuteUnmute
end;

// Pause current track
procedure TMainForm.PauseCurrent;
begin
  // Pause the playback
  BASS_ChannelPause(PlayerStream);
  PlayPauseItem.Caption :=
  GetFormControlText('main_form', 'playpauseitem', 'caption_play');
  StatusBar1.Panels[0].Text :=
  GetFormControlText('main_form', 'statusbar', 'playstate_paused');
  StatusBar1.Panels[1].Text :=
  StatusBarWriteFromFile(Tracks[PlaylistPosition].FileName);
  // Set the "play" button image to "play"
  Playbar.PlayPauseState := ppsPlay
end;

// "Mute"/"Unmute" button
procedure TMainForm.PlaybarMuteUnmuteClick(Sender: TObject);
begin
  MuteUnmute
end;

// "Next" button
procedure TMainForm.PlaybarNextClick(Sender: TObject);
begin
  GoForward
end;

// "Play"/"Pause" button
procedure TMainForm.PlaybarPlayPauseClick(Sender: TObject);
begin
  PlayPause
end;

// "Prev" button
procedure TMainForm.PlaybarPrevClick(Sender: TObject);
begin
  GoBack
end;

// "Stop" button
procedure TMainForm.PlaybarStopClick(Sender: TObject);
begin
  StopPlayback(True)
end;

// On clicking "Play CD" button
procedure TMainForm.PlayCDButtonClick(Sender: TObject);
begin
  // Open the Play CD form
  CDForm.Show
end;

// Play current track
procedure TMainForm.PlayCurrent(Restart: Boolean);
var
  CoverImageFile, CoverImageTag, LyricsString, TempString: String;
  CoverImageStream: TStringStream;
  Mark: BASS_MIDI_MARK;
  SoundFont: BASS_MIDI_FONT;
  TrackNumber: Integer;
begin
  // To resume the playback of the existing stream
  if not Restart then
     begin
       BASS_ChannelPlay(PlayerStream, False);
       PlayPauseItem.Caption :=
       GetFormControlText('main_form', 'playpauseitem', 'caption_pause');
       StatusBar1.Panels[0].Text :=
       GetFormControlText('main_form', 'statusbar', 'playstate_playing');
       StatusBar1.Panels[1].Text :=
       StatusBarWriteFromFile(Tracks[PlaylistPosition].FileName);
       Playbar.PlayPauseState := ppsPause
     end
  else
      begin
       TempString := Tracks[PlaylistPosition].FileName;
       // Create the appropriate stream
       if IsCDTrack(TempString) then
          begin
            TrackNumber :=
            StrToIntDef(Tracks[PlaylistPosition].Track, 0) - 1;
            // If the floating point playback is supported, then use it
            if FloatingPointSupport then
               PlayerStream := BASS_CD_StreamCreate(SetDrive,
               TrackNumber, BASS_STREAM_AUTOFREE and BASS_SAMPLE_FLOAT)
            // If the floating point playback is not supported,
            // then do not use it
            else
              PlayerStream :=
              BASS_CD_StreamCreate(SetDrive, TrackNumber, BASS_STREAM_AUTOFREE)
          end
       else
           begin
             if IsMIDI(TempString) then
                begin
                  // Load the sound font
                  if FileExists(SFPath) then
                     SoundFont.font := BASS_MIDI_FontInit(PChar(SFPath), 0);
                  if SoundFont.font <> 0 then
                     begin
                       SoundFont.preset := -1;
                       SoundFont.bank := 0;
                       BASS_MIDI_StreamSetFonts(
                       0, PBASS_MIDI_FONT(@SoundFont), 1)
                     end
                  else
                      begin
                        BASSErrorMessage(GetMessageCaption('couldnotloadsf'),
                        GetMessageText('couldnotloadsf'));
                        Exit
                      end;
                  BASS_SetConfig(BASS_CONFIG_MIDI_AUTOFONT, 1);
                  // If the floating point playback is supported, then use it
                  if FloatingPointSupport then
                     PlayerStream :=
                     BASS_MIDI_StreamCreateFile(False, PChar(TempString), 0, 0,
                     BASS_SAMPLE_FLOAT and BASS_STREAM_AUTOFREE, 1)
                  // If the floating point playback is not supported,
                  // then do not use it
                  else
                    PlayerStream := BASS_MIDI_StreamCreateFile(False,
                    PChar(TempString), 0, 0, BASS_STREAM_AUTOFREE, 1)
                end
             else
                 begin
                   // If the floating point playback is supported, then use it
                   if FloatingPointSupport then
                      PlayerStream := BASS_StreamCreateFile(False,
                      PChar(TempString), 0, 0, BASS_STREAM_AUTOFREE and
                      BASS_SAMPLE_FLOAT)
                   // If the floating point playback is not supported,
                   // then do not use it
                   else
                     PlayerStream := BASS_StreamCreateFile(False,
                     PChar(TempString), 0, 0, BASS_STREAM_AUTOFREE);
                 end
           end;
           // If the stream is created, then start/resume the playback
           if (PlayerStream <> 0) and BASS_ChannelPlay(PlayerStream, True) then
              begin
                // Set the volume
                BASS_ChannelSetAttribute(PlayerStream, BASS_ATTRIB_VOL,
                PlayerVolume);
                // Display the played track
                PlayPauseItem.Caption := GetFormControlText('main_form',
                'playpauseitem', 'caption_pause');
                StatusBar1.Panels[0].Text := GetFormControlText('main_form',
                'statusbar', 'playstate_playing');
                StatusBar1.Panels[1].Text :=
                StatusBarWriteFromFile(TempString);
                // Reset the playback time and get the track length
                HH := 0;
                MM := 0;
                SS := 0;
                CurrentTrackLength := GetTrackLength(PlayerStream);
                // Set the "play" button image to "pause"
                Playbar.PlayPauseState := ppsPause;
                // Enable the ability to change the playback position
                // and set its initial length
                PlaybackBar.Max :=
                BASS_ChannelGetLength(PlayerStream, BASS_POS_BYTE);
                PlaybackBar.Visible := True;
                // Set cover image from tag, if available
                CoverImageTag := GetCoverImageTag(TempString);
                if CoverImageTag <> '' then
                   begin
                     CoverImageStream := TStringStream.Create(CoverImageTag);
                     CoverImage.Picture.LoadFromStream(CoverImageStream);
                     CoverImageLoaded := True;
                     CoverImageStream.Free
                   end
                else
                    begin
                      // Set cover image file, if available
                      CoverImageFile :=
                      GetCoverImageFile(GetFilePath(TempString));
                      if CoverImageFile <> '' then
                         begin
                           CoverImage.Picture.LoadFromFile(CoverImageFile);
                           CoverImageLoaded := True
                         end
                      else
                          CoverImageLoaded := False
                    end;
                // If the file contains lyrics
                LyricsForm.LyricsMemo.Text := '';
                LyricsString := '';
                if IsMIDI(TempString) then
                   begin
                     Mark.text := '';
                     if BASS_MIDI_StreamGetMark(PlayerStream,
                     BASS_MIDI_MARK_LYRIC, 0, Mark) then
                        BASS_ChannelSetSync(PlayerStream,
                        BASS_SYNC_MIDI_MARK, BASS_MIDI_MARK_LYRIC,
                        @MIDILyricSync, LyricsForm.LyricsMemo)
                     else
                        begin
                          if BASS_MIDI_StreamGetMark(PlayerStream,
                          BASS_MIDI_MARK_TEXT, 20, Mark) then
                             BASS_ChannelSetSync(PlayerStream,
                             BASS_SYNC_MIDI_MARK, BASS_MIDI_MARK_TEXT,
                             @MIDITextSync, LyricsForm.LyricsMemo)
                          else
                            BASS_ChannelSetSync(PlayerStream, BASS_SYNC_END, 0,
                            @MIDIEndSync, LyricsForm.LyricsMemo);
                          LyricsString := Mark.text
                        end
                   end
                else
                  LyricsString := GetLyrics(Tracks[PlaylistPosition].FileName);
                if (LyricsString <> '') and (LyricsString <> 'N') then
                   begin
                     LyricsForm.LyricsMemo.Text := LyricsString;
                     LyricsButton.Visible := True;
                     LyricsForm.Show
                   end
                else
                    begin
                      LyricsButton.Visible := False;
                      LyricsForm.Hide
                    end;
                 // Start the timer
                 PlaybackTimer.Enabled := True
               end
              // If the stream was not created or the playback could not start
              else
                  BASSErrorMessage(GetMessageCaption('couldnotstartplayback'),
                  GetMessageText('couldnotstartplayback'))
            end
end;

// "Play"/"Pause"
procedure TMainForm.PlayPause;
var
  TempNode: PVirtualNode;
begin
  case BASS_ChannelIsActive(PlayerStream) of
       // If the playback is stopped or not started
       BASS_ACTIVE_STOPPED:
                           // If the playlist is not empty
                           if Playlist.RootNodeCount > 0 then
                              begin
                                TempNode := Playlist.GetFirstSelected(False);
                                // If no track is selected,
                                // then set the first track for the playback
                                if TempNode = nil then
                                   TempNode := Playlist.GetFirst(False);
                                PlaylistPosition := Round(TempNode^.Index);
                                // Play current track
                                PlayCurrent(True)
                              end;
       // If the playback is active, then pause the playback
       BASS_ACTIVE_PLAYING: PauseCurrent;
       // If the playback is paused, then resume the playback
       BASS_ACTIVE_PAUSED: PlayCurrent(False)
  end
end;

// "Play"/"Pause" menu item
procedure TMainForm.PlayPauseItemClick(Sender: TObject);
begin
  PlayPause
end;

// On changing the playback position
procedure TMainForm.PlaybackBarChange(Sender: TObject);
begin
  // If the position is being changed automatically, then do nothing
  if PlaybackBarAutoUpdate then
     PlaybackBarAutoUpdate := False
  // If the position is being changed manually,
  // then change the playback position to the one corresponding to
  // the playback bar position
  else
     if PlayerStream <> 0 then
        BASS_ChannelSetPosition(PlayerStream, PlaybackBar.Position,
        BASS_POS_BYTE)
end;

// On the playback timer
procedure TMainForm.PlaybackTimerTimer(Sender: TObject);
var
  LenByte: QWORD;
  PEQ: ^BASS_DX8_PARAMEQ;
  PosByte: QWORD;
  PosSec: Double;
  TempString: String[17];
begin
  // Get the streaming track position in bytes
  PosByte := BASS_ChannelGetPosition(PlayerStream, BASS_POS_BYTE);
  // If the position in bytes could be retrieved
  if PosByte >= 0 then
     begin
       // Get the streaming track position in seconds
       PosSec := BASS_ChannelBytes2Seconds(PlayerStream, PosByte);
       // If the position in seconds could be retrieved
       if PosSec >= 0 then
          begin
               SS := Round(PosSec);
               // Get the minutes out of the seconds
               if SS >= 60 then
                  begin
                    MM := SS div 60;
                    SS := SS mod 60
                  end;
               // Get the hours out of the minutes
               if MM >= 60 then
                  begin
                    HH := MM div 60;
                    MM := MM mod 60
                  end
          end
     end;
  // Display hours
  if HH < 10 then
     TempString := '0' + IntToStr(HH) + ':'
  else
    TempString := IntToStr(HH) + ':';
  // Display minutes
  if MM < 10 then
     TempString := TempString + '0' + IntToStr(MM) + ':'
  else
    TempString := TempString + IntToStr(MM) + ':';
  // Display seconds
  if SS < 10 then
     TempString := TempString + '0' + IntToStr(SS)
  else
    TempString := TempString + IntToStr(SS);
  StatusBar1.Panels[2].Text := TempString + '/' + CurrentTrackLength;
  LenByte := BASS_ChannelGetLength(PlayerStream, BASS_POS_BYTE);
  PlaybackBarAutoUpdate := True;
  PlaybackBar.Position := PosByte;
  // Check if the end of file is reached
  if PosByte = LenByte then
     PlaylistEndCheck
  else
    begin
      // Check if the equalizer is on or not
      if ReadFromSettingsFile(DefaultSettingsFileName, 'EQ_STATE') = 'ON' then
         EQOn := True
      else
         EQOn := False;
      // If the equalizer is turned on
      if EQOn then
         begin
           EqualizerFX :=
           BASS_ChannelSetFX(PlayerStream, BASS_FX_DX8_PARAMEQ, 1);
           if EqualizerFX = 0 then
              BASSErrorMessage(GetMessageCaption('couldnotseteqfx'),
              GetMessageText('couldnotseteqfx'));
           New(PEQ);
           PEQ^ := Equalizer;
           if not BASS_FXGetParameters(EqualizerFX, PEQ) then
              BASSErrorMessage(GetMessageCaption('couldnotgeteqparams'),
              GetMessageText('couldnotgeteqparams'))
           else
             begin
               Equalizer.fBandwidth := StrToFloat(ReadFromSettingsFile(
               DefaultSettingsFileName, 'EQ_BANDWIDTH'));
               Equalizer.fCenter := StrToFloat(ReadFromSettingsFile(
               DefaultSettingsFileName, 'EQ_CENTER'));
               Equalizer.fGain := StrToFloat(ReadFromSettingsFile(
               DefaultSettingsFileName, 'EQ_GAIN'))
             end;
           if not BASS_FXSetParameters(EqualizerFX, PEQ) then
              BASSErrorMessage(GetMessageCaption('couldnotapplyeq'),
              GetMessageText('couldnotapplyeq'));
           Dispose(PEQ)
         end
    end
end;

// After painting the playlist cell
procedure TMainForm.PlaylistAfterCellPaint(Sender: TBaseVirtualTree;
  TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
  const CellRect: TRect);
var
  ABitmap: TBGRABitmap;
  Color1, Color2: TColor;
  CellHeight, CellWidth, LineLength, XMargin, YMargin: Integer;
  TempPoints: Array[0..2] of TPointF;
begin
  // Draw playback state symbol
  if (Column = 0) and (PlaylistPosition = Round(Node^.Index)) then
     begin
       // Determine the colors
       if vsSelected in Node^.States then
          begin
            Color1 := Sender.Color;
            Color2 := Sender.Font.Color
          end
       else
         begin
           Color1 := Sender.Font.Color;
           Color2 := Sender.Color
         end;
       // Determine the cell size
       CellHeight := CellRect.Bottom - CellRect.Top - 2;
       CellWidth := CellRect.Right - CellRect.Left - 1;
       ABitmap := TBGRABitmap.Create(CellWidth, CellHeight, Color2);
       try
         LineLength := Max(1, Min(CellWidth, CellHeight) - 2);
         case BASS_ChannelIsActive(PlayerStream) of
              BASS_ACTIVE_PAUSED:
                // Draw the pause symbol
                begin
                  XMargin := (CellWidth - LineLength*3 div 4) div 2;
                  YMargin := (CellHeight - LineLength) div 2;
                  ABitmap.RectangleAntialias(XMargin, YMargin,
                  XMargin + LineLength div 4, Max(0, CellHeight - YMargin - 1),
                  ColorToBGRA(Color2), 1, ColorToBGRA(Color1));
                  ABitmap.RectangleAntialias(Max(0, CellWidth - XMargin -
                  LineLength div 4 - 1), YMargin, Max(0, CellWidth -
                  XMargin - 1), Max(0, CellHeight - YMargin - 1),
                  ColorToBGRA(Color2), 1, ColorToBGRA(Color1))
                end;
              BASS_ACTIVE_PLAYING:
                // Draw the play symbol
                begin
                  XMargin := (CellWidth - LineLength) div 2;
                  YMargin := (CellHeight - LineLength) div 2;
                  TempPoints[0] := PointF(XMargin, YMargin);
                  TempPoints[1] := PointF(XMargin, Max(0, CellHeight -
                  YMargin - 1));
                  TempPoints[2] := PointF(XMargin + LineLength,
                  CellHeight div 2);
                  ABitmap.DrawPolygonAntialias(TempPoints, ColorToBGRA(Color2),
                  1, ColorToBGRA(Color1))
                end
         end;
         ABitmap.Draw(TargetCanvas, 0, 0)
       finally
         ABitmap.Free
       end
     end
end;

// Before painting the playlist cell
procedure TMainForm.PlaylistBeforeCellPaint(Sender: TBaseVirtualTree;
  TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
  CellPaintMode: TVTCellPaintMode; CellRect: TRect; var ContentRect: TRect);
begin
  with TargetCanvas do
       begin
        Brush.Color := Sender.Color;
        Pen.Color := TVirtualStringTree(Sender).Colors.BorderColor;
        FillRect(CellRect)
       end
end;

// On comparing the nodes
procedure TMainForm.PlaylistCompareNodes(Sender: TBaseVirtualTree; Node1,
  Node2: PVirtualNode; Column: TColumnIndex; var Result: Integer);
var
  Track1, Track2: TTrack;
begin
  Track1 := PTrack(Sender.GetNodeData(Node1))^;
  Track2 := PTrack(Sender.GetNodeData(Node2))^;
  case Playlist.Header.Columns[Column].Tag of
       1: Result := UTF8CompareText(Track1.Artist, Track2.Artist);
       2: Result := UTF8CompareText(Track1.Album, Track2.Album);
       3: Result := UTF8CompareText(Track1.Year, Track2.Year);
       4: Result := UTF8CompareText(Track1.Title, Track2.Title);
       5: Result := StrToIntDef(Track1.Track, 0) - StrToIntDef(Track2.Track, 0);
       6: Result := StrToIntDef(Track1.Disc, 0) - StrToIntDef(Track2.Disc, 0);
       7: Result := UTF8CompareText(Track1.Genre, Track2.Genre);
       8: Result := UTF8CompareText(Track1.Comment, Track2.Comment);
       9: Result := UTF8CompareText(Track1.Composer, Track2.Composer);
       10: Result := UTF8CompareText(Track1.Copyright, Track2.Copyright);
       11: Result := UTF8CompareText(Track1.Subtitle, Track2.Subtitle);
       12: Result := UTF8CompareText(Track1.AlbumArtist, Track2.AlbumArtist);
       13: Result := UTF8CompareText(Track1.FileName, Track2.FileName)
  end
end;

// Drag'n'dropping on the playlist
procedure TMainForm.PlaylistDragDrop(Sender: TBaseVirtualTree; Source: TObject;
  DataObject: IDataObject; Formats: TFormatArray; Shift: TShiftState;
  const Pt: TPoint; var Effect: LongWord; Mode: TDropMode);
var
  Level, SourcePosition, TargetPosition: Integer;
  Node, Node1, Node2: PVirtualNode;
  TempString: String;
  TempStrings: TStringList;
begin
  // Dropping from the collection
  if Source = CollectionTree then
     with Source as TVirtualStringTree do
       begin
         TempStrings := TStringList.Create;
         Node := GetFirstSelected(False);
         if Node = nil then
            Exit;
         Level := GetNodeLevel(Node);
         repeat
            case Level of
               0:
                 // Dropping the artist
                 begin
                   Expanded[Node] := True;
                   Node1 := Node^.FirstChild;
                   repeat
                      Expanded[Node1] := True;
                      Node2 := Node1^.FirstChild;
                      repeat
                         TempStrings.Add(Artists[Node^.Index].
                         Albums[Node1^.Index].Tracks[Node2^.Index].FileName);
                         Node2 := Node2^.NextSibling
                      until (Node2 = nil);
                      Node1 := Node1^.NextSibling
                   until (Node1 = nil)
                 end;
               1:
                 // Dropping the album
                 begin
                   Expanded[Node] := True;
                   Node2 := Node^.FirstChild;
                   repeat
                      TempStrings.Add(Artists[Node2^.Parent^.Parent^.Index].
                      Albums[Node2^.Parent^.Index].
                      Tracks[Node2^.Index].FileName);
                      Node2 := Node2^.NextSibling
                   until (Node2 = nil)
                 end;
               2:
                 //Dropping the track
                 TempStrings.Add(Artists[Node^.Parent^.Parent^.Index].
                 Albums[Node^.Parent^.Index].
                 Tracks[Node^.Index].FileName);
            end;
            Node := GetNextSelected(Node, False)
         until Node = nil;
         // Add all the available files to the playlist
         AddFilesToPlaylist(TempStrings, DefaultPlaylistFileName);
         TempStrings.Free
       end;
  // Drag'n'dropping within the playlist
  if Source = Sender then
     begin
       with Source as TVirtualStringTree do
         begin
           Node := GetFirstSelected(False);
           if Node = nil then
              Exit;
           Node1 := Sender.DropTargetNode;
           TargetPosition := Sender.DropTargetNode^.Index;
           repeat
              SourcePosition := Node^.Index;
              TempString := PTrack(Sender.GetNodeData(Node))^.FileName;
              RemoveFileFromPlaylist(DefaultPlaylistFileName, SourcePosition);
              AddFileToPlaylistPosition(TempString, DefaultPlaylistFileName,
              TargetPosition);
              if SourcePosition < TargetPosition then
                 Sender.MoveTo(Node, Node1, amInsertAfter, False)
              else
                Sender.MoveTo(Node, Node1, amInsertBefore, False);
              Inc(TargetPosition);
              Node1 := Sender.GetNext(Node1);
              Node^.States := Node^.States - [vsSelected];
              Node := GetFirstSelected(False)
           until Node = nil
         end
     end;
  // Reload the playlist
  LoadPlaylist(DefaultPlaylistFileName)
end;

// Dragging over the playlist
procedure TMainForm.PlaylistDragOver(Sender: TBaseVirtualTree; Source: TObject;
  Shift: TShiftState; State: TDragState; const Pt: TPoint; Mode: TDropMode;
  var Effect: LongWord; var Accept: Boolean);
begin
  // Only accepts dragging over within the playlist or from the collection tree
  Accept := (Sender = Playlist) or (Source = CollectionTree)
end;

// On getting the appropriate playlist popup menu
procedure TMainForm.PlaylistGetPopupMenu(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; const P: TPoint;
  var AskParent: Boolean; var APopupMenu: TPopupMenu);
begin
  if Playlist.Header.InHeader(P) then
     APopupMenu := PlaylistHeaderPopupMenu
  else
    APopupMenu := PlaylistPopupMenu
end;

// Get the playlist cell text
procedure TMainForm.PlaylistGetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: String);
var
  Data: TTrack;
begin
  Data := PTrack(Sender.GetNodeData(Node))^;
  case Playlist.Header.Columns[Column].Tag of
          1: CellText := Data.Artist;
          2: CellText := Data.Album;
          3: CellText := Data.Year;
          4: CellText := Data.Title;
          5: CellText := Data.Track;
          6: CellText := Data.Disc;
          7: CellText := Data.Genre;
          8: CellText := Data.Comment;
          9: CellText := Data.Composer;
          10: CellText := Data.Copyright;
          11: CellText := Data.Subtitle;
          12: CellText := Data.AlbumArtist;
          13: CellText := Data.FileName
  end
end;

// On clicking the playlist header
procedure TMainForm.PlaylistHeaderClick(Sender: TVTHeader;
  Column: TColumnIndex; Button: TMouseButton; Shift: TShiftState; X, Y: Integer
  );
begin
  if Button = mbLeft then
     with Sender do
       begin
         if SortColumn = 0 then
            Exit;
         if SortColumn <> Column then
            begin
              SortColumn := Column;
              SortDirection := sdAscending
            end
         else
             if SortDirection = sdAscending then
                SortDirection := sdDescending
             else
               SortDirection := sdAscending;
         Playlist.SortTree(SortColumn, SortDirection, False)
       end
end;

// On drawing the playlist header
procedure TMainForm.PlaylistHeaderDraw(Sender: TVTHeader;
  HeaderCanvas: TCanvas; Column: TVirtualTreeColumn; const R: TRect; Hover,
  Pressed: Boolean; DropMark: TVTDropMarkMode);
var
  TempPoints: Array[0..3] of TPoint;
  TempRect: TRect;
begin
  with HeaderCanvas do
       begin
         Brush.Color := AppTheme.Colour1;
         Font.Color := AppTheme.FontColour1;
         Pen.Color := AppTheme.Colour2;
         TempRect := R;
         DrawEdge(HeaderCanvas.Handle, TempRect, EDGE_SUNKEN, BF_ADJUST or
         BF_RECT);
         InflateRect(TempRect, 1, 1);
         FillRect(TempRect);
         if Column.Index <> Sender.SortColumn then
            TextRect(TempRect, TempRect.Left + PlaylistHeaderMargin,
            TempRect.Top, Column.Text)
         else
           begin
             if Sender.SortDirection = sdAscending then
                begin
                  TempPoints[0] :=
                  Point(TempRect.Left + PlaylistHeaderMargin,
                  (TempRect.Bottom - PlaylistHeaderSide) div 2);
                  TempPoints[1] :=
                  Point(TempRect.Left + PlaylistHeaderMargin +
                  PlaylistHeaderSide,
                  (TempRect.Bottom - PlaylistHeaderSide) div 2);
                  TempPoints[2] :=
                  Point(TempRect.Left + PlaylistHeaderMargin +
                  PlaylistHeaderSide div 2, TempRect.Bottom -
                  (TempRect.Bottom - PlaylistHeaderSide) div 2);
                  TempPoints[3] := TempPoints[0]
                end
             else
               begin
                 TempPoints[0] :=
                 Point(TempRect.Left + PlaylistHeaderMargin, TempRect.Bottom -
                 (TempRect.Bottom - PlaylistHeaderSide) div 2);
                 TempPoints[1] :=
                 Point(TempRect.Left + PlaylistHeaderMargin +
                 PlaylistHeaderSide, TempRect.Bottom -
                 (TempRect.Bottom - PlaylistHeaderSide) div 2);
                 TempPoints[2] :=
                 Point(TempRect.Left + PlaylistHeaderMargin +
                 PlaylistHeaderSide div 2,
                 (TempRect.Bottom - PlaylistHeaderSide) div 2);
                 TempPoints[3] := TempPoints[0]
               end;
             Brush.Color := Font.Color;
             Polygon(TempPoints);
             TextRect(TempRect, TempRect.Left + PlaylistHeaderSide +
             PlaylistHeaderMargin*2, TempRect.Top,
             Column.Text)
           end
       end
end;

// On accessing the playlist header popup menu
procedure TMainForm.PlaylistHeaderPopupMenuPopup(Sender: TObject);
var
  Iterator1, Iterator2, Pos: Integer;
begin
  for Iterator1 := 1 to Playlist.Header.Columns.Count - 1 do
     begin
       Pos := Playlist.Header.Columns[Iterator1].Position;
       PlaylistHeaderPopupMenu.Items[Pos - 1].Caption :=
       Playlist.Header.Columns[Iterator1].Text
     end;
  for Iterator1 := 0 to PlaylistHeaderPopupMenu.Items.Count - 1 do
     for Iterator2 := 0 to Playlist.Header.Columns.Count - 1 do
        if PlaylistHeaderPopupMenu.Items[Iterator1].Caption =
        Playlist.Header.Columns[Iterator2].Text then
           begin
             if coVisible in Playlist.Header.Columns[Iterator2].Options then
                PlaylistHeaderPopupMenu.Items[Iterator1].Checked := True
             else
               PlaylistHeaderPopupMenu.Items[Iterator1].Checked := False;
             Break
           end
end;

// On initializing a playlist node
procedure TMainForm.PlaylistInitNode(Sender: TBaseVirtualTree; ParentNode,
  Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
begin
  PTrack(Sender.GetNodeData(Node))^ := Tracks[Node^.Index]
end;

// On double clicking the playlist
procedure TMainForm.PlaylistDblClick(Sender: TObject);
var
  TempNode: PVirtualNode;
begin
  TempNode := Playlist.GetFirstSelected(False);
  // If any item is selected in the playlist, then play the corresponding file
  if TempNode <> nil then
     begin
       StopPlayback(True);
       PlaylistPosition := Round(TempNode^.Index);
       PlayCurrent(True)
     end
end;

// Check if the end of the playlist is reached
procedure TMainForm.PlaylistEndCheck;
begin
  // If the end is not reached, then play the next song
  if PlaylistPosition < (Round(Playlist.RootNodeCount) - 1) then
     PlaylistMove(1)
  // Else stop the playback
  else
     StopPlayback(True)
end;

// On pressing a key
procedure TMainForm.PlaylistKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  // On pressing the Delete key remove all the selected files from the playlist
  if Key = VK_DELETE then
     RemoveFiles(DefaultPlaylistFileName)
end;

// Move back or forward in the playlist
procedure TMainForm.PlaylistMove(Count: Integer);
begin
  // Check if the playlist is not empty
  if Playlist.RootNodeCount > 0 then
     begin
       StopPlayback(False);
       // Change the position in the playlist
       PlaylistPosition := PlaylistPosition + Count;
       // The position cannot go below 1
       if PlaylistPosition < 0 then
          begin
            PlaylistPosition := 0;
            PlayCurrent(True)
          end
       else
           // The position cannot go above the number of files in the playlist
           if PlaylistPosition >= Round(Playlist.RootNodeCount) then
              begin
                PlaylistPosition :=  Round(Playlist.RootNodeCount) - 1;
                PlayCurrent(True)
              end
           // If the position is correct, play the new file
           else
               begin
                PlaybackTimer.Enabled := False;
                PlayCurrent(True)
               end
     end
end;

// On accessing the playlist popup menu
procedure TMainForm.PlaylistPopupMenuPopup(Sender: TObject);
var
  IsValid: Boolean;
  TempNode: PVirtualNode;
  TempString1, TempString2: String;
begin
  IsValid := False;
  GetCDDBInfoItem.Visible := False;
  TempNode := Playlist.GetFirstSelected(False);
  // Check if any item is selected
  if (TempNode <> nil) and not IsCDTrack(Tracks[TempNode^.Index].FileName) then
     begin
       TempString1 :=
       UpperCase(ExtractFileExt(Tracks[TempNode^.Index].FileName));
       // Check if the file format allows editing tags
       for TempString1 in EditTagExt do
          begin
           IsValid := True;
           TempNode := Playlist.GetNextSelected(TempNode, False);
           while (TempNode <> nil) and IsValid do
              begin
               TempString2 :=
               UpperCase(ExtractFileExt(Tracks[TempNode^.Index].FileName));
               IsValid := False;
               for TempString2 in EditTagExt do
                  IsValid := True;
               TempNode := Playlist.GetNextSelected(TempNode, False)
              end
           end;
       // If it's possible to edit lyrics or tags, enable those options
       if IsValid then
          begin
            EditLyricsItem.Enabled := True;
            EditTagsItem.Enabled := True
          end
       // If not, disable them
       else
           begin
            EditLyricsItem.Enabled := False;
            EditTagsItem.Enabled := False
           end;
       // Renaming files is enabled for any file format except CD track
       RenameFilesItem.Enabled := True
     end
  // If no item is selected or a CD track is selected, disable all the options
  else
      begin
        if (TempNode <> nil) and
        IsCDTrack(Tracks[TempNode^.Index].FileName) then
           GetCDDBInfoItem.Visible := True;
        EditLyricsItem.Enabled := False;
        EditTagsItem.Enabled := False;
        RenameFilesItem.Enabled := False
      end
end;

// On program timer display real time CPU usage
procedure TMainForm.ProgramTimerTimer(Sender: TObject);
begin
  CPUUsageLabel.Caption := GetFormControlText('main_form', 'cpuusagelabel',
  'caption') + FloatToStrF(BASS_GetCPU, ffFixed, 4, 2)
end;

// Remove files from the playlist
procedure TMainForm.RemoveFiles(const PlaylistFileName: String);
var
  TempNode: PVirtualNode;
begin
  TempNode := Playlist.GetFirstSelected(False);
  Playlist.BeginUpdate;
  // Check if any item is selected
  while TempNode <> nil do
     begin
       // Remove the file only if it's not in a playback
       if not (((BASS_ChannelIsActive(PlayerStream) = BASS_ACTIVE_PLAYING) or
       (BASS_ChannelIsActive(PlayerStream) = BASS_ACTIVE_PAUSED)) and
       (PlaylistPosition = Round(TempNode^.Index))) then
          begin
            if PlaylistPosition > Round(TempNode^.Index) then
               PlaylistPosition := PlaylistPosition - 1;
            RemoveFileFromPlaylist(PlaylistFileName, Round(TempNode^.Index));
            SetLength(Tracks, Length(Tracks) - 1);
            Playlist.DeleteNode(TempNode, True);
            TempNode := Playlist.GetFirstSelected(False)
          end
       // If it's in a playback, try to get the next one
       else
           TempNode := Playlist.GetNextSelected(TempNode, False)
     end;
  Playlist.EndUpdate;
  // Reload the playlist
  LoadPlaylist(DefaultPlaylistFileName)
end;

// "Remove Files" button
procedure TMainForm.RemoveFilesButtonClick(Sender: TObject);
begin
  RemoveFiles(DefaultPlaylistFileName)
end;

// "Remove Files" menu item
procedure TMainForm.RemoveFilesItemClick(Sender: TObject);
begin
  RemoveFiles(DefaultPlaylistFileName)
end;

// On clicking "Rename Files" open the "Rename Files" form
procedure TMainForm.RenameFilesItemClick(Sender: TObject);
var
  FileListItem: TListItem;
  TempNode: PVirtualNode;
begin
  // Check all the selected items
  TempNode := Playlist.GetFirstSelected(False);
  if TempNode <> nil then
     begin
       // Fill out the file list
       with RenameFilesForm.FileListNow do
            begin
             BeginUpdate;
             Clear;
             repeat
               FileListItem := Items.Add;
               FileListItem.SubItems.Add(Tracks[TempNode^.Index].Title);
               FileListItem.SubItems.Add(Tracks[TempNode^.Index].Subtitle);
               FileListItem.SubItems.Add(Tracks[TempNode^.Index].Artist);
               FileListItem.SubItems.Add(Tracks[TempNode^.Index].AlbumArtist);
               FileListItem.SubItems.Add(Tracks[TempNode^.Index].Album);
               FileListItem.SubItems.Add(Tracks[TempNode^.Index].Year);
               FileListItem.SubItems.Add(Tracks[TempNode^.Index].Track);
               FileListItem.SubItems.Add(Tracks[TempNode^.Index].Disc);
               FileListItem.SubItems.Add(Tracks[TempNode^.Index].Genre);
               FileListItem.SubItems.Add(Tracks[TempNode^.Index].Comment);
               FileListItem.SubItems.Add(Tracks[TempNode^.Index].Composer);
               FileListItem.SubItems.Add(Tracks[TempNode^.Index].Copyright);
               FileListItem.SubItems.Add(Tracks[TempNode^.Index].FileName);
               // Position in the playlist
               FileListItem.SubItems.Add(IntToStr(TempNode^.Index));
               // File name
               FileListItem.Caption :=
               GetShortFileName(Tracks[TempNode^.Index].FileName);
               TempNode := Playlist.GetNextSelected(TempNode, False)
	     until TempNode = nil;
             EndUpdate
            end
     end;
  RenameFilesForm.Show
end;

// "Save Playlist" menu item
procedure TMainForm.SavePlaylistItemClick(Sender: TObject);
var
  DefPlaylistFile, OutputPlaylistFile: TFileStream;
  TempString: String;
begin
  if SavePlaylistDialog.Execute then
     begin
       DefPlaylistFile :=
       TFileStream.Create(DefaultPlaylistFileName, fmOpenRead);
       try
          OutputPlaylistFile :=
          TFileStream.Create(SavePlaylistDialog.FileName, fmCreate);
          try
             // Add all the selected files to the playlist
             while DefPlaylistFile.Position < DefPlaylistFile.Size do
                begin
                 TempString := DefPlaylistFile.ReadAnsiString;
                 OutputPlaylistFile.WriteAnsiString(TempString)
                end
          finally
            OutputPlaylistFile.Free
          end
       finally
         DefPlaylistFile.Free
       end
     end
end;

// On clicking "Settings" open the "Settings" form
procedure TMainForm.SettingsItemClick(Sender: TObject);
begin
  SettingsForm.Show
end;

// Show the collection tree
procedure TMainForm.ShowCollection;
begin
  if CollectionTree.Width = 1 then
     CollectionTree.Width := CollectionWidth;
  Width := Width + CollectionTree.Width;
  Left := Left - CollectionTree.Width;
  CollectionTree.Visible := True
end;

// "Stop" menu item
procedure TMainForm.StopItemClick(Sender: TObject);
begin
  StopPlayback(True)
end;

// Stop the playback
// PositionReset - whether reset the playback position or not
procedure TMainForm.StopPlayback(const PositionReset: Boolean);
begin
  // If the playback could be stopped, then stop the timer and clear
  // the displayed time and the cover image
  if BASS_ChannelStop(PlayerStream) or
  (BASS_ChannelIsActive(PlayerStream) = BASS_ACTIVE_STOPPED) then
     begin
       PlaybackTimer.Enabled := False;
       PlaybackBar.Visible := False;
       Playbar.PlayPauseState := ppsPlay;
       if PositionReset then
          PlaylistPosition := -1;
       PlayPauseItem.Caption :=
       GetFormControlText('main_form', 'playpauseitem', 'caption_play');
       CoverImage.Picture.Clear;
       CoverImageLoaded := False;
       if LyricsForm.Showing then
          LyricsForm.Close;
       if LyricsButton.Visible then
          LyricsButton.Visible := False;
       StatusBar1.Panels[0].Text := '';
       StatusBar1.Panels[1].Text := '';
       StatusBar1.Panels[2].Text := ''
     end
  else
      BASSErrorMessage(GetMessageCaption('couldnotstopplayback'),
      GetMessageText('couldnotstopplayback'));
  Playlist.Repaint;
  // If the CD/DVD drive is locked, unlock it
  if BASS_CD_DoorIsLocked(SetDrive) then
     BASS_CD_Door(SetDrive, BASS_CD_DOOR_UNLOCK)
end;

// Toggles the playlist column visibility
// MenuItem - the playlist header popup menu item
procedure TMainForm.TogglePlaylistColumn(MenuItem: TMenuItem);
var
  Iterator: Integer;
begin
  MenuItem.Checked := not MenuItem.Checked;
  for Iterator:= 0 to Playlist.Header.Columns.Count - 1 do
     if MenuItem.Caption = Playlist.Header.Columns[Iterator].Text then
        begin
          if MenuItem.Checked then
             Playlist.Header.Columns[Iterator].Options :=
             Playlist.Header.Columns[Iterator].Options + [coVisible]
          else
             Playlist.Header.Columns[Iterator].Options :=
             Playlist.Header.Columns[Iterator].Options - [coVisible];
          Break
        end
end;

// Update the interface language
procedure TMainForm.UpdateLanguage;
var
  Iterator: Integer;
begin
  AbtItem.Caption := GetFormControlText('main_form', 'abtitem', 'caption');
  AddFilesDialog.Filter :=
  GetFormControlText('main_form', 'addfilesdialog', 'filter') +
  '|*.aac; *.ac3; *.ape; *.flac; *.kar; *.m4a; *.mid; *.midi; *.mpc; *.mp3; ' +
  '*.mp4; *.ogg; *.rmi; *.wav';
  AddFilesDialog.Title :=
  GetFormControlText('main_form', 'addfilesdialog', 'title');
  AddFilesItem.Caption :=
  GetFormControlText('main_form', 'addfilesitem', 'caption');
  BackItem.Caption :=
  GetFormControlText('main_form', 'backitem', 'caption');
  CPUUsageLabel.Caption :=
  GetFormControlText('main_form', 'cpuusagelabel', 'caption');
  EditLyricsItem.Caption :=
  GetFormControlText('main_form', 'editlyricsitem', 'caption');
  EditTagsItem.Caption :=
  GetFormControlText('main_form', 'edittagsitem', 'caption');
  ExitItem.Caption := GetFormControlText('main_form', 'exititem', 'caption');
  FilesItem.Caption := GetFormControlText('main_form', 'filesitem', 'caption');
  ForwardItem.Caption :=
  GetFormControlText('main_form', 'forwarditem', 'caption');
  GetCDDBInfoItem.Caption :=
  GetFormControlText('main_form', 'getcddbinfoitem', 'caption');
  HelpItem.Caption := GetFormControlText('main_form', 'helpitem', 'caption');
  LoadPlaylistDialog.Filter :=
  GetFormControlText('main_form', 'loadplaylistdialog', 'filter') +
  '|*.npl';
  LoadPlaylistDialog.Title :=
  GetFormControlText('main_form', 'loadplaylistdialog', 'title');
  LoadPlaylistItem.Caption :=
  GetFormControlText('main_form', 'loadplaylistitem', 'caption');
  PlaybackItem.Caption :=
  GetFormControlText('main_form', 'playbackitem', 'caption');
  for Iterator := 0 to Playlist.Header.Columns.Count - 1 do
     case Playlist.Header.Columns[Iterator].Tag of
          1: Playlist.Header.Columns[Iterator].Text := GetTagText('artist');
          2: Playlist.Header.Columns[Iterator].Text := GetTagText('album');
          3: Playlist.Header.Columns[Iterator].Text := GetTagText('year');
          4: Playlist.Header.Columns[Iterator].Text := GetTagText('title');
          5: Playlist.Header.Columns[Iterator].Text := GetTagText('track');
          6: Playlist.Header.Columns[Iterator].Text := GetTagText('disc');
          7: Playlist.Header.Columns[Iterator].Text := GetTagText('genre');
          8: Playlist.Header.Columns[Iterator].Text := GetTagText('comment');
          9: Playlist.Header.Columns[Iterator].Text := GetTagText('composer');
          10: Playlist.Header.Columns[Iterator].Text := GetTagText('copyright');
          11: Playlist.Header.Columns[Iterator].Text := GetTagText('subtitle');
          12:
            Playlist.Header.Columns[Iterator].Text := GetTagText('albumartist');
          13: Playlist.Header.Columns[Iterator].Text := GetTagText('filename')
     end;
  PlayPauseItem.Caption :=
  GetFormControlText('main_form', 'playpauseitem', 'caption_play');
  RenameFilesItem.Caption :=
  GetFormControlText('main_form', 'renamefilesitem', 'caption');
  RemoveFilesItem.Caption :=
  GetFormControlText('main_form', 'removefilesitem', 'caption');
  SavePlaylistDialog.Filter :=
  GetFormControlText('main_form', 'saveplaylistdialog', 'filter') +
  '|*.npl';
  SavePlaylistDialog.Title :=
  GetFormControlText('main_form', 'saveplaylistdialog', 'title');
  SavePlaylistItem.Caption :=
  GetFormControlText('main_form', 'saveplaylistitem', 'caption');
  SettingsItem.Caption :=
  GetFormControlText('main_form', 'settingsitem', 'caption');
  StopItem.Caption :=
  GetFormControlText('main_form', 'stopitem', 'caption');
  if BASS_ChannelIsActive(PlayerStream) = BASS_ACTIVE_PLAYING then
     StatusBar1.Panels[0].Text :=
     GetFormControlText('main_form', 'statusbar', 'playstate_playing')
  else
      if BASS_ChannelIsActive(PlayerStream) = BASS_ACTIVE_PAUSED then
         StatusBar1.Panels[0].Text :=
         GetFormControlText('main_form', 'statusbar', 'playstate_paused');
  if PlayerVolume > 0 then
     MuteUnmuteItem.Caption :=
     GetFormControlText('main_form', 'muteunmuteitem', 'caption_mute')
  else
      MuteUnmuteItem.Caption :=
      GetFormControlText('main_form', 'muteunmuteitem', 'caption_unmute')
end;

// Update the interface theme
procedure TMainForm.UpdateTheme;
var
  Iterator: Byte;
begin
  Color := AppTheme.Colour1;
  if AppTheme.FontName <> '' then
    Font.Name := AppTheme.FontName
  else
    Font.Name := 'default';
  Font.Color := AppTheme.FontColour1;
  AddFilesButton.BackgroundColor := AppTheme.Colour1;
  AddFilesButton.Color := AppTheme.Colour2;
  AddFilesButton.Font.Color := AppTheme.FontColour2;
  AddFilesButton.Font.Name := Font.Name;
  AddFilesButton.HoverColor := AppTheme.Colour1;
  AddFilesButton.HoverFontColor := AppTheme.FontColour1;
  CollectionButton.BackgroundColor := AppTheme.Colour1;
  CollectionButton.Color := AppTheme.Colour2;
  CollectionButton.Font.Color := AppTheme.FontColour2;
  CollectionButton.Font.Name := Font.Name;
  CollectionButton.HoverColor := AppTheme.Colour1;
  CollectionButton.HoverFontColor := AppTheme.FontColour1;
  CollectionTree.Color := AppTheme.Colour1;
  CollectionTree.Font.Color := AppTheme.FontColour1;
  CollectionTree.Font.Name := Font.Name;
  CPUUsageLabel.Font.Color := AppTheme.FontColour1;
  CPUUsageLabel.Font.Name := Font.Name;
  LyricsButton.BackgroundColor := AppTheme.Colour1;
  LyricsButton.Color := AppTheme.Colour2;
  LyricsButton.Font.Color := AppTheme.FontColour2;
  LyricsButton.Font.Name := Font.Name;
  LyricsButton.HoverColor := AppTheme.Colour1;
  LyricsButton.HoverFontColor := AppTheme.FontColour1;
  PlaybackBar.BackgroundColor := AppTheme.Colour1;
  PlaybackBar.Color := AppTheme.Colour2;
  Playbar.BackgroundColor := AppTheme.Colour1;
  Playbar.ClickColor := AppTheme.Colour1;
  Playbar.ClickFontColor := AppTheme.FontColour1;
  Playbar.Color := AppTheme.Colour2;
  Playbar.Font.Color := AppTheme.FontColour2;
  Playbar.HoverColor := AppTheme.Colour1;
  Playbar.HoverFontColor := AppTheme.FontColour1;
  Playlist.Color := AppTheme.Colour1;
  Playlist.Colors.DropMarkColor := AppTheme.Colour2;
  Playlist.Colors.DropTargetBorderColor := AppTheme.Colour2;
  Playlist.Colors.DropTargetColor := AppTheme.Colour2;
  Playlist.Colors.FocusedSelectionBorderColor := AppTheme.Colour1;
  Playlist.Colors.FocusedSelectionColor := AppTheme.Colour2;
  Playlist.Colors.SelectionRectangleBlendColor := AppTheme.Colour2;
  Playlist.Colors.SelectionRectangleBorderColor := AppTheme.Colour2;
  Playlist.Colors.SelectionTextColor := AppTheme.FontColour2;
  Playlist.Colors.UnfocusedColor := AppTheme.Colour1;
  Playlist.Colors.UnfocusedSelectionBorderColor := AppTheme.Colour1;
  Playlist.Colors.UnfocusedSelectionColor := AppTheme.Colour2;
  Playlist.Font.Color := AppTheme.FontColour1;
  Playlist.Font.Name := Font.Name;
  Playlist.Header.Background := AppTheme.Colour1;
  PlayCDButton.BackgroundColor := AppTheme.Colour1;
  PlayCDButton.Color := AppTheme.Colour2;
  PlayCDButton.Font.Color := AppTheme.FontColour2;
  PlayCDButton.Font.Name := Font.Name;
  PlayCDButton.HoverColor := AppTheme.Colour1;
  PlayCDButton.HoverFontColor := AppTheme.FontColour1;
  RemoveFilesButton.BackgroundColor := AppTheme.Colour1;
  RemoveFilesButton.Color := AppTheme.Colour2;
  RemoveFilesButton.Font.Color := AppTheme.FontColour2;
  RemoveFilesButton.Font.Name := Font.Name;
  RemoveFilesButton.HoverColor := AppTheme.Colour1;
  RemoveFilesButton.HoverFontColor := AppTheme.FontColour1;
  StatusBar1.BackgroundColor := AppTheme.Colour1;
  for Iterator := 0 to StatusBar1.Panels.Count - 1 do
     begin
      StatusBar1.Panels[Iterator].Color := AppTheme.Colour1;
      StatusBar1.Panels[Iterator].Font.Color := AppTheme.FontColour1;
      StatusBar1.Panels[Iterator].Font.Name := Font.Name
     end;
  VolumeBar.BackgroundColor := AppTheme.Colour1;
  VolumeBar.Color := AppTheme.Colour2;
end;

// Volume slider
procedure TMainForm.VolumeBarChange(Sender: TObject);
begin
  PlayerVolume := VolumeBar.Position/100;
  if PlayerVolume > 0 then
     begin
       Playbar.MuteUnmuteState := musUnmute;
       MuteUnmuteItem.Caption :=
       GetFormControlText('main_form', 'muteunmuteitem', 'caption_mute')
     end
  else
      begin
        Playbar.MuteUnmuteState := musMute;
        MuteUnmuteItem.Caption :=
        GetFormControlText('main_form', 'muteunmuteitem', 'caption_unmute')
      end;
  if PlayerStream <> 0 then
     BASS_ChannelSetAttribute(PlayerStream, BASS_ATTRIB_VOL, PlayerVolume)
end;

end.

