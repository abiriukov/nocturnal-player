{
    Settings form of Nocturnal Player

    Nocturnal Player is a freeware audio player for Linux with focus on 
    simplicity and functionality that an audiophile may need in everyday usage.
    Copyright (C) 2015-2016 NoSt

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
}

unit Settings_Form;

{$mode objfpc}{$H+}

interface

uses
  Classes, ComCtrls, Controls, Dialogs, ExtCtrls, FileUtil, Forms, Graphics,
  LCLType, Nocturnal_Themes, NocturnalButton, NocturnalComboBox,
  NocturnalGroupBox, NocturnalEdit, Spin, StdCtrls, SysUtils;

type

  { TSettingsForm }

  TSettingsForm = class(TForm)
    AddFolderButton: TNocturnalButton;
    ApplyButton: TNocturnalButton;
    CancelButton: TNocturnalButton;
    CollectionFolderDialog: TSelectDirectoryDialog;
    CollectionDirList: TListView;
    CollectionPage: TTabSheet;
    SelectedSF: TNocturnalEdit;
    EffectsPage: TTabSheet;
    EQBandwidthEdit: TFloatSpinEdit;
    EQBandwidthLabel: TLabel;
    EQCenterEdit: TFloatSpinEdit;
    EQCenterLabel: TLabel;
    EQGainEdit: TFloatSpinEdit;
    EQGainLabel: TLabel;
    EQOnOffBox: TCheckBox;
    EqualizerBox: TNocturnalGroupBox;
    InterfacePage: TTabSheet;
    LanguageLabel: TLabel;
    LanguageSelection: TNocturnalComboBox;
    SelectSFButton: TNocturnalButton;
    DefaultSFButton: TNocturnalButton;
    OKButton: TNocturnalButton;
    SoundFontDialog: TOpenDialog;
    RemoveFolderButton: TNocturnalButton;
    SettingsPageControl: TPageControl;
    MIDIPage: TTabSheet;
    ThemeLabel: TLabel;
    ThemeSelection: TNocturnalComboBox;
    procedure AddFolderButtonClick(Sender: TObject);
    procedure ApplyButtonClick(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure CollectionDirListKeyDown(Sender: TObject; var Key: Word;
      {%H-}Shift: TShiftState);
    procedure CollectionPageShow(Sender: TObject);
    procedure DefaultSFButtonClick(Sender: TObject);
    procedure EQBandwidthEditChange(Sender: TObject);
    procedure EQCenterEditChange(Sender: TObject);
    procedure EQGainEditChange(Sender: TObject);
    procedure EQOnOffBoxChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var {%H-}CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure LanguageSelectionChange(Sender: TObject);
    procedure LanguageSelectionDrawItem({%H-}Control: TWinControl; Index: Integer;
      ARect: TRect; {%H-}State: TOwnerDrawState);
    procedure LoadCollection;
    procedure OKButtonClick(Sender: TObject);
    procedure Remove;
    procedure RemoveFolderButtonClick(Sender: TObject);
    procedure SelectSFButtonClick(Sender: TObject);
    procedure ThemeSelectionChange(Sender: TObject);
    procedure ThemeSelectionDrawItem({%H-}Control: TWinControl; Index: Integer;
      ARect: TRect; {%H-}State: TOwnerDrawState);
    procedure UpdateLanguage;
    procedure UpdateTheme;
  private
    { private declarations }
    CollectionChanged, DeviceChanged,
      EQBandwidthChanged, EQCenterChanged, EQGainChanged, EQStateChanged,
      LanguageChanged, ThemeChanged: Boolean;
    ThemePreview: TTheme;
    // Apply the changes
    procedure ApplyChanges;
  public
    { public declarations }
    DeviceNumber: Byte;
  end;

var
  SettingsForm: TSettingsForm;

implementation

uses
  Collection, FileNameConst, LazFileUtils, LazUTF8, LazUTF8Classes, laz2_DOM,
  laz2_XMLRead, Nocturnal_Language, Nocturnal_Playback, Nocturnal_Settings;

{$R *.lfm}

{ TSettingsForm }

// On clicking "Add" button
procedure TSettingsForm.AddFolderButtonClick(Sender: TObject);
begin
  if CollectionFolderDialog.Execute then
     begin
       AddFoldersToCollection(CollectionFolderDialog.Files,
       DefaultCollectionFileName);
       CollectionChanged := True;
       ApplyButton.Enabled := True;
       LoadCollection
     end
end;

// On clicking "Apply" button
procedure TSettingsForm.ApplyButtonClick(Sender: TObject);
begin
  ApplyChanges
end;

// Apply the changes
procedure TSettingsForm.ApplyChanges;
begin
  // If the folders of the collection are changed
  if CollectionChanged then
     begin
       SetLength(CollectionList, 0);
       InitializeCollection;
       CollectionChanged := False
     end;
  // If equalizer bandwidth value has been changed
  if EQBandwidthChanged then
     begin
       WriteToSettingsFile(DefaultSettingsFileName, 'EQ_BANDWIDTH',
       FloatToStr(EQBandwidthEdit.Value));
       EQBandwidthChanged := False
     end;
  // If equalizer center value has been changed
  if EQCenterChanged then
     begin
       WriteToSettingsFile(DefaultSettingsFileName, 'EQ_CENTER',
       FloatToStr(EQCenterEdit.Value));
       EQCenterChanged := False
     end;
  // If equalizer gain value has been changed
  if EQGainChanged then
     begin
       WriteToSettingsFile(DefaultSettingsFileName, 'EQ_GAIN',
       FloatToStr(EQGainEdit.Value));
       EQGainChanged := False
     end;
  // If equalizer state has been changed
  if EQStateChanged then
     begin
       if EQOnOffBox.Checked then
          WriteToSettingsFile(DefaultSettingsFileName, 'EQ_STATE', 'ON')
       else
         WriteToSettingsFile(DefaultSettingsFileName, 'EQ_STATE', 'OFF');
       EQStateChanged := False
     end;
  // If the theme has been changed, then write it to the settings file
  if ThemeChanged and (GetThemeFromName(ThemeSelection.Text).Name <> '') then
     begin
       AppTheme := GetThemeFromName(ThemeSelection.Text);
       WriteToSettingsFile(DefaultSettingsFileName, 'THEME_NAME',
       AppTheme.Name);
       WriteToSettingsFile(DefaultSettingsFileName, 'THEME_VERSION',
       AppTheme.Version);
       WriteToSettingsFile(DefaultSettingsFileName, 'THEME_AUTHOR',
       AppTheme.Author);
       UpdateTheme;
       ThemeChanged := False
     end;
  // If the language has been changed, then write it to the settings file
  // and apply the changes to the form
  if LanguageChanged and
  (GetLanguageFromName(LanguageSelection.Text).Name <> '') then
     begin
       AppLanguage := GetLanguageFromName(LanguageSelection.Text);
       WriteToSettingsFile(DefaultSettingsFileName, 'LANGUAGE',
       AppLanguage.Code);
       UpdateLanguage;
       LanguageChanged := False
     end;
  ApplyButton.Enabled := False
end;

// On clicking "Cancel" button
procedure TSettingsForm.CancelButtonClick(Sender: TObject);
var
  Cap, Msg: String;
begin
  if ApplyButton.Enabled then
     begin
       Cap := GetMessageCaption('discardchanges');
       Msg := GetMessageText('discardchanges');
       if MessageDlg(Cap, Msg, mtWarning, [mbYes, mbNo], 0) = mrYes then
          SettingsForm.Close
     end
  else
      SettingsForm.Close
end;

// On pushing the button in the collection directories' list
procedure TSettingsForm.CollectionDirListKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  // If pushed Del, remove the directory from the list
  if Key = VK_DELETE then
     Remove
end;

// On showing the Collection page
procedure TSettingsForm.CollectionPageShow(Sender: TObject);
begin
  LoadCollection
end;

// On clicking the Default Soundfont button
procedure TSettingsForm.DefaultSFButtonClick(Sender: TObject);
begin
  SFPath := ChoriumPath;
  SelectedSF.Text := SFPath
end;

// On changing the EQ bandwidth
procedure TSettingsForm.EQBandwidthEditChange(Sender: TObject);
begin
  // Prepare to apply the equalizer bandwidth value change
  EQBandwidthChanged := True;
  ApplyButton.Enabled := True
end;

// On changing the EQ center
procedure TSettingsForm.EQCenterEditChange(Sender: TObject);
begin
  // Prepare to apply the equalizer center value change
  EQCenterChanged := True;
  ApplyButton.Enabled := True
end;

// On changing the EQ gain
procedure TSettingsForm.EQGainEditChange(Sender: TObject);
begin
  // Prepare to apply the equalizer gain value change
  EQGainChanged := True;
  ApplyButton.Enabled := True
end;

// On turning the EQ on/off
procedure TSettingsForm.EQOnOffBoxChange(Sender: TObject);
begin
  if EQOnOffBox.Checked then
     EQOnOffBox.Caption :=
     GetFormControlText('settings_form', 'eqonoffbox', 'state_on')
  else
    EQOnOffBox.Caption :=
     GetFormControlText('settings_form', 'eqonoffbox', 'state_off');
  // Prepare to apply the equalizer state change
  EQStateChanged := True;
  ApplyButton.Enabled := True
end;

// On closing the Settings form
procedure TSettingsForm.FormClose(Sender: TObject; var CloseAction: TCloseAction
  );
begin
  // On close write the position and the size of the form into the settings file
  if Height <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'SETTINGS_FORM_HEIGHT'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'SETTINGS_FORM_HEIGHT',
     IntToStr(Height));
  if Left <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'SETTINGS_FORM_LEFT'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'SETTINGS_FORM_LEFT',
     IntToStr(Left));
  if Top <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'SETTINGS_FORM_TOP'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'SETTINGS_FORM_TOP',
     IntToStr(Top));
  if Width <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'SETTINGS_FORM_WIDTH'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'SETTINGS_FORM_WIDTH',
     IntToStr(Width))
end;

// On "Settings" form creation
procedure TSettingsForm.FormCreate(Sender: TObject);
var
  SHeight, SLeft, STop, SWidth: Integer;
begin
  // Load Settings form display settings
  SHeight := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'SETTINGS_FORM_HEIGHT'), 0);
  SLeft := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'SETTINGS_FORM_LEFT'), 0);
  STop := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'SETTINGS_FORM_TOP'), 0);
  SWidth := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'SETTINGS_FORM_WIDTH'), 0);
  if SHeight <> 0 then
      Height := SHeight;
  if SLeft <> 0 then
      Left := SLeft;
  if STop <> 0 then
      Top := STop;
  if SWidth <> 0 then
      Width := SWidth
end;

// On "Settings" form show
procedure TSettingsForm.FormShow(Sender: TObject);
begin
  UpdateLanguage;
  UpdateTheme;
  // Resetting the variables
  CollectionChanged := False;
  DeviceChanged := False;
  EQBandwidthChanged := False;
  EQCenterChanged := False;
  EQGainChanged := False;
  EQStateChanged := False;
  LanguageChanged := False;
  ThemeChanged := False;
  SettingsPageControl.ActivePageIndex := 0;
  // Get the equalizer settings
  if ReadFromSettingsFile(DefaultSettingsFileName, 'EQ_STATE') = 'ON' then
     EQOnOffBox.Checked := True
  else
      EQOnOffBox.Checked := False;
  EQBandwidthEdit.Value := StrToFloat(ReadFromSettingsFile(
  DefaultSettingsFileName, 'EQ_BANDWIDTH'));
  EQCenterEdit.Value :=
  StrToFloat(ReadFromSettingsFile(DefaultSettingsFileName, 'EQ_CENTER'));
  EQGainEdit.Value :=
  StrToFloat(ReadFromSettingsFile(DefaultSettingsFileName, 'EQ_GAIN'));
  // Get the sound font
  SelectedSF.Text := SFPath
end;

// On language selection
procedure TSettingsForm.LanguageSelectionChange(Sender: TObject);
begin
  // Prepare to apply the language change
  LanguageChanged := True;
  ApplyButton.Enabled := True
end;

// Drawing procedure for the language selection items
procedure TSettingsForm.LanguageSelectionDrawItem(Control: TWinControl;
  Index: Integer; ARect: TRect; State: TOwnerDrawState);
begin
  with LanguageSelection.Canvas do
       begin
         FillRect(ARect);
         TextRect(ARect, 7, ARect.Top, LanguageSelection.Items[Index])
       end
end;

// Load the collection
procedure TSettingsForm.LoadCollection;
var
  CollectionFile: TFileStreamUTF8;
  CollectionListItem: TListItem;
begin
  if FileExistsUTF8(DefaultCollectionFileName) then
     begin
       CollectionFile :=
       TFileStreamUTF8.Create(DefaultCollectionFileName, fmOpenRead);
       try
         with CollectionDirList do
           begin
             BeginUpdate;
             Clear;
             if CollectionFile.Size > 0 then
              while CollectionFile.Position < CollectionFile.Size do
                begin
                  CollectionListItem := Items.Add;
                  CollectionListItem.Caption := CollectionFile.ReadAnsiString
                end;
             EndUpdate
           end;
       finally
         CollectionFile.Free
       end
     end
end;

// On clicking "OK" button
procedure TSettingsForm.OKButtonClick(Sender: TObject);
var
  Cap, Msg: String;
begin
  if ApplyButton.Enabled then
     begin
       Cap := GetMessageCaption('applychanges');
       Msg := GetMessageText('applychanges');
       if MessageDlg(Cap, Msg, mtWarning, [mbYes, mbNo], 0) = mrYes then
          begin
            ApplyChanges;
            SettingsForm.Close
          end
     end
  else SettingsForm.Close
end;

// Removing a folder(folders) from the collection
procedure TSettingsForm.Remove;
var
  CollectionFile: TFileStreamUTF8;
  Count: Integer;
begin
  if CollectionDirList.Selected <> nil then
     begin
       CollectionFile :=
       TFileStreamUTF8.Create(DefaultCollectionFileName, fmCreate);
       try
         for Count := 0 to CollectionDirList.Items.Count - 1 do
           if not CollectionDirList.Items[Count].Selected then
              CollectionFile.WriteAnsiString(
              CollectionDirList.Items[Count].Caption)
       finally
         CollectionFile.Free
       end;
       CollectionChanged := True;
       ApplyButton.Enabled := True;
       LoadCollection
     end
end;

// On clicking the "Remove" button
procedure TSettingsForm.RemoveFolderButtonClick(Sender: TObject);
begin
  Remove
end;

// On clicking the "Select Soundfont" button
procedure TSettingsForm.SelectSFButtonClick(Sender: TObject);
begin
  if SoundFontDialog.Execute then
     begin
       SFPath := SoundFontDialog.FileName;
       SelectedSF.Text := SFPath
     end
end;

// On theme selection
procedure TSettingsForm.ThemeSelectionChange(Sender: TObject);
begin
  // Preview the selected theme
  ThemePreview := GetThemeFromName(ThemeSelection.Text);
  Color := ThemePreview.Colour1;
  if (ThemePreview.FontName <> '') then
    Font.Name := ThemePreview.FontName
  else
    Font.Name := 'default';
  Font.Color := ThemePreview.FontColour1;
  AddFolderButton.Color := ThemePreview.Colour2;
  AddFolderButton.Font.Color := ThemePreview.FontColour2;
  AddFolderButton.Font.Name := Font.Name;
  AddFolderButton.HoverColor := ThemePreview.Colour1;
  AddFolderButton.HoverFontColor := ThemePreview.FontColour1;
  ApplyButton.Color := ThemePreview.Colour2;
  ApplyButton.Font.Color := ThemePreview.FontColour2;
  ApplyButton.Font.Name := Font.Name;
  ApplyButton.HoverColor := ThemePreview.Colour1;
  ApplyButton.HoverFontColor := ThemePreview.FontColour1;
  CancelButton.Color := ThemePreview.Colour2;
  CancelButton.Font.Color := ThemePreview.FontColour2;
  CancelButton.Font.Name := Font.Name;
  CancelButton.HoverColor := ThemePreview.Colour1;
  CancelButton.HoverFontColor := ThemePreview.FontColour1;
  CollectionDirList.Color := ThemePreview.Colour2;
  CollectionDirList.Font.Color := ThemePreview.FontColour2;
  CollectionDirList.Font.Name := Font.Name;
  CollectionPage.Font.Color := ThemePreview.FontColour1;
  CollectionPage.Font.Name := Font.Name;
  DefaultSFButton.Color := ThemePreview.Colour2;
  DefaultSFButton.Font.Color := ThemePreview.FontColour2;
  DefaultSFButton.Font.Name := Font.Name;
  DefaultSFButton.HoverColor := ThemePreview.Colour1;
  DefaultSFButton.HoverFontColor := ThemePreview.FontColour1;
  EffectsPage.Font.Color := ThemePreview.FontColour1;
  EffectsPage.Font.Name := Font.Name;
  EqualizerBox.BevelColor := ThemePreview.Colour2;
  EqualizerBox.BevelShadowColor := ThemePreview.Colour2 - (clBtnHighlight -
  clBtnShadow);
  EqualizerBox.Color := ThemePreview.Colour1;
  EqualizerBox.Font.Color := ThemePreview.Colour1;
  EqualizerBox.Font.Name := Font.Name;
  EQBandwidthEdit.Color := ThemePreview.Colour2;
  EQBandwidthEdit.Font.Color := ThemePreview.FontColour2;
  EQBandwidthEdit.Font.Name := Font.Name;
  EQBandwidthLabel.Font.Color := ThemePreview.FontColour1;
  EQBandwidthLabel.Font.Name := Font.Name;
  EQCenterEdit.Color := ThemePreview.Colour2;
  EQCenterEdit.Font.Color := ThemePreview.FontColour2;
  EQCenterEdit.Font.Name := Font.Name;
  EQCenterLabel.Font.Color := ThemePreview.FontColour1;
  EQCenterLabel.Font.Name := Font.Name;
  EQGainEdit.Color := ThemePreview.Colour2;
  EQGainEdit.Font.Color := ThemePreview.FontColour2;
  EQGainEdit.Font.Name := Font.Name;
  EQGainLabel.Font.Color := ThemePreview.FontColour1;
  EQGainLabel.Font.Name := Font.Name;
  EQOnOffBox.Font.Color := ThemePreview.FontColour1;
  EQOnOffBox.Font.Name := Font.Name;
  InterfacePage.Font.Color := ThemePreview.FontColour1;
  InterfacePage.Font.Name := Font.Name;
  MIDIPage.Font.Color := ThemePreview.FontColour1;
  MIDIPage.Font.Name := Font.Name;
  LanguageLabel.Font.Color := ThemePreview.FontColour1;
  LanguageLabel.Font.Name := Font.Name;
  LanguageSelection.Color := ThemePreview.Colour2;
  LanguageSelection.Font.Color := ThemePreview.FontColour2;
  LanguageSelection.Font.Name := Font.Name;
  LanguageSelection.SelectionColor := ThemePreview.Colour1;
  LanguageSelection.SelectionFontColor := ThemePreview.FontColour1;
  OKButton.Color := ThemePreview.Colour2;
  OKButton.Font.Color := ThemePreview.FontColour2;
  OKButton.Font.Name := Font.Name;
  OKButton.HoverColor := ThemePreview.Colour1;
  OKButton.HoverFontColor := ThemePreview.FontColour1;
  RemoveFolderButton.Color := ThemePreview.Colour2;
  RemoveFolderButton.Font.Color := ThemePreview.FontColour2;
  RemoveFolderButton.Font.Name := Font.Name;
  RemoveFolderButton.HoverColor := ThemePreview.Colour1;
  RemoveFolderButton.HoverFontColor := ThemePreview.FontColour1;
  SelectedSF.Color := ThemePreview.Colour2;
  SelectedSF.Font.Color := ThemePreview.FontColour2;
  SelectedSF.Font.Name := Font.Name;
  SelectedSF.SelectionColor := ThemePreview.Colour1;
  SelectedSF.SelectionFontColor := ThemePreview.FontColour1;
  SelectSFButton.Color := ThemePreview.Colour2;
  SelectSFButton.Font.Color := ThemePreview.FontColour2;
  SelectSFButton.Font.Name := Font.Name;
  SelectSFButton.HoverColor := ThemePreview.Colour1;
  SelectSFButton.HoverFontColor := ThemePreview.FontColour1;
  ThemeLabel.Font.Color := ThemePreview.FontColour1;
  ThemeLabel.Font.Name := Font.Name;
  ThemeSelection.Color := ThemePreview.Colour2;
  ThemeSelection.Font.Color := ThemePreview.FontColour2;
  ThemeSelection.Font.Name := Font.Name;
  ThemeSelection.SelectionColor := ThemePreview.Colour1;
  ThemeSelection.SelectionFontColor := ThemePreview.FontColour1;
  // Prepare to apply the theme change
  ThemeChanged := True;
  ApplyButton.Enabled := True
end;

// Drawing procedure for the theme selection items
procedure TSettingsForm.ThemeSelectionDrawItem(Control: TWinControl;
  Index: Integer; ARect: TRect; State: TOwnerDrawState);
var
  LRect, RRect: TRect;
  RectWidth: Integer;
  TempChar: Char;
  TempTheme: TTheme;
begin
  TempTheme := GetThemeFromName(ThemeSelection.Items[Index]);
  TempChar := TempTheme.Name[1];
  with ThemeSelection.Canvas do
       begin
         // Set the font
         if TempTheme.FontName <> '' then
           ThemeSelection.Canvas.Font.Name := TempTheme.FontName
         else
           ThemeSelection.Canvas.Font.Name := 'default';
         // Get the width of a letter
         RectWidth := TextWidth(TempChar);
         // Fill out the rectangle
         FillRect(ARect);
         TextRect(ARect, RectWidth * 2 + 7, ARect.Top,
         ThemeSelection.Items[Index]);
         // Set the dimensions of the left rectangle and the right rectangle
         LRect.Left := ARect.Left + 2;
         LRect.Right := LRect.Left + RectWidth;
         LRect.Top := ARect.Top + 1;
         LRect.Bottom := ARect.Bottom - 1;
         RRect.Left := LRect.Right;
         RRect.Right := LRect.Right + RectWidth;
         RRect.Top := ARect.Top + 1;
         RRect.Bottom := ARect.Bottom - 1;
         // Draw the left rectangle
         Pen.Color := clNone;
         Rectangle(LRect);
         Brush.Color := TempTheme.Colour1;
         FillRect(LRect);
         // Put the letter into the left rectangle
         Font.Name := TempTheme.FontName;
         Font.Color := TempTheme.FontColour1;
         TextRect(LRect, LRect.Left, LRect.Top, TempChar);
         // Draw the right rectangle
         Rectangle(RRect);
         Brush.Color := TempTheme.Colour2;
         FillRect(RRect);
         // Put the letter into the right rectangle
         Font.Color := TempTheme.FontColour2;
         TextRect(RRect, RRect.Left, RRect.Top, TempChar)
       end
end;

// Update the interface language
procedure TSettingsForm.UpdateLanguage;
var
  AuthorString: String;
  Iterator: Integer;
begin
  Caption := GetFormCaption('settings_form');
  AddFolderButton.Caption :=
  GetFormControlText('settings_form', 'addfolderbutton', 'caption');
  ApplyButton.Caption := GetCommonControlCaption('applybutton');
  CancelButton.Caption := GetCommonControlCaption('cancelbutton');
  CollectionPage.Caption :=
  GetFormControlText('settings_form', 'collectionpage', 'caption');
  DefaultSFButton.Caption :=
  GetFormControlText('settings_form', 'defaultsfbutton', 'caption');
  EffectsPage.Caption :=
  GetFormControlText('settings_form', 'effectspage', 'caption');
  EqualizerBox.Caption :=
  GetFormControlText('settings_form', 'equalizerbox', 'caption');
  EQBandwidthLabel.Caption :=
  GetFormControlText('settings_form', 'eqbandwidthlabel', 'caption');
  EQCenterLabel.Caption :=
  GetFormControlText('settings_form', 'eqcenterlabel', 'caption');
  EQGainLabel.Caption :=
  GetFormControlText('settings_form', 'eqgainlabel', 'caption');
  if EQOnOffBox.Checked then
     EQOnOffBox.Caption :=
     GetFormControlText('settings_form', 'eqonoffbox', 'state_on')
  else
      EQOnOffBox.Caption :=
      GetFormControlText('settings_form', 'eqonoffbox', 'state_off');
  InterfacePage.Caption :=
  GetFormControlText('settings_form', 'interfacepage', 'caption');
  LanguageLabel.Caption :=
  GetFormControlText('settings_form', 'languagelabel', 'caption');
  OKButton.Caption := GetCommonControlCaption('okbutton');
  RemoveFolderButton.Caption :=
  GetFormControlText('settings_form', 'removefolderbutton', 'caption');
  SelectSFButton.Caption :=
  GetFormControlText('settings_form', 'selectsfbutton', 'caption');
  ThemeLabel.Caption :=
  GetFormControlText('settings_form', 'themelabel', 'caption');
  AuthorString := GetCommonControlCaption('author');
  // Get all the available languages
  LanguageSelection.Clear;
  if Length(AllLanguages) > 0 then
     for Iterator := 0 to Length(AllLanguages) - 1 do
         LanguageSelection.Items.Add(Format('%s (%s: %s)',
         [AllLanguages[Iterator].Name, AuthorString,
         AllLanguages[Iterator].Author]));
  // Get the actual language
  for Iterator := 0 to LanguageSelection.Items.Count - 1 do
      if LanguageSelection.Items[Iterator] = Format('%s (%s: %s)',
      [AppLanguage.Name, AuthorString, AppLanguage.Author]) then
         LanguageSelection.ItemIndex := Iterator;
  // Get all the available themes
  ThemeSelection.Clear;
  if Length(AllThemes) > 0 then
     for Iterator := 0 to Length(AllThemes) - 1 do
         ThemeSelection.Items.Add(Format('%s v.%s (%s: %s)',
         [AllThemes[Iterator].Name, AllThemes[Iterator].Version,
         AuthorString, AllThemes[Iterator].Author]));
  // Get the actual theme
  for Iterator := 0 to ThemeSelection.Items.Count - 1 do
      if ThemeSelection.Items[Iterator] = Format('%s v.%s (%s: %s)',
      [AppTheme.Name, AppTheme.Version, AuthorString, AppTheme.Author]) then
         ThemeSelection.ItemIndex := Iterator
end;

// Update the interface theme
procedure TSettingsForm.UpdateTheme;
begin
  Color := AppTheme.Colour1;
  if AppTheme.FontName <> '' then
    Font.Name := AppTheme.FontName
  else
    Font.Name := 'default';
  Font.Color := AppTheme.FontColour1;
  AddFolderButton.Color := AppTheme.Colour2;
  AddFolderButton.Font.Color := AppTheme.FontColour2;
  AddFolderButton.Font.Name := Font.Name;
  AddFolderButton.HoverColor := AppTheme.Colour1;
  AddFolderButton.HoverFontColor := AppTheme.FontColour1;
  ApplyButton.Color := AppTheme.Colour2;
  ApplyButton.Font.Color := AppTheme.FontColour2;
  ApplyButton.Font.Name := Font.Name;
  ApplyButton.HoverColor := AppTheme.Colour1;
  ApplyButton.HoverFontColor := AppTheme.FontColour1;
  CancelButton.Color := AppTheme.Colour2;
  CancelButton.Font.Color := AppTheme.FontColour2;
  CancelButton.Font.Name := Font.Name;
  CancelButton.HoverColor := AppTheme.Colour1;
  CancelButton.HoverFontColor := AppTheme.FontColour1;
  CollectionDirList.Color := AppTheme.Colour2;
  CollectionDirList.Font.Color := AppTheme.FontColour2;
  CollectionDirList.Font.Name := Font.Name;
  CollectionPage.Font.Color := AppTheme.FontColour1;
  CollectionPage.Font.Name := Font.Name;
  DefaultSFButton.Color := AppTheme.Colour2;
  DefaultSFButton.Font.Color := AppTheme.FontColour2;
  DefaultSFButton.Font.Name := Font.Name;
  DefaultSFButton.HoverColor := AppTheme.Colour1;
  DefaultSFButton.HoverFontColor := AppTheme.FontColour1;
  EffectsPage.Font.Color := AppTheme.FontColour1;
  EffectsPage.Font.Name := Font.Name;
  EqualizerBox.BevelColor := AppTheme.Colour2;
  EqualizerBox.BevelShadowColor := AppTheme.Colour2 - (clBtnHighlight -
  clBtnShadow);
  EqualizerBox.Color := AppTheme.Colour1;
  EqualizerBox.Font.Color := AppTheme.Colour1;
  EqualizerBox.Font.Name := Font.Name;
  EQBandwidthEdit.Color := AppTheme.Colour2;
  EQBandwidthEdit.Font.Color := AppTheme.FontColour2;
  EQBandwidthEdit.Font.Name := Font.Name;
  EQBandwidthLabel.Font.Color := AppTheme.FontColour1;
  EQBandwidthLabel.Font.Name := Font.Name;
  EQCenterEdit.Color := AppTheme.Colour2;
  EQCenterEdit.Font.Color := AppTheme.FontColour2;
  EQCenterEdit.Font.Name := Font.Name;
  EQCenterLabel.Font.Color := AppTheme.FontColour1;
  EQCenterLabel.Font.Name := Font.Name;
  EQGainEdit.Color := AppTheme.Colour2;
  EQGainEdit.Font.Color := AppTheme.FontColour2;
  EQGainEdit.Font.Name := Font.Name;
  EQGainLabel.Font.Color := AppTheme.FontColour1;
  EQGainLabel.Font.Name := Font.Name;
  EQOnOffBox.Font.Color := AppTheme.FontColour1;
  EQOnOffBox.Font.Name := Font.Name;
  InterfacePage.Font.Color := AppTheme.FontColour1;
  InterfacePage.Font.Name := Font.Name;
  MIDIPage.Font.Color := AppTheme.FontColour1;
  MIDIPage.Font.Name := Font.Name;
  LanguageLabel.Font.Color := AppTheme.FontColour1;
  LanguageLabel.Font.Name := Font.Name;
  LanguageSelection.Color := AppTheme.Colour2;
  LanguageSelection.Font.Color := AppTheme.FontColour2;
  LanguageSelection.Font.Name := Font.Name;
  LanguageSelection.SelectionColor := AppTheme.Colour1;
  LanguageSelection.SelectionFontColor := AppTheme.FontColour1;
  OKButton.Color := AppTheme.Colour2;
  OKButton.Font.Color := AppTheme.FontColour2;
  OKButton.Font.Name := Font.Name;
  OKButton.HoverColor := AppTheme.Colour1;
  OKButton.HoverFontColor := AppTheme.FontColour1;
  RemoveFolderButton.Color := AppTheme.Colour2;
  RemoveFolderButton.Font.Color := AppTheme.FontColour2;
  RemoveFolderButton.Font.Name := Font.Name;
  RemoveFolderButton.HoverColor := AppTheme.Colour1;
  RemoveFolderButton.HoverFontColor := AppTheme.FontColour1;
  SelectedSF.Color := AppTheme.Colour2;
  SelectedSF.Font.Color := AppTheme.FontColour2;
  SelectedSF.Font.Name := Font.Name;
  SelectedSF.SelectionColor := AppTheme.Colour1;
  SelectedSF.SelectionFontColor := AppTheme.FontColour1;
  SelectSFButton.Color := AppTheme.Colour2;
  SelectSFButton.Font.Color := AppTheme.FontColour2;
  SelectSFButton.Font.Name := Font.Name;
  SelectSFButton.HoverColor := AppTheme.Colour1;
  SelectSFButton.HoverFontColor := AppTheme.FontColour1;
  ThemeLabel.Font.Color := AppTheme.FontColour1;
  ThemeLabel.Font.Name := Font.Name;
  ThemeSelection.Color := AppTheme.Colour2;
  ThemeSelection.Font.Color := AppTheme.FontColour2;
  ThemeSelection.Font.Name := Font.Name;
  ThemeSelection.SelectionColor := AppTheme.Colour1;
  ThemeSelection.SelectionFontColor := AppTheme.FontColour1
end;

end.

