{
    URL input form of Nocturnal Player

    Nocturnal Player is a freeware audio player for Linux with focus on
    simplicity and functionality that an audiophile may need in everyday usage.
    Copyright (C) 2015-2016 NoSt

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
}

unit URL_Input_Form;

{$mode objfpc}{$H+}

interface

uses
  Classes, Controls, Dialogs, FileNameConst, FileUtil, Forms, Graphics,
  NocturnalButton, NocturnalEdit, Nocturnal_Language, Nocturnal_Playback,
  Nocturnal_Settings, Nocturnal_Themes, StdCtrls, SysUtils;

type

  { TURLInputForm }

  TURLInputForm = class(TForm)
    URLInputLabel: TLabel;
    CancelButton: TNocturnalButton;
    OKButton: TNocturnalButton;
    URLInputEdit: TNocturnalEdit;
    procedure CancelButtonClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var {%H-}CloseAction: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
    procedure UpdateLanguage;
    procedure UpdateTheme;
    procedure URLInputEditChange(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  URLInputForm: TURLInputForm;

implementation

{$R *.lfm}

{ TURLInputForm }

// On closing the form
procedure TURLInputForm.FormClose(Sender: TObject; var CloseAction: TCloseAction
  );
begin
  // On close write the position and the size of the form into the settings file
  if Height <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'URL_INPUT_FORM_HEIGHT'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'URL_INPUT_FORM_HEIGHT',
     IntToStr(Height));
  if Left <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'URL_INPUT_FORM_LEFT'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'URL_INPUT_FORM_LEFT',
     IntToStr(Left));
  if Top <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'URL_INPUT_FORM_TOP'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'URL_INPUT_FORM_TOP',
     IntToStr(Top));
  if Width <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'URL_INPUT_FORM_WIDTH'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'URL_INPUT_FORM_WIDTH',
     IntToStr(Width))
end;

// On clicking the "Cancel" button
procedure TURLInputForm.CancelButtonClick(Sender: TObject);
begin
  Close
end;

// On showing the form
procedure TURLInputForm.FormShow(Sender: TObject);
var
  URLHeight, URLLeft, URLTop, URLWidth: Integer;
begin
  // Load URL Input form display settings
  URLHeight := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'URL_INPUT_FORM_HEIGHT'), 0);
  URLLeft := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'URL_INPUT_FORM_LEFT'), 0);
  URLTop := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'URL_INPUT_FORM_TOP'), 0);
  URLWidth := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'URL_INPUT_FORM_WIDTH'), 0);
  if URLHeight <> 0 then
     Height := URLHeight;
  if URLLeft <> 0 then
     Left := URLLeft;
  if URLTop <> 0 then
     Top := URLTop;
  if URLWidth <> 0 then
     Width := URLWidth;
  UpdateLanguage;
  UpdateTheme
end;

// On clicking the "OK" button
procedure TURLInputForm.OKButtonClick(Sender: TObject);
begin
  // Add URL to the playlist
  AddURLToPlaylist(URLInputEdit.Text, DefaultPlaylistFileName);
  Close
end;

// Update the interface language
procedure TURLInputForm.UpdateLanguage;
begin
  Caption := GetFormCaption('url_input_form');
  CancelButton.Caption := GetCommonControlCaption('cancelbutton');
  OKButton.Caption := GetCommonControlCaption('okbutton');
  URLInputLabel.Caption :=
  GetFormControlText('url_input_form', 'urlinputlabel', 'caption')
end;

// Update the interface theme
procedure TURLInputForm.UpdateTheme;
begin
  Color := AppTheme.Colour1;
  if AppTheme.FontName <> '' then
    Font.Name := AppTheme.FontName
  else
    Font.Name := 'default';
  Font.Color := AppTheme.FontColour1;
  CancelButton.BackgroundColor := AppTheme.Colour1;
  CancelButton.Color := AppTheme.Colour2;
  CancelButton.Font.Color := AppTheme.FontColour2;
  CancelButton.HoverColor := AppTheme.Colour1;
  CancelButton.HoverFontColor := AppTheme.FontColour1;
  OKButton.BackgroundColor := AppTheme.Colour1;
  OKButton.Color := AppTheme.Colour2;
  OKButton.Font.Color := AppTheme.FontColour2;
  OKButton.HoverColor := AppTheme.Colour1;
  OKButton.HoverFontColor := AppTheme.FontColour1;
  URLInputEdit.Color := AppTheme.Colour2;
  URLInputEdit.Font.Color := AppTheme.FontColour2;
  URLInputEdit.SelectionColor := AppTheme.Colour1;
  URLInputEdit.SelectionFontColor := AppTheme.FontColour1
end;

// On changing the URL input edit text
procedure TURLInputForm.URLInputEditChange(Sender: TObject);
begin
  // If the text is a valid URL, then enable the OK button
  if IsURL(URLInputEdit.Text) then
     OKButton.Enabled := True
  else
    OKButton.Enabled := False
end;

end.

