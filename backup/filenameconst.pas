{
    File Name constant values of Nocturnal Player

    Nocturnal Player is a freeware audio player for Linux with focus on
    simplicity and functionality that an audiophile may need in everyday usage.
    Copyright (C) 2015-2016 NoSt

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
}

unit FileNameConst;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

const
  {$IFDEF DEBUG}
  ChoriumPath = '/';
  HelpPath = '/help/index.html';
  LangPath = '/lang/';
  LibsPath = '/libs/';
  ThemePath = '/themes/';
  {$ENDIF}
  {$IFDEF RELEASE}
  ChoriumPath = '/usr/share/nocturnal-player/ChoriumRevA.sf2';
  HelpPath = '/usr/share/doc/nocturnal-player/help/index.html';
  LangPath = '/usr/share/nocturnal-player/lang/';
  LibsPath = '/usr/lib/nocturnal-player/';
  ThemePath = '/usr/share/nocturnal-player/themes/';
  {$ENDIF}

var
  DefaultCollectionFileName, DefaultPlaylistFileName,
    DefaultSettingsFileName, ProgramPath, SFPath: String;

implementation

end.

