{
    Settings-related utilities for Nocturnal Player

    Nocturnal Player is a freeware audio player for Linux with focus on
    simplicity and functionality that an audiophile may need in everyday usage.
    Copyright (C) 2015-2016 NoSt

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
}

unit Nocturnal_Settings;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type

  // Param type containing the settings
  TParam = Record
    ParamName: String[255];
    ParamValue: String[255]
  end;

// Read the parameter value from the settings file
// Returns the value of string
// SettingsFileName - the name of the settings file
// SetName - the name of the parameter to read
function ReadFromSettingsFile(const SettingsFileName: String; SetName: String):
String;

// Create a new settings file
// SettingsFileName - the name of the settings file
procedure CreateSettingsFile(const SettingsFileName: String);

// Write the parameter value to the settings file
// SettingsFileName - the name of the settings file
// SetName - the name of the parameter to write
// SetValue - the value of the parameter to write
procedure WriteToSettingsFile(const SettingsFileName: String; SetName: String;
  SetValue: String);

implementation

uses
  LazFileUtils, LazUTF8Classes;

// Read the parameter value from the settings file
// Returns the value of string
// SettingsFileName - the name of the settings file
// SetName - the name of the parameter to read
function ReadFromSettingsFile(const SettingsFileName: String; SetName: String):
String;
var
  Param: TParam;
  SettingsFile: TFileStreamUTF8;
begin
  Result := '';
  if FileExistsUTF8(SettingsFileName) then
     begin
       Param.ParamName := '';
       SettingsFile := TFileStreamUTF8.Create(SettingsFileName, fmOpenRead);
       try
         if SettingsFile.Size > 0 then
           while SettingsFile.Position < SettingsFile.Size do
             begin
               SettingsFile.ReadBuffer(Param, SizeOf(TParam));
               if (Param.ParamName = SetName) then
                 Result := Param.ParamValue
             end
       finally
         SettingsFile.Free
       end
     end
end;

// Create a new settings file
// FileName - the name of the settings file
procedure CreateSettingsFile(const SettingsFileName: String);
var
  SettingsFile: TFileStreamUTF8;
begin
  if not FileExistsUTF8(SettingsFileName) then
    begin
      SettingsFile := TFileStreamUTF8.Create(SettingsFileName, fmCreate);
      SettingsFile.Free
    end
end;

// Write the parameter value to the settings file
// SettingsFileName - the name of the settings file
// SetName - the name of the parameter to write
// SetValue - the value of the parameter to write
procedure WriteToSettingsFile(const SettingsFileName: String; SetName: String;
  SetValue: String);
var
  Param: TParam;
  SettingsFile: TFileStreamUTF8;
  SetNameFound: Boolean;
  TempStream: TMemoryStreamUTF8;
begin
  if not FileExistsUTF8(SettingsFileName) then
    CreateSettingsFile(SettingsFileName);
  SettingsFile := TFileStreamUTF8.Create(SettingsFileName, fmOpenReadWrite);
  try
    SetNameFound := False;
    Param.ParamName := '';
    // Using the temporary stream to store the data
    TempStream := TMemoryStreamUTF8.Create;
    try
      // Search for the set parameter in the settings file
      while SettingsFile.Position < SettingsFile.Size do
        begin
          SettingsFile.ReadBuffer(Param, SizeOf(TParam));
          // If the set parameter is encountered
          if Param.ParamName = SetName then
            begin
              SetNameFound := True;
              // If its value is changed, then write the new value
              if Param.ParamValue <> SetValue then
                Param.ParamValue := SetValue
            end;
          TempStream.WriteBuffer(Param, SizeOf(TParam))
        end;
      // If the set parameter is not encountered, then add it to the settings file
      if not SetNameFound then
        begin
          Param.ParamName := SetName;
          Param.ParamValue := SetValue;
          TempStream.WriteBuffer(Param, SizeOf(TParam))
        end;
      SettingsFile.Free;
      SettingsFile := TFileStreamUTF8.Create(SettingsFileName, fmCreate);
      SettingsFile.CopyFrom(TempStream, 0);
    finally
      TempStream.Free
    end
  finally
    SettingsFile.Free
  end
end;

end.

