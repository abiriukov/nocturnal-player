{
    Collection utilities for Nocturnal Player

    Nocturnal Player is a freeware audio player for Linux with focus on
    simplicity and functionality that an audiophile may need in everyday usage.
    Copyright (C) 2015-2016 NoSt

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
}

unit Collection;

{$mode objfpc}{$H+}

interface

uses
  Classes, Nocturnal_Playback, SysUtils;

type

  // FileScanned type containing the scanned file info
  TFileScanned = Record
    FileName: String[255];
    FilePath: String[255];
    LastChanged: LongInt;
    Track: TTrack
  end;

// Get the time of the last change for the file from the scanned file
// Returns the longint value (default: -1)
// ScannedFileName - the name of the scanned file
// FileName - the name of the file
// FilePath - the path to the file
function GetLastChangedFromScannedFile(const ScannedFileName, FileName,
  FilePath: String): LongInt;
// Get the track info for the file from the scanned file
// Returns a TTrack value (default: nil)
// ScannedFileName - the name of the scanned file
// FileName - the name of the file
// FilePath - the path to the file
function GetTrackFromScannedFile(const ScannedFileName, FileName,
  FilePath: String): TTrack;
// Create a new scanned file
// ScannedFileName - the name of the scanned file
procedure CreateScannedFile(const ScannedFileName: String);
// Initialize the collection
procedure InitializeCollection;
// Scan the collection
procedure ScanCollection(const CollectionFileName, ScannedFileName: String);
// Write the file to the scanned file
// ScannedFileName - the name of the scanned file
// FileName - the name of the file
// FilePath - the path to the file
// Track - the track info
// LastChanged - the longint value of the last change time
procedure WriteToScannedFile(const ScannedFileName, FileName, FilePath: String;
  Track: TTrack; LastChanged: LongInt);

var
  // Collection
  CollectionList: TTracks;
  Msg: String;

implementation

uses
  ComCtrls, Dialogs, FileNameConst, Forms, LazFileUtils, LazUTF8Classes,
  Main_Form, Nocturnal_Language, Please_Wait_Form;

// Get the time of the last change for the file from the scanned file
// Returns a longint value (default: -1)
// ScannedFileName - the name of the scanned file
// FileName - the name of the file
// FilePath - the path to the file
function GetLastChangedFromScannedFile(const ScannedFileName, FileName,
  FilePath: String): LongInt;
var
  FileScanned: TFileScanned;
  FileSize: LongInt;
  ScannedFile: TFileStreamUTF8;
begin
  Result := -1;
  if FileExistsUTF8(ScannedFileName) then
     begin
       FileScanned.FileName := '';
       FileScanned.FilePath := '';
       ScannedFile := TFileStreamUTF8.Create(ScannedFileName, fmOpenRead);
       try
         if ScannedFile.Size > 0 then
           while ScannedFile.Position < ScannedFile.Size do
             begin
               ShowMessage(Format('Reading the last changed info from %s',
               [FileName]));
               ScannedFile.ReadBuffer(FileSize, SizeOf(FileSize));
               ScannedFile.ReadBuffer(FileScanned, FileSize);
               if (FileScanned.FileName = FileName) and
               (FileScanned.FilePath = FilePath) then
                 Result := FileScanned.LastChanged;
               ShowMessage(Result)
             end
       finally
         ScannedFile.Free
       end
     end
end;

// Get the track info for the file from the scanned file
// Returns a TTrack value (default: nil)
// ScannedFileName - the name of the scanned file
// FileName - the name of the file
// FilePath - the path to the file
function GetTrackFromScannedFile(const ScannedFileName, FileName,
  FilePath: String): TTrack;
var
  FileScanned: TFileScanned;
  FileSize: LongInt;
  ScannedFile: TFileStreamUTF8;
begin
  if FileExistsUTF8(ScannedFileName) then
    begin
      FileScanned.FileName := '';
      FileScanned.FilePath := '';
      ScannedFile := TFileStreamUTF8.Create(ScannedFileName, fmOpenRead);
      try
        if ScannedFile.Size > 0 then
          while ScannedFile.Position < ScannedFile.Size do
            begin
              ShowMessage(Format('Reading the track info from %s:',
               [FileName]));
              ScannedFile.ReadBuffer(FileSize, SizeOf(FileSize));
              ScannedFile.ReadBuffer(FileScanned, FileSize);
              if (FileScanned.FileName = FileName) and
              (FileScanned.FilePath = FilePath) then
                Result := FileScanned.Track
            end
      finally
        ScannedFile.Free
      end
    end;
end;

// Create a new scanned file
// ScannedFileName - the name of the scanned file
procedure CreateScannedFile(const ScannedFileName: String);
var
  ScannedFile: TFileStreamUTF8;
begin
  if not FileExistsUTF8(ScannedFileName) then
    begin
      ScannedFile := TFileStreamUTF8.Create(ScannedFileName, fmCreate);
      ScannedFile.Free
    end
end;

// Initialize the collection
procedure InitializeCollection;

  // Sort the artists in alphabetical order
  procedure SortArtists;
  var
    Count1, Count2, Iterator, Min: Integer;
    TempArtist: TArtist;
  begin
    Count1 := Length(Artists);
    Min := 0;
    for Iterator := 1 to Count1 - 1 do
       if Artists[Iterator].Name < Artists[Min].Name then
          Min := Iterator;
    TempArtist := Artists[0];
    Artists[0] := Artists[Min];
    Artists[Min] := TempArtist;
    for Iterator := 1 to Count1 - 1 do
       begin
         TempArtist := Artists[Iterator];
         Count2 := Iterator - 1;
         while Artists[Count2].Name > TempArtist.Name do
           begin
             Artists[Count2 + 1] := Artists[Count2];
             Dec(Count2)
           end;
         Artists[Count2 + 1] := TempArtist
       end
  end;

  // Get all the artists
  function GetRootNodes: Integer;
  var
    Count, Iterator1, Iterator2: Integer;
    Repeated: Boolean;
    TempTrack: TTrack;
  begin
    Count := 0;
    if Length(CollectionList) > 0 then
       for Iterator1 := 0 to Length(CollectionList) - 1 do
          begin
            Application.ProcessMessages;
            TempTrack := CollectionList[Iterator1];
            Repeated := False;
            if Length(Artists) > 0 then
               begin
                 for Iterator2 := 0 to Count - 1 do
                    begin
                      Repeated := Repeated or
                      (Artists[Iterator2].Name <> '') and
                      ((TempTrack.AlbumArtist =
                      Artists[Iterator2].Name) or
                      (TempTrack.Artist = Artists[Iterator2].Name)) or
                      (Artists[Iterator2].Name = 'Unknown Artist') and
                      (TempTrack.AlbumArtist = '') and
                      (TempTrack.Artist = '');
                      if Repeated then
                         Break
                    end
               end;
            if not Repeated then
               begin
                 SetLength(Artists, Count + 1);
                 if TempTrack.AlbumArtist <> '' then
                    Artists[Count].Name := TempTrack.AlbumArtist
                 else
                   begin
                     if TempTrack.Artist <> '' then
                        Artists[Count].Name := TempTrack.Artist
                     else
                       Artists[Count].Name := 'Unknown Artist'
                   end;
                 Inc(Count)
               end
          end;
    if Length(Artists) > 1 then
       SortArtists;
    Result := Count
  end;

begin
  // If the collection is empty, perform the scan
  if Length(CollectionList) = 0 then
     ScanCollection(DefaultCollectionFileName, DefaultScannedFileName);
  if PleaseWaitForm.Visible then
    PleaseWaitForm.Hide;
  SetLength(Artists, 0);
  // Prepare the collection tree
  with MainForm.CollectionTree do
    begin
      RootNodeCount := GetRootNodes;
      NodeDataSize := SizeOf(TTrack)
    end
end;

// Scan the collection
procedure ScanCollection(const CollectionFileName, ScannedFileName: String);

  // Scan inside the directory
  procedure ScanDir(const Directory: String);
  var
    Count, LastChanged: Integer;
    SearchRec: TSearchRec;
    TempString: String;
  begin
    // Check if the directory exists
    if not DirectoryExistsUTF8(Directory) then
       Exit;
    // Check, if there is any file in the directory
    if FindFirstUTF8(Directory + '/*', faAnyFile, SearchRec) = 0 then
       repeat
         Application.ProcessMessages;
         Count := Length(CollectionList);
         // If not a directory
         if not (SearchRec.Attr and faDirectory > 0) then
            begin
              // Check if the file playback is supported
              for TempString in AllowedExt do
                if UpperCase(ExtractFileExt(SearchRec.Name)) = TempString then
                  begin
                    ShowMessage(Format('Testing %s', [SearchRec.Name]));
                    SetLength(CollectionList, Count + 1);
                    // Read the last change time from the scanned file
                    LastChanged := GetLastChangedFromScannedFile(
                    ScannedFileName, SearchRec.Name, Directory);
                    // If the file was not scanned, scan it
                    // If the file was changed since the scan, scan it again
                    if (LastChanged = -1) or
                    (LastChanged <> SearchRec.Time) then
                      begin
                        ShowMessage(Format('Scanning %s', [SearchRec.Name]));
                        CollectionList[Count] :=
                        GetTags(Directory + '/' + SearchRec.Name);
                        WriteToScannedFile(ScannedFileName, SearchRec.Name,
                        Directory, CollectionList[Count], SearchRec.Time);
                        if CollectionList[Count].AlbumArtist <> '' then
                          Msg := GetMessageText('scan') + ': ' +
                          CollectionList[Count].AlbumArtist
                        else
                          Msg := GetMessageText('scan') + ': ' +
                          CollectionList[Count].Artist;
                        ShowPleaseWait(Msg, pbstMarquee)
                      end
                    else
                      CollectionList[Count] := GetTrackFromScannedFile(
                      ScannedFileName, SearchRec.Name, Directory);
                    Break
                  end
            end
         // If a directory
         else
           begin
             // Scan inside the directory
             if SearchRec.Name[1] <> '.' then
                ScanDir(Directory + '/' + SearchRec.Name)
           end
       until FindNextUTF8(SearchRec) <> 0;
    FindCloseUTF8(SearchRec)
  end;

var
  CollectionStream: TFileStreamUTF8;
  TempString: String;
begin
  // If the collection file exists
  if FileExistsUTF8(CollectionFileName) then
     begin
       CollectionStream :=
       TFileStreamUTF8.Create(CollectionFileName, fmOpenRead);
       // If the collection file is not empty
       if CollectionStream.Size > 0 then
          // Until the end of the file
          while CollectionStream.Position < CollectionStream.Size do
            begin
              Application.ProcessMessages;
              TempString := CollectionStream.ReadAnsiString;
              // If the directory exists, scan it
              if DirectoryExistsUTF8(TempString) then
                 ScanDir(TempString)
            end
       // If the collection file is empty
       else
           SetLength(CollectionList, 0);
       CollectionStream.Free
     end
end;

// Write the file to the scanned file
// ScannedFileName - the name of the scanned file
// FileName - the name of the file
// FilePath - the path to the file
// Track - the track info
// LastChanged - the longint value of the last change time
procedure WriteToScannedFile(const ScannedFileName, FileName, FilePath: String;
  Track: TTrack; LastChanged: LongInt);
var
  FileScanned: TFileScanned;
  FileSize: LongInt;
  ScannedFile: TFileStreamUTF8;
  ScannedFound: Boolean;
  TempStream: TMemoryStreamUTF8;
begin
  if not FileExistsUTF8(ScannedFileName) then
    CreateScannedFile(ScannedFileName);
  ScannedFile := TFileStreamUTF8.Create(ScannedFileName, fmOpenReadWrite);
  try
    ScannedFound := False;
    FileScanned.FileName := '';
    FileScanned.FilePath := '';
    // Using the temporary stream to store the data
    TempStream := TMemoryStreamUTF8.Create;
    try
      {if ScannedFile.Size > 0 then
        begin
          // Search for the file name and path in the scanned file
          while ScannedFile.Position < ScannedFile.Size do
            begin
              ScannedFile.ReadBuffer(FileSize, SizeOf(FileSize));
              ScannedFile.ReadBuffer(FileScanned, FileSize);
              // If the file name and path are encountered
              if (FileScanned.FileName = FileName) and
              (FileScanned.FilePath = FilePath) then
                begin
                  ScannedFound := True;
                  // If its value is changed, then write the new value
                  if FileScanned.LastChanged <> LastChanged then
                    begin
                      FileScanned.LastChanged := LastChanged;
                      FileScanned.Track := Track;
                      FileSize := SizeOf(FileScanned)
                    end
                end;
              TempStream.WriteBuffer(FileScanned, FileSize)
            end;
        end;}
      // If the file name and path are not encountered or the scanned file is empty,
      // then add the file to the scanned file
      if not ScannedFound then
        begin
          FileScanned.FileName := FileName;
          FileScanned.FilePath := FilePath;
          FileScanned.Track := Track;
          FileScanned.LastChanged := LastChanged;
          FileSize := SizeOf(FileScanned);
          ShowMessage(Format('Writing %s (size: %d) into the scanned file',
          [FileScanned.FileName, FileSize]));
          TempStream.WriteBuffer(FileSize, SizeOf(FileSize));
          TempStream.WriteBuffer(FileScanned, FileSize)
        end;
      ScannedFile.Free;
      ScannedFile := TFileStreamUTF8.Create(ScannedFileName, fmCreate);
      ScannedFile.CopyFrom(TempStream, 0);
    finally
      TempStream.Free
    end
  finally
    ScannedFile.Free
  end
end;

end.

