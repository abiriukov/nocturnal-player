{
    CD form of Nocturnal Player

    Nocturnal Player is a freeware audio player for Linux with focus on 
    simplicity and functionality that an audiophile may need in everyday usage.
    Copyright (C) 2015-2016 NoSt

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
}

unit CD_Form;  { TODO -oNoSt -cFeatures : Add CD grabbing availability }

{$mode objfpc}{$H+}

interface

uses
  Classes, Controls, Dialogs, FileUtil, Forms, Graphics, NocturnalButton,
  StdCtrls, SysUtils;

type

  { TCDForm }

  TCDForm = class(TForm)
    RipButton: TNocturnalButton;
    PlayButton: TNocturnalButton;
    AddToPlaylistButton: TNocturnalButton;
    CancelButton: TNocturnalButton;
    CDDriveLabel: TLabel;
    CDDriveSelection: TComboBox;
    procedure AddToPlaylistButtonClick(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var {%H-}CloseAction: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure PlayButtonClick(Sender: TObject);
    procedure RipButtonClick(Sender: TObject);
    procedure UpdateLanguage;
    procedure UpdateTheme;
  private
    { private declarations }
    // Read CD tracks to the playlist
    procedure ReadCDTracks;
  public
    { public declarations }
  end;

var
  CDForm: TCDForm;

implementation

uses
  BASSCD, FileNameConst, LazUTF8, Main_Form, Nocturnal_Language,
  Nocturnal_Playback, Nocturnal_Settings, Nocturnal_Themes;

{$R *.lfm}

{ TCDForm }

// On clicking "Add to Playlist" button
procedure TCDForm.AddToPlaylistButtonClick(Sender: TObject);
begin
  ReadCDTracks
end;

// On clicking "Cancel" button
procedure TCDForm.CancelButtonClick(Sender: TObject);
begin
  CDForm.Close
end;

// On closing the form
procedure TCDForm.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  // Write the position and the size of the form into the settings file
  if Height <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'CD_FORM_HEIGHT'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'CD_FORM_HEIGHT',
     IntToStr(Height));
  if Left <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'CD_FORM_LEFT'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'CD_FORM_LEFT',
     IntToStr(Left));
  if Top <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'CD_FORM_TOP'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'CD_FORM_TOP',
     IntToStr(Top));
  if Width <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'CD_FORM_WIDTH'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'CD_FORM_WIDTH',
     IntToStr(Width))
end;

// Ons showing the form
procedure TCDForm.FormShow(Sender: TObject);
var
  CDHeight, CDLeft, CDTop, CDWidth: Integer;
  Iterator: Integer;
begin
  // Load CD Selection form display settings
  CDHeight := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'CD_FORM_HEIGHT'), 0);
  CDLeft := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'CD_FORM_LEFT'), 0);
  CDTop := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'CD_FORM_TOP'), 0);
  CDWidth := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'CD_FORM_WIDTH'), 0);
  if CDHeight <> 0 then
     Height := CDHeight;
  if CDLeft <> 0 then
     Left := CDLeft;
  if CDTop <> 0 then
     Top := CDTop;
  if CDWidth <> 0 then
     Width := CDWidth;
  UpdateLanguage;
  UpdateTheme;
  // Default CD drive
  for Iterator := 0 to CDDriveSelection.Items.Count - 1 do
      if StrToIntDef(UTF8Copy(CDDriveSelection.Items[Iterator], 1,
         UTF8Pos(':', CDDriveSelection.Items[Iterator]) - 1), 0) = SetDrive then
            CDDriveSelection.Text := CDDriveSelection.Items[Iterator]
end;

// On clicking "Play" button
procedure TCDForm.PlayButtonClick(Sender: TObject);
var
  TempInt: Integer;
begin
  with MainForm do
       begin
         TempInt := Playlist.RootNodeCount;
         ReadCDTracks;
         StopPlayback(True);
         PlaylistPosition := TempInt;
         PlayCurrent(True)
       end;
  Close
end;

procedure TCDForm.RipButtonClick(Sender: TObject);
begin

end;

// Read CD tracks to the playlist
procedure TCDForm.ReadCDTracks;

  // Update the playlist with CD tracks
  procedure PlaylistUpdate(CDNFO: BASS_CD_INFO);
  begin
    // If possible, lock the door while playing
    //if CDNFO.canlock and not BASS_CD_DoorIsLocked(SetDrive) then
    //   BASS_CD_Door(SetDrive, BASS_CD_DOOR_LOCK);
    ADDCDToPlaylist(DefaultPlaylistFileName, BASS_CD_GetTracks(SetDrive));
    MainForm.LoadPlaylist(DefaultPlaylistFileName);
    Close
  end;

var
  Cap, Msg: String;
  CDNFO: BASS_CD_INFO;
  MsgDlg: TModalResult;
begin
  CDNFO.product := nil;
  // Get the drive info
  BASS_CD_GetInfo(SetDrive, CDNFO);
  // Check if the drive is ready for the playback
  if BASS_CD_IsReady(SetDrive) then
     PlaylistUpdate(CDNFO)
  // If the drive is not ready
  else
      begin
        // If the door is currently locked
        //if BASS_CD_DoorIsLocked(SetDrive) then
        //   begin
             // Try to unlock the door
        //     if not BASS_CD_Door(SetDrive, BASS_CD_DOOR_UNLOCK) then
        //        ShowMessage(GetMessageText('cddoorislocked'))
        //   end
        // If the door is not locked
        //else
            // If the door is closed, try to open it
            if not BASS_CD_DoorIsOpen(SetDrive) then
               begin
                 if BASS_CD_Door(SetDrive, BASS_CD_DOOR_OPEN) then
                    begin
                      MsgDlg := mrNone;
                      Cap := GetMessageCaption('cdpleaseinsert');
                      Msg := GetMessageText('cdpleaseinsert');
                      while (not BASS_CD_IsReady(SetDrive)) and
                      (MsgDlg <> mrCancel) do
                              MsgDlg := MessageDlg(Cap, Msg, mtWarning,
                              [mbCancel, mbOk], 0);
                      if BASS_CD_IsReady(SetDrive) then
                         PlaylistUpdate(CDNFO)
                    end
                 else
                     ShowMessage(GetMessageText('cddoorcouldnotopen'))
               end
      end
end;

// Update the interface language
procedure TCDForm.UpdateLanguage;
begin
  Caption := GetFormCaption('cd_form');
  AddToPlaylistButton.Caption :=
  GetFormControlText('cd_form', 'addtoplaylistbutton', 'caption');
  CancelButton.Caption := GetCommonControlCaption('cancelbutton');
  CDDriveLabel.Caption :=
  GetFormControlText('cd_form', 'cddrivelabel', 'caption');
  PlayButton.Caption :=
  GetFormControlText('cd_form', 'playbutton', 'caption');
  RipButton.Caption :=
  GetFormControlText('cd_form', 'ripbutton', 'caption')
end;

// Update the interface theme
procedure TCDForm.UpdateTheme;
begin
  Color := AppTheme.Colour1;
  if AppTheme.FontName <> '' then
    Font.Name := AppTheme.FontName
  else
    Font.Name := 'default';
  Font.Color := AppTheme.FontColour1;
  AddToPlaylistButton.BackgroundColor := AppTheme.Colour1;
  AddToPlaylistButton.Color := AppTheme.Colour2;
  AddToPlaylistButton.Font.Color := AppTheme.FontColour2;
  AddToPlaylistButton.HoverColor := AppTheme.Colour1;
  AddToPlaylistButton.HoverFontColor := AppTheme.FontColour1;
  CancelButton.BackgroundColor := AppTheme.Colour1;
  CancelButton.Color := AppTheme.Colour2;
  CancelButton.Font.Color := AppTheme.FontColour2;
  CancelButton.HoverColor := AppTheme.Colour1;
  CancelButton.HoverFontColor := AppTheme.FontColour1;
  CDDriveSelection.Font.Color := AppTheme.FontColour1 and
  AppTheme.FontColour2;
  CDDriveLabel.Font.Color := AppTheme.FontColour1;
  PlayButton.BackgroundColor := AppTheme.Colour1;
  PlayButton.Color := AppTheme.Colour2;
  PlayButton.Font.Color := AppTheme.FontColour2;
  PlayButton.HoverColor := AppTheme.Colour1;
  PlayButton.HoverFontColor := AppTheme.FontColour1;
  RipButton.BackgroundColor := AppTheme.Colour1;
  RipButton.Color := AppTheme.Colour2;
  RipButton.Font.Color := AppTheme.FontColour2;
  RipButton.HoverColor := AppTheme.Colour1;
  RipButton.HoverFontColor := AppTheme.FontColour1;
  RipButton.Visible := False // Not implemented yet
end;

end.

