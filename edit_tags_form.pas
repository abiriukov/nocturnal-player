{
    Edit Tags form of Nocturnal Player

    Nocturnal Player is a freeware audio player for Linux with focus on 
    simplicity and functionality that an audiophile may need in everyday usage.
    Copyright (C) 2015-2016 NoSt

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
}

unit Edit_Tags_Form;

{$mode objfpc}{$H+}

interface

uses
  Classes, ComCtrls, Controls, Dialogs, FileNameConst, FileUtil, Forms,
  Graphics, Grids, NocturnalButton, SysUtils;

type

  { TEditTagsForm }

  TEditTagsForm = class(TForm)
    FileList: TListView;
    ApplyButton: TNocturnalButton;
    CancelButton: TNocturnalButton;
    OKButton: TNocturnalButton;
    Tags: TStringGrid;
    procedure ApplyButtonClick(Sender: TObject);
    procedure CancelButtonClick(Sender: TObject);
    procedure FileListSelectItem(Sender: TObject; {%H-}Item: TListItem;
      {%H-}Selected: Boolean);
    procedure FormClose(Sender: TObject; var {%H-}CloseAction: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure OKButtonClick(Sender: TObject);
    procedure TagsDrawCell(Sender: TObject; aCol, aRow: Integer; {%H-}aRect: TRect;
      {%H-}aState: TGridDrawState);
    procedure TagsEditingDone(Sender: TObject);
    procedure TagsKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure TagsSelectEditor(Sender: TObject; aCol, aRow: Integer;
      var {%H-}Editor: TWinControl);
    procedure UpdateLanguage;
    procedure UpdateTheme;
  private
    { private declarations }
    StoredRow: Integer;
    StoredValue: String;
    // If the cell value is changed
    ValueChanged: Array[1..12] of Boolean;
    // Write all the changed tags into the files
    function WriteTags: Boolean;
  public
    { public declarations }
  end;

var
  EditTagsForm: TEditTagsForm;

implementation

{$R *.lfm}

uses
  AACfile, APEtag, FLACfile, ID3v2, LCLType, Main_Form, Nocturnal_Language,
  Nocturnal_Playback, Nocturnal_Settings, Nocturnal_Themes, OggVorbis,
  Please_Wait_Form;

{ TEditTagsForm }

// Write all the changed tags into the files
function TEditTagsForm.WriteTags: Boolean;
var
  AACTag: TAACfile;
  APETag: TAPEtag;
  Count1: Integer;
  Count2: Byte;
  FLACTag: TFLACfile;
  ID3v2Tag: TID3v2;
  Msg: String;
  OGGVorbisTag: TOggVorbis;
  Written: Boolean;
begin
  Result := False;
  Written := True;
  with FileList do
       begin
         // Check if any of the tags has been changed
         for Count1 := 0 to Items.Count - 1 do
             if Items[Count1].SubItems[13] = '1' then
                begin
                  Application.ProcessMessages;
                  // Display the "Please Wait" form
                  Msg := GetMessageText('writetags') + ': ' +
                  Items[Count1].SubItems[12];
                  ShowPleaseWait(Msg, pbstNormal, Count1 + 1, Items.Count);
                  // Check the file extension
                  case UpperCase(ExtractFileExt(Items[Count1].SubItems[12])) of
                     '.AAC', '.M4A', '.MP4':
                               begin
                                 // Write tags to AAC, M4A or MP4 file
                                 AACTag := TAACfile.Create;
                                 try
                                   // Store the values
                                   AACTag.ID3v2.Album :=
                                   Items[Count1].SubItems[4];
                                   AACTag.ID3v2.Artist :=
                                   Items[Count1].SubItems[2];
                                   AACTag.ID3v2.Comment :=
                                   Items[Count1].SubItems[9];
                                   AACTag.ID3v2.Composer :=
                                   Items[Count1].SubItems[10];
                                   AACTag.ID3v2.Copyright :=
                                   Items[Count1].SubItems[11];
                                   AACTag.ID3v2.Disc :=
                                   StrToIntDef(Items[Count1].SubItems[7], 0);
                                   AACTag.ID3v2.Genre :=
                                   Items[Count1].SubItems[8];
                                   // Get lyrics in order to keep them intact
                                   AACTag.ID3v2.Lyric :=
                                   GetLyrics(Items[Count1].SubItems[12]);
                                   // Album artist is written to TPE2
                                   AACTag.ID3v2.Orchestra :=
                                   Items[Count1].SubItems[3];
                                   AACTag.ID3v2.SubTitle :=
                                   Items[Count1].SubItems[1];
                                   AACTag.ID3v2.Title :=
                                   Items[Count1].SubItems[0];
                                   AACTag.ID3v2.Track :=
                                   StrToIntDef(Items[Count1].SubItems[6], 0);
                                   AACTag.ID3v2.Year := Items[Count1].SubItems[5];
                                   // Attempt to save the tags to the file
                                   if not AACTag.ID3v2.SaveToFile(
                                   Items[Count1].SubItems[12]) then
                                      begin
                                        ShowMessage(GetMessageText(
                                        'couldnotwritetofile') +
                                        Items[Count1].Caption);
                                        Written := Written and False
                                      end
                                   else
                                     begin
                                       StoredRow := 0;
                                       for Count2 := 1 to 12 do
                                           ValueChanged[Count2] := False;
                                       Items[Count1].SubItems[13] := '';
                                       Written := Written and True
                                     end
                                 finally
                                   AACTag.Free
                                 end
                               end;
                     '.APE':
                               begin
                                 // Write tags to APE file
                                 APETag := TAPEtag.Create;
                                 try
                                   // Store the values
                                   APETag.AppendField('Album',
                                   Items[Count1].SubItems[4]);
                                   APETag.AppendField('Artist',
                                   Items[Count1].SubItems[2]);
                                   APETag.AppendField('Comment',
                                   Items[Count1].SubItems[9]);
                                   APETag.AppendField('Composer',
                                   Items[Count1].SubItems[10]);
                                   APETag.AppendField('Copyright',
                                   Items[Count1].SubItems[11]);
                                   APETag.AppendField('Genre',
                                   Items[Count1].SubItems[8]);
                                   APETag.AppendField('ORCHESTRA',
                                   Items[Count1].SubItems[3]);
                                   APETag.AppendField('SUBTITLE',
                                   Items[Count1].SubItems[1]);
                                   APETag.AppendField('Title',
                                   Items[Count1].SubItems[0]);
                                   APETag.AppendField('TRACK',
                                   Items[Count1].SubItems[6]);
                                   APETag.AppendField('Year',
                                   Items[Count1].SubItems[5]);
                                   // Attempt to save the tags to the file
                                   if not APETag.WriteTagInFile(WideString(
                                   Items[Count1].SubItems[12])) then
                                      begin
                                        ShowMessage(GetMessageText(
                                        'couldnotwritetofile') +
                                        Items[Count1].Caption);
                                        Written := Written and False
                                      end
                                   else
                                     begin
                                       StoredRow := 0;
                                       for Count2 := 1 to 12 do
                                           ValueChanged[Count2] := False;
                                       Items[Count1].SubItems[13] := '';
                                       Written := Written and True
                                     end
                                 finally
                                   APETag.Free
                                 end
                               end;
                     '.FLAC':
                               begin
                                 // Write tags to FLAC file
                                 FLACTag := TFLACfile.Create;
                                 try
                                   // Store the values
                                   FLACTag.Album := Items[Count1].SubItems[4];
                                   FLACTag.Artist := Items[Count1].SubItems[2];
                                   FLACTag.Comment := Items[Count1].SubItems[9];
                                   FLACTag.Composer :=
                                   Items[Count1].SubItems[10];
                                   FLACTag.Copyright :=
                                   Items[Count1].SubItems[11];
                                   FLACTag.DiscNumber :=
                                   Items[Count1].SubItems[7];
                                   FLACTag.Genre := Items[Count1].SubItems[8];
                                   // Get lyrics in order to keep them intact
                                   FLACTag.Lyrics :=
                                   GetLyrics(Items[Count1].SubItems[12]);
                                   // Album artist is written to TPE2
                                   FLACTag.Orchestra :=
                                   Items[Count1].SubItems[3];
                                   FLACTag.SubTitle :=
                                   Items[Count1].SubItems[1];
                                   FLACTag.Title := Items[Count1].SubItems[0];
                                   FLACTag.TrackString :=
                                   Items[Count1].SubItems[6];
                                   FLACTag.Year := Items[Count1].SubItems[5];
                                   // Attempt to save the tags to the file
                                   if not FLACTag.SaveToFile(
                                   Items[Count1].SubItems[12]) then
                                      begin
                                        ShowMessage(GetMessageText(
                                        'couldnotwritetofile') +
                                        Items[Count1].Caption);
                                        Written := Written and False
                                      end
                                   else
                                     begin
                                       StoredRow := 0;
                                       for Count2 := 1 to 12 do
                                           ValueChanged[Count2] := False;
                                       Items[Count1].SubItems[13] := '';
                                       Written := Written and True
                                     end
                                 finally
                                   FLACTag.Free
                                 end
                               end;
                     '.MP3':
                               begin
                                 // Write tags to MP3 file
                                 ID3v2Tag := TID3v2.Create;
                                 try
                                   // Store the values
                                   ID3v2Tag.Album := Items[Count1].SubItems[4];
                                   ID3v2Tag.Artist := Items[Count1].SubItems[2];
                                   ID3v2Tag.Comment :=
                                   Items[Count1].SubItems[9];
                                   ID3v2Tag.Composer :=
                                   Items[Count1].SubItems[10];
                                   ID3v2Tag.Copyright :=
                                   Items[Count1].SubItems[11];
                                   ID3v2Tag.Disc :=
                                   StrToIntDef(Items[Count1].SubItems[7], 0);
                                   ID3v2Tag.Genre := Items[Count1].SubItems[8];
                                   // Get lyrics in order to keep them intact
                                   ID3v2Tag.Lyric :=
                                   GetLyrics(Items[Count1].SubItems[12]);
                                   // Album artist is written to TPE2
                                   ID3v2Tag.Orchestra :=
                                   Items[Count1].SubItems[3];
                                   ID3v2Tag.SubTitle :=
                                   Items[Count1].SubItems[1];
                                   ID3v2Tag.Title := Items[Count1].SubItems[0];
                                   ID3v2Tag.Track :=
                                   StrToIntDef(Items[Count1].SubItems[6], 0);
                                   ID3v2Tag.Year := Items[Count1].SubItems[5];
                                   RemoveID3v1FromFile(
                                   Items[Count1].SubItems[12]);
                                   RemoveID3v2FromFile(
                                   Items[Count1].SubItems[12]);
                                   // Attempt to save the tags to the file
                                   if not ID3v2Tag.SaveToFile(
                                   Items[Count1].SubItems[12]) then
                                      begin
                                        ShowMessage(GetMessageText(
                                        'couldnotwritetofile') +
                                        Items[Count1].Caption);
                                        Written := Written and False
                                      end
                                   else
                                     begin
                                       StoredRow := 0;
                                       for Count2 := 1 to 12 do
                                           ValueChanged[Count2] := False;
                                       Items[Count1].SubItems[13] := '';
                                       Written := Written and True
                                     end
                                 finally
                                   ID3v2Tag.Free
                                 end
                               end;
                     '.OGG':
                               begin
                                 // Write tags to OGG file
                                 OGGVorbisTag := TOggVorbis.Create;
                                 try
                                   // Store the values
                                   OGGVorbisTag.Album :=
                                   Items[Count1].SubItems[4];
                                   OGGVorbisTag.Artist :=
                                   Items[Count1].SubItems[2];
                                   OGGVorbisTag.Comment :=
                                   Items[Count1].SubItems[9];
                                   OGGVorbisTag.Composer :=
                                   Items[Count1].SubItems[10];
                                   OGGVorbisTag.Copyright :=
                                   Items[Count1].SubItems[11];
                                   OGGVorbisTag.Genre :=
                                   Items[Count1].SubItems[8];
                                   // Get lyrics in order to keep them intact
                                   OGGVorbisTag.Lyrics :=
                                   GetLyrics(Items[Count1].SubItems[12]);
                                   OGGVorbisTag.Orchestra :=
                                   Items[Count1].SubItems[3];
                                   OGGVorbisTag.SubTitle :=
                                   Items[Count1].SubItems[1];
                                   OGGVorbisTag.Title :=
                                   Items[Count1].SubItems[0];
                                   OGGVorbisTag.Track :=
                                   StrToIntDef(Items[Count1].SubItems[6], 0);
                                   OGGVorbisTag.Date :=
                                   Items[Count1].SubItems[5];
                                   // Attempt to save the tags to the file
                                   if not OGGVorbisTag.SaveTag(WideString(
                                   Items[Count1].SubItems[12])) then
                                      begin
                                        ShowMessage(GetMessageText(
                                        'couldnotwritetofile') +
                                        Items[Count1].Caption);
                                        Written := Written and False
                                      end
                                   else
                                     begin
                                       StoredRow := 0;
                                       for Count2 := 1 to 12 do
                                           ValueChanged[Count2] := False;
                                       Items[Count1].SubItems[13] := '';
                                       Written := Written and True
                                     end
                                 finally
                                   OGGVorbisTag.Free
                                 end
                               end
                  end
                end
       end;
  if Written then
     MainForm.LoadPlaylist(DefaultPlaylistFileName);
  Result := Written;
  // Hide the "Please Wait" form
  if PleaseWaitForm.Visible then
     PleaseWaitForm.Hide
end;

// On clicking "Apply"
procedure TEditTagsForm.ApplyButtonClick(Sender: TObject);
begin
  if WriteTags then
     ApplyButton.Enabled := False
end;

// On clicking "Cancel"
procedure TEditTagsForm.CancelButtonClick(Sender: TObject);
var
  Cap, Msg: String;
begin
  if ApplyButton.Enabled then
     begin
       Cap := GetMessageCaption('discardchanges');
       Msg := GetMessageText('discardchanges');
       if MessageDlg(Cap, Msg, mtWarning, [mbYes, mbNo], 0) = mrYes then
          EditTagsForm.Close
     end
  else
      EditTagsForm.Close
end;

// On filelist item selection
procedure TEditTagsForm.FileListSelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
var
  Count1, Count2, Count3: Integer;
  MultiString: String;
  SItems: TStringList;
begin
  MultiString := GetFormControlText('edit_tags_form', 'multistring', 'caption');
  Count2 := 0;
  for Count1 := 1 to Tags.RowCount - 1 do
      Tags.Cells[1, Count1] := '';
  Tags.Columns[1].ReadOnly := True;
  SItems := TStringList.Create;
  // Check how many items are selected
  for Count1 := 0 to FileList.Items.Count - 1 do
      if FileList.Items[Count1].Selected then
         begin
           Tags.Columns[1].ReadOnly := False;
           Inc(Count2);
           with Tags do
                begin
                  BeginUpdate;
                  SItems.Assign(FileList.Items[Count1].SubItems);
                  for Count3 := 1 to RowCount - 1 do
                      // If only one item is selected
                      if Count2 <= 1 then
                         Cells[1, Count3] := SItems[Count3 - 1]
                      // If multiple items are selected
                      else
                          begin
                            if SItems[Count3 - 1] = Cells[1, Count3] then
                               Cells[1, Count3] := SItems[Count3 - 1]
                            else
                                Cells[1, Count3] := MultiString
                          end;
                  EndUpdate
                end
         end;
  SItems.Free
end;

// On closing the Edit Tags form
procedure TEditTagsForm.FormClose(Sender: TObject; var CloseAction: TCloseAction
  );
begin
  // On close write the position and the size of the form into the settings file
  if Height <> StrToIntDef(ReadFromSettingsFile(
  DefaultSettingsFileName, 'EDIT_TAGS_FORM_HEIGHT'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName,
     'EDIT_TAGS_FORM_HEIGHT', IntToStr(Height));
  if Left <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'EDIT_TAGS_FORM_LEFT'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName,
     'EDIT_TAGS_FORM_LEFT', IntToStr(Left));
  if Top <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'EDIT_TAGS_FORM_TOP'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'EDIT_TAGS_FORM_TOP',
     IntToStr(Top));
  if Width <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'EDIT_TAGS_FORM_WIDTH'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName,
     'EDIT_TAGS_FORM_WIDTH', IntToStr(Width));
  if Tags.Columns[0].Width <> StrToIntDef(ReadFromSettingsFile(
  DefaultSettingsFileName, 'EDIT_TAGS_TAG_COLUMN_WIDTH'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'EDIT_TAGS_TAG_COLUMN_WIDTH',
     IntToStr(Tags.Columns[0].Width));
  if Tags.Columns[1].Width <> StrToIntDef(ReadFromSettingsFile(
  DefaultSettingsFileName, 'EDIT_TAGS_VALUE_COLUMN_WIDTH'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName,
     'EDIT_TAGS_VALUE_COLUMN_WIDTH', IntToStr(Tags.Columns[1].Width))
end;

// On showing the Edit Tags form
procedure TEditTagsForm.FormShow(Sender: TObject);
var
  ETHeight, ETLeft, ETTag, ETTop, ETValue, ETWidth, Iterator: Integer;
begin
  // Load Edit Tags form display settings
  ETHeight := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'EDIT_TAGS_FORM_HEIGHT'), 0);
  ETLeft := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'EDIT_TAGS_FORM_LEFT'), 0);
  ETTag := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'EDIT_TAGS_TAG_COLUMN_WIDTH'), 0);
  ETTop := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'EDIT_TAGS_FORM_TOP'), 0);
  ETValue := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'EDIT_TAGS_VALUE_COLUMN_WIDTH'), 0);
  ETWidth := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'EDIT_TAGS_FORM_WIDTH'), 0);
  if ETHeight <> 0 then
     Height := ETHeight;
  if ETLeft <> 0 then
     Left := ETLeft;
  if ETTag <> 0 then
     Tags.Columns[0].Width := ETTag;
  if ETTop <> 0 then
     Top := ETTop;
  if ETValue <> 0 then
     Tags.Columns[1].Width := ETValue;
  if ETWidth <> 0 then
     Width := ETWidth;
  for Iterator := 1 to 12 do
      ValueChanged[Iterator] := False;
  UpdateLanguage;
  UpdateTheme;
  ApplyButton.Enabled := False
end;

// On clicking "OK"
procedure TEditTagsForm.OKButtonClick(Sender: TObject);
var
  Cap, Msg: String;
begin
  if ApplyButton.Enabled then
     begin
       Cap := GetMessageCaption('applychanges');
       Msg := GetMessageText('applychanges');
       if MessageDlg(Cap, Msg, mtWarning, [mbYes, mbNo], 0) = mrYes then
          begin
            if WriteTags then
               begin
                 ApplyButton.Enabled := False;
                 EditTagsForm.Close
               end
          end
     end
  else EditTagsForm.Close
end;

// On drawing the table cell
procedure TEditTagsForm.TagsDrawCell(Sender: TObject; aCol, aRow: Integer;
  aRect: TRect; aState: TGridDrawState);
begin
  if aRow > (Tags.FixedRows - 1) then
     with Tags do
          begin
            Canvas.Pen.Color := FixedColor;
            Canvas.Pen.Width := GridLineWidth;
            Canvas.Rectangle(aRect.Left + GridLineWidth,
            aRect.Top + GridLineWidth, aRect.Right - GridLineWidth,
            aRect.Bottom - GridLineWidth);
            Canvas.Rectangle(aRect);
            Canvas.Brush.Color := Color;
            Canvas.FillRect(aRect.Left + GridLineWidth,
            aRect.Top + GridLineWidth, aRect.Right - GridLineWidth - 1,
            aRect.Bottom - GridLineWidth - 1);
            Canvas.Font := Font;
            // If the value has been changed, draw the row in bold font
            if ValueChanged[aRow] then
               Canvas.Font.Style := [fsBold]
            else
                Canvas.Font.Style := [];
            Canvas.TextRect(aRect, aRect.Left + GridLineWidth,
            aRect.Top + GridLineWidth, Cells[aCol, aRow])
          end
  // Drawing the fixed row
  else
      with Tags do
           begin
             Canvas.Pen.Color := Color;
             Canvas.Pen.Width := GridLineWidth;
             Canvas.Rectangle(aRect);
             Canvas.Brush.Color := FixedColor;
             Canvas.FillRect(aRect.Left + GridLineWidth,
             aRect.Top + GridLineWidth, aRect.Right - GridLineWidth - 1,
             aRect.Bottom - GridLineWidth - 1);
             Canvas.Font := Font;
             Canvas.TextRect(aRect, aRect.Left + GridLineWidth,
             aRect.Top + GridLineWidth, Tags.Columns[aCol].Title.Caption)
           end
end;

// When the tag editing is finished
procedure TEditTagsForm.TagsEditingDone(Sender: TObject);
var
  Count: Integer;
  TempString: String;
begin
  if StoredRow > 0 then
     begin
       TempString := Tags.Cells[1, StoredRow];
       // Check for the correct track/disc values
       if ((StoredRow = 7) or (StoredRow = 8)) and
       (StrToIntDef(TempString, -1) < 0) and
       (TempString <> '') then
          begin
            TempString := StoredValue;
            Tags.Cells[1, StoredRow] := TempString
          end;
       // Check if the value has changed
       if TempString <> StoredValue then
          begin
          ValueChanged[StoredRow] := True;
          ApplyButton.Enabled := True;
          // Write to all the items selected
          for Count := 0 to FileList.Items.Count - 1 do
              with FileList.Items[Count] do
                   begin
                     if Selected then
                        begin
                          SubItems[13] := '1';
                          SubItems[StoredRow - 1] := TempString
                        end
                   end
     end
  end
end;

// When Ctrl+Z is pressed
procedure TEditTagsForm.TagsKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_Z) and (Shift = [ssCtrl]) then
     Tags.Cells[1, StoredRow] := StoredValue
end;

// When the tag editing is started
procedure TEditTagsForm.TagsSelectEditor(Sender: TObject; aCol, aRow: Integer;
  var Editor: TWinControl);
begin
  StoredRow := aRow;
  StoredValue := Tags.Cells[aCol, aRow]
end;

// Update the interface language
procedure TEditTagsForm.UpdateLanguage;
begin
  Caption := GetFormCaption('edit_tags_form');
  ApplyButton.Caption := GetCommonControlCaption('applybutton');
  CancelButton.Caption := GetCommonControlCaption('cancelbutton');
  OKButton.Caption := GetCommonControlCaption('okbutton');
  // Set up the table
  with Tags do
       begin
         BeginUpdate;
         RowCount := 1;
         RowCount := 13;
         Cells[0, 1] := GetTagText('title');
         Cells[0, 2] := GetTagText('subtitle');
         Cells[0, 3] := GetTagText('artist');
         Cells[0, 4] := GetTagText('albumartist');
         Cells[0, 5] := GetTagText('album');
         Cells[0, 6] := GetTagText('year');
         Cells[0, 7] := GetTagText('track');
         Cells[0, 8] := GetTagText('disc');
         Cells[0, 9] := GetTagText('genre');
         Cells[0, 10] := GetTagText('comment');
         Cells[0, 11] := GetTagText('composer');
         Cells[0, 12] := GetTagText('copyright');
         Columns[0].Title.Caption :=
         GetFormControlText('edit_tags_form', 'tags', 'tag');
         Columns[1].Title.Caption :=
         GetFormControlText('edit_tags_form', 'tags', 'value');
         EndUpdate
       end
end;

// Update the interface theme
procedure TEditTagsForm.UpdateTheme;
begin
  Color := AppTheme.Colour1;
  if AppTheme.FontName <> '' then
    Font.Name := AppTheme.FontName
  else
    Font.Name := 'default';
  Font.Color := AppTheme.FontColour1;
  ApplyButton.BackgroundColor := AppTheme.Colour1;
  ApplyButton.Color := AppTheme.Colour2;
  ApplyButton.Font.Color := AppTheme.FontColour2;
  ApplyButton.Font.Name := Font.Name;
  ApplyButton.HoverColor := AppTheme.Colour1;
  ApplyButton.HoverFontColor := AppTheme.FontColour1;
  CancelButton.BackgroundColor := AppTheme.Colour1;
  CancelButton.Color := AppTheme.Colour2;
  CancelButton.Font.Color := AppTheme.FontColour2;
  CancelButton.Font.Name := Font.Name;
  CancelButton.HoverColor := AppTheme.Colour1;
  CancelButton.HoverFontColor := AppTheme.FontColour1;
  FileList.Color := AppTheme.Colour2;
  FileList.Font.Color := AppTheme.FontColour2;
  FileList.Font.Name := Font.Name;
  OKButton.BackgroundColor := AppTheme.Colour1;
  OKButton.Color := AppTheme.Colour2;
  OKButton.Font.Color := AppTheme.FontColour2;
  OKButton.Font.Name := Font.Name;
  OKButton.HoverColor := AppTheme.Colour1;
  OKButton.HoverFontColor := AppTheme.FontColour1;
  Tags.Color := AppTheme.Colour2;
  Tags.FixedColor := AppTheme.Colour1;
  Tags.Font.Color := AppTheme.FontColour2;
  Tags.Font.Name := Font.Name;
  Tags.Columns[0].Font.Name := Font.Name;
  Tags.Columns[1].Font.Name := Font.Name
end;

end.

