{
    Lyrics form of Nocturnal Player

    Nocturnal Player is a freeware audio player for Linux with focus on 
    simplicity and functionality that an audiophile may need in everyday usage.
    Copyright (C) 2015-2016 NoSt

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
}

unit Lyrics_Form;

{$mode objfpc}{$H+}

interface

uses
  Classes, Controls, Dialogs, FileUtil, Forms, Graphics, StdCtrls, SysUtils;

type

  { TLyricsForm }

  TLyricsForm = class(TForm)
    LyricsMemo: TMemo;
    procedure FormClose(Sender: TObject; var {%H-}CloseAction: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure UpdateLanguage;
    procedure UpdateTheme;
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  LyricsForm: TLyricsForm;

implementation

uses
  FileNameConst, Nocturnal_Language, Nocturnal_Settings, Nocturnal_Themes;

{$R *.lfm}

{ TLyricsForm }

// On closing the Lyrics form
procedure TLyricsForm.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  // On close write the position and the size of the form into the settings file
  if Height <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'LYRICS_FORM_HEIGHT'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'LYRICS_FORM_HEIGHT',
     IntToStr(Height));
  if Left <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'LYRICS_FORM_LEFT'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'LYRICS_FORM_LEFT',
     IntToStr(Left));
  if Top <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'LYRICS_FORM_TOP'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'LYRICS_FORM_TOP',
     IntToStr(Top));
  if Width <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'LYRICS_FORM_WIDTH'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'LYRICS_FORM_WIDTH',
     IntToStr(Width))
end;

// On showing the Lyrics form
procedure TLyricsForm.FormShow(Sender: TObject);
var
  LHeight, LLeft, LTop, LWidth: Integer;
begin
  // Load Lyrics form display settings
  LHeight := StrToIntDef(ReadFromSettingsFile(
  DefaultSettingsFileName, 'LYRICS_FORM_HEIGHT'), 0);
  LLeft := StrToIntDef(ReadFromSettingsFile(
  DefaultSettingsFileName, 'LYRICS_FORM_LEFT'), 0);
  LTop := StrToIntDef(ReadFromSettingsFile(
  DefaultSettingsFileName, 'LYRICS_FORM_TOP'), 0);
  LWidth := StrToIntDef(ReadFromSettingsFile(
  DefaultSettingsFileName, 'LYRICS_FORM_WIDTH'), 0);
  if LHeight <> 0 then
     Height := LHeight;
  if LLeft <> 0 then
     Left := LLeft;
  if LTop <> 0 then
     Top := LTop;
  if LWidth <> 0 then
     Width := LWidth;
  UpdateLanguage;
  UpdateTheme
end;

// Update the interface language
procedure TLyricsForm.UpdateLanguage;
begin
  Caption := GetFormCaption('lyrics_form')
end;

// Update the interface theme
procedure TLyricsForm.UpdateTheme;
begin
  Color := AppTheme.Colour1;
  if AppTheme.FontName <> '' then
    Font.Name := AppTheme.FontName
  else
    Font.Name := 'default';
  Font.Color := AppTheme.FontColour1;
  LyricsMemo.Color := AppTheme.Colour2;
  LyricsMemo.Font.Color := AppTheme.FontColour2;
  LyricsMemo.Font.Name := Font.Name
end;

end.

