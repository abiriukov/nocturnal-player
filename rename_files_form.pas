{
    Rename Files form of Nocturnal Player

    Nocturnal Player is a freeware audio player for Linux with focus on 
    simplicity and functionality that an audiophile may need in everyday usage.
    Copyright (C) 2015-2016 NoSt

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
}

unit Rename_Files_Form;

{$mode objfpc}{$H+}

interface

uses
  Classes, ComCtrls, Controls, Dialogs, ExtCtrls, FileUtil, Forms, Graphics,
  NocturnalComboBox, NocturnalButton, StdCtrls, SysUtils;

type

  { TRenameFilesForm }

  TRenameFilesForm = class(TForm)
    AfterRenamingLabel: TLabel;
    UnicodeBox: TCheckBox;
    CloseButton: TNocturnalButton;
    RenameButton: TNocturnalButton;
    PatternSelection: TNocturnalComboBox;
    NowLabel: TLabel;
    FileListAfter: TListView;
    FileListNow: TListView;
    PatternLabel: TLabel;
    procedure CloseButtonClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var {%H-}CloseAction: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure PatternSelectionChange(Sender: TObject);
    procedure PatternSelectionDrawItem({%H-}Control: TWinControl; Index: Integer;
      ARect: TRect; {%H-}State: TOwnerDrawState);

    procedure RenameButtonClick(Sender: TObject);
    procedure UnicodeBoxChange(Sender: TObject);
    procedure UpdateLanguage;
    procedure UpdateTheme;
  private
    { private declarations }
    // Update the prepared renaming pattern
    procedure PatternUpdate;
  public
    { public declarations }
  end;

var
  RenameFilesForm: TRenameFilesForm;

implementation

uses
  FileNameConst, LazUTF8, Main_Form, Nocturnal_Files, Nocturnal_Language,
  Nocturnal_Playback, Nocturnal_Settings, Nocturnal_Themes, Please_Wait_Form;

{$R *.lfm}

{ TRenameFilesForm }

// On clicking "Close"
procedure TRenameFilesForm.CloseButtonClick(Sender: TObject);
begin
  Close
end;

// On "Rename Files" form activation
procedure TRenameFilesForm.FormActivate(Sender: TObject);
begin
  PatternUpdate
end;

// On closing the Rename Files form
procedure TRenameFilesForm.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
  // On close write the position and the size of the form into the settings file
  if Height <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'RENAME_FILES_FORM_HEIGHT'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'RENAME_FILES_FORM_HEIGHT',
     IntToStr(Height));
  if Left <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'RENAME_FILES_FORM_LEFT'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'RENAME_FILES_FORM_LEFT',
     IntToStr(Left));
  if Top <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'RENAME_FILES_FORM_TOP'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'RENAME_FILES_FORM_TOP',
     IntToStr(Top));
  if Width <> StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'RENAME_FILES_FORM_WIDTH'), 0) then
     WriteToSettingsFile(DefaultSettingsFileName, 'RENAME_FILES_FORM_WIDTH',
     IntToStr(Width));
  if UnicodeBox.Checked then
     WriteToSettingsFile(DefaultSettingsFileName, 'UNICODE_ALLOWED', 'ON')
  else
    WriteToSettingsFile(DefaultSettingsFileName, 'UNICODE_ALLOWED', 'OFF');
  if PatternSelection.Text <> ReadFromSettingsFile(DefaultSettingsFileName,
  'PATTERN') then
    WriteToSettingsFile(DefaultSettingsFileName, 'PATTERN',
    PatternSelection.Text)
end;

// On showing the Rename Files form
procedure TRenameFilesForm.FormShow(Sender: TObject);
var
  RFHeight, RFLeft, RFTop, RFWidth: Integer;
  Pattern, UnicodeAllowed: String;
begin
  // Load Rename Files form display settings
  RFHeight := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'RENAME_FILES_FORM_HEIGHT'), 0);
  RFLeft := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'RENAME_FILES_FORM_LEFT'), 0);
  RFTop := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'RENAME_FILES_FORM_TOP'), 0);
  RFWidth := StrToIntDef(ReadFromSettingsFile(DefaultSettingsFileName,
  'RENAME_FILES_FORM_WIDTH'), 0);
  UnicodeAllowed :=
  ReadFromSettingsFile(DefaultSettingsFileName, 'UNICODE_ALLOWED');
  case UnicodeAllowed of
       'OFF': UnicodeBox.Checked := False;
       'ON': UnicodeBox.Checked := True;
       else
         begin
           UnicodeBox.Checked := True;
           WriteToSettingsFile(DefaultSettingsFileName, 'UNICODE_ALLOWED', 'ON')
         end
  end;
  if RFHeight <> 0 then
     Height := RFHeight;
  if RFLeft <> 0 then
     Left := RFLeft;
  if RFTop <> 0 then
     Top := RFTop;
  if RFWidth <> 0 then
     Width := RFWidth;
  UpdateLanguage;
  UpdateTheme;
  FileListAfter.Clear;
  RenameButton.Enabled := False;
  Pattern := ReadFromSettingsFile(DefaultSettingsFileName, 'PATTERN');
  if Pattern = '' then
     begin
       Pattern := '%TRACK. %TITLE';
       WriteToSettingsFile(DefaultSettingsFileName, 'PATTERN', Pattern)
     end;
  PatternSelection.Text := Pattern
end;

// On choosing the renaming pattern
procedure TRenameFilesForm.PatternSelectionChange(Sender: TObject);
begin
  PatternUpdate
end;

// On drawing the pattern selection item
procedure TRenameFilesForm.PatternSelectionDrawItem(Control: TWinControl;
  Index: Integer; ARect: TRect; State: TOwnerDrawState);
begin
  with PatternSelection.Canvas do
       begin
         // Fill out the rectangle
         FillRect(ARect);
         TextRect(ARect, ARect.Left, ARect.Top, PatternSelection.Items[Index])
       end
end;

// Update the prepared renaming pattern
procedure TRenameFilesForm.PatternUpdate;
var
  Iterator: Integer;
  TempString: String;
begin
  FileListAfter.Clear;
  for Iterator := 0 to FileListNow.Items.Count - 1 do
      begin
        TempString :=
        DecodeRenamingPattern(FileListNow.Items[Iterator].SubItems,
        PatternSelection.Text, FileListNow.Items[Iterator].Caption);
        if UnicodeBox.Checked then
           FileListAfter.Items.Add.Caption := TempString
        else
          FileListAfter.Items.Add.Caption := FileNameTransliteration(TempString)
      end;
  for Iterator := 0 to FileListNow.Items.Count - 1 do
      // Enable "Rename" button only the file names are changed
      if FileListAfter.Items[Iterator].Caption <>
      FileListNow.Items[Iterator].Caption
      then
         begin
           RenameButton.Enabled := True;
           Break
         end
      else
          RenameButton.Enabled := False
end;

// On clicking "Rename"
procedure TRenameFilesForm.RenameButtonClick(Sender: TObject);
var
  FileNameAfter, FileNameNow, Msg: String;
  Iterator, Pos: Integer;
  Renamed: Boolean;
begin
  Renamed := True;
  // Try to rename all the files
  for Iterator := 0 to FileListNow.Items.Count - 1 do
      if FileListAfter.Items[Iterator].Caption <>
      FileListNow.Items[Iterator].Caption
      then
         begin
           Application.ProcessMessages;
           // Display the "Please Wait" form
           Msg := GetMessageText('renamefiles') + ': ' +
           FileListNow.Items[Iterator].SubItems[12];
           ShowPleaseWait(Msg, pbstNormal, Iterator + 1,
           FileListNow.Items.Count);
           FileNameNow := FileListNow.Items[Iterator].SubItems[12];
           FileNameAfter := GetFilePath(FileNameNow) +
           FileListAfter.Items[Iterator].Caption;
           if RenameFile(FileNameNow, FileNameAfter) then
              begin
                Pos := StrToInt(FileListNow.Items[Iterator].SubItems[13]);
                RemoveFileFromPlaylist(DefaultPlaylistFileName, Pos);
                // Add the new file name to the playlist
                AddFileToPlaylistPosition(FileNameAfter,
                DefaultPlaylistFileName, Pos);
                Renamed := Renamed and True;
                FileListNow.Items[Iterator].Caption :=
                FileListAfter.Items[Iterator].Caption;
                FileListNow.Items[Iterator].SubItems[12] := FileNameAfter
              end
           else
             Renamed := Renamed and False
         end;
  PleaseWaitForm.Hide;
  // If all the files are renamed, disable "Rename" button and refresh
  if Renamed then
     RenameButton.Enabled := False
  else
    RenameButton.Enabled := True;
  // Reload the playlist
  MainForm.LoadPlaylist(DefaultPlaylistFileName)
end;

// On Unicode box on/off
procedure TRenameFilesForm.UnicodeBoxChange(Sender: TObject);
begin
  if UnicodeBox.Checked then
     UnicodeBox.Caption :=
     GetFormControlText('rename_files_form', 'unicodebox', 'state_on')
  else
    UnicodeBox.Caption :=
    GetFormControlText('rename_files_form', 'unicodebox', 'state_off');
  PatternUpdate
end;

// Update the interface language
procedure TRenameFilesForm.UpdateLanguage;
begin
  Caption := GetFormCaption('rename_files_form');
  AfterRenamingLabel.Caption :=
  GetFormControlText('rename_files_form', 'afterrenaminglabel', 'caption');
  CloseButton.Caption :=
  GetFormControlText('rename_files_form', 'closebutton', 'caption');
  NowLabel.Caption :=
  GetFormControlText('rename_files_form', 'nowlabel', 'caption');
  PatternLabel.Caption :=
  GetFormControlText('rename_files_form', 'patternlabel', 'caption');
  RenameButton.Caption :=
  GetFormControlText('rename_files_form', 'renamebutton', 'caption');
  if UnicodeBox.Checked then
     UnicodeBox.Caption :=
     GetFormControlText('rename_files_form', 'unicodebox', 'state_on')
  else
    UnicodeBox.Caption :=
    GetFormControlText('rename_files_form', 'unicodebox', 'state_off')
end;

// Update the interface theme
procedure TRenameFilesForm.UpdateTheme;
begin
  Color := AppTheme.Colour1;
  if AppTheme.FontName <> '' then
    Font.Name := AppTheme.FontName
  else
    Font.Name := 'default';
  Font.Color := AppTheme.FontColour1;
  AfterRenamingLabel.Font.Color := AppTheme.FontColour1;
  AfterRenamingLabel.Font.Name := Font.Name;
  CloseButton.BackgroundColor := AppTheme.Colour1;
  CloseButton.Color := AppTheme.Colour2;
  CloseButton.Font.Color := AppTheme.FontColour2;
  CloseButton.Font.Name := Font.Name;
  CloseButton.HoverColor := AppTheme.Colour1;
  CloseButton.HoverFontColor := AppTheme.FontColour1;
  FileListAfter.Color := AppTheme.Colour2;
  FileListAfter.Font.Color := AppTheme.FontColour2;
  FileListAfter.Font.Name := Font.Name;
  FileListNow.Color := AppTheme.Colour2;
  FileListNow.Font.Color := AppTheme.FontColour2;
  FileListNow.Font.Name := Font.Name;
  NowLabel.Font.Color := AppTheme.FontColour1;
  NowLabel.Font.Name := Font.Name;
  PatternLabel.Font.Color := AppTheme.FontColour1;
  PatternLabel.Font.Name := Font.Name;
  PatternSelection.Color := AppTheme.Colour2;
  PatternSelection.Font.Color := AppTheme.FontColour2;
  PatternSelection.Font.Name := Font.Name;
  PatternSelection.SelectionColor := AppTheme.Colour1;
  PatternSelection.SelectionFontColor := AppTheme.FontColour1;
  RenameButton.BackgroundColor := AppTheme.Colour1;
  RenameButton.Color := AppTheme.Colour2;
  RenameButton.Font.Color := AppTheme.FontColour2;
  RenameButton.Font.Name := Font.Name;
  RenameButton.HoverColor := AppTheme.Colour1;
  RenameButton.HoverFontColor := AppTheme.FontColour1;
  UnicodeBox.Font.Color := AppTheme.FontColour1;
  UnicodeBox.Font.Name := Font.Name
end;

end.

