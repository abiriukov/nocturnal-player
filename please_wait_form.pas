{
    Please Wait form of Nocturnal Player

    Nocturnal Player is a freeware audio player for Linux with focus on
    simplicity and functionality that an audiophile may need in everyday usage.
    Copyright (C) 2015-2016 NoSt

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
}

unit Please_Wait_Form;

{$mode objfpc}{$H+}

interface

uses
  Classes, ComCtrls, Controls, Dialogs, FileUtil, Forms, Graphics, StdCtrls,
  SysUtils;

type

  { TPleaseWaitForm }

  TPleaseWaitForm = class(TForm)
    PleaseWaitLabel: TLabel;
    PleaseWaitBar: TProgressBar;
    procedure FormShow(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

  // Show Please Wait form
  // Msg - the message string
  // ProgressBarStyle - the progress bar style
  // Position - current progress bar position, default: 0
  // Max - max. value of the progress bar position, default: 100
  procedure ShowPleaseWait(const Msg: String;
    ProgressBarStyle: TProgressBarStyle; Pos: Integer = 0; Max: Integer = 100);

var
  PleaseWaitForm: TPleaseWaitForm;

implementation

uses
  Nocturnal_Themes;

{$R *.lfm}

{ TPleaseWaitForm }

// Show Please Wait form
// Msg - the message string
// ProgressBarStyle - the progress bar style
// Position - current progress bar position, default: 0
// Max - max. value of the progress bar position, default: 100
procedure ShowPleaseWait(const Msg: String; ProgressBarStyle: TProgressBarStyle;
  Pos: Integer = 0; Max: Integer = 100);
begin
  with PleaseWaitForm do
       begin
         PleaseWaitLabel.Caption := Msg;
         PleaseWaitBar.Style := ProgressBarStyle;
         PleaseWaitBar.Position := Pos;
         PleaseWaitBar.Max := Max;
         if not Visible then
            Show
       end
end;

// On showing the form
procedure TPleaseWaitForm.FormShow(Sender: TObject);
begin
  Color := AppTheme.Colour1;
  if AppTheme.FontName <> '' then
    Font.Name := AppTheme.FontName
  else
    Font.Name := 'default';
  Font.Color := AppTheme.FontColour1;
  PleaseWaitBar.Color := AppTheme.Colour2;
  PleaseWaitBar.Position := 0;
  PleaseWaitLabel.Font.Color := AppTheme.FontColour1;
  PleaseWaitLabel.Font.Name := Font.Name
end;

end.

