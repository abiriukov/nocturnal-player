program Nocturnal_Player;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  CThreads,
  {$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, Main_Form;

{$R *.res}

begin
  Application.Scaled:=True;
  Application.Title:='Nocturnal Player';
  RequireDerivedFormResource := True;
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run
end.

