{
    Miscellaneous utilities for Nocturnal Player

    Nocturnal Player is a freeware audio player for Linux with focus on
    simplicity and functionality that an audiophile may need in everyday usage.
    Copyright (C) 2015-2016 NoSt

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
}

unit Nocturnal_Misc;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

// Get the program version information
function ResourceVersionInfo: String;
// Sort strings by inserting
// InputStrings - the list of strings to sort
// Ascending - if True sort the strings in ascending order,
// if False - in descending order
procedure SortByInsert(const InputStrings: TStrings; Ascending: Boolean);

implementation

uses
  VersionResource, VersionTypes;

// Get the program version information
function ResourceVersionInfo: String;
var
  ResStream: TResourceStream;
  VerFixedInfo: TVersionFixedInfo;
  VerRes: TVersionResource;
begin
     Result := '';
     ResStream :=
     TResourceStream.CreateFromID(HINSTANCE, 1, PChar(RT_VERSION));
     try
       VerRes := TVersionResource.Create;
       try
         VerRes.SetCustomRawDataStream(ResStream);
         VerFixedInfo := VerRes.FixedInfo;
         Result := 'v.' + IntToStr(VerFixedInfo.FileVersion[0]) + '.' +
         IntToStr(VerFixedInfo.FileVersion[1]) + '.' +
         IntToStr(VerFixedInfo.FileVersion[2]) + '.' +
         IntToStr(VerFixedInfo.FileVersion[3]);
         VerRes.SetCustomRawDataStream(nil)
       finally
         VerRes.Free
       end
     finally
       ResStream.Free
     end
end;

// Sort strings by inserting
// InputStrings - the list of strings to sort
// Ascending - if True sort the strings in ascending order,
// if False - in descending order
procedure SortByInsert(const InputStrings: TStrings; Ascending: Boolean);
var
  Count1, Count2, Max, Min: Integer;
  TempString: String;
begin
  // Ascending order
  if Ascending then
     begin
       // Searching for the minimal string in the list
       Min := 0;
       for Count1 := 1 to InputStrings.Count - 1 do
           if InputStrings[Count1] < InputStrings[Min] then
              Min := Count1;
       // Changing the first (0) string in the list to the minimal string
       TempString := InputStrings[0];
       InputStrings[0] := InputStrings[Min];
       InputStrings[Min] := TempString;
       // Sorting the list by inserting
       for Count1 := 1 to InputStrings.Count - 1 do
          begin
               TempString := InputStrings[Count1];
               Count2 := Count1 - 1;
               while InputStrings[Count2] > TempString do
                 begin
                      InputStrings[Count2 + 1] := InputStrings[Count2];
                      Dec(Count2)
                 end;
               InputStrings[Count2 + 1] := TempString
          end
     end
  // Descending order
  else
      begin
           // Searching for the maximal string in the list
           Max := 0;
           for Count1 := 1 to InputStrings.Count - 1 do
               if InputStrings[Count1] > InputStrings[Max] then
                  Max := Count1;
           // Changing the first (0) string in the list to the maximal string
           TempString := InputStrings[0];
           InputStrings[0] := InputStrings[Max];
           InputStrings[Max] := TempString;
           // Sorting the list by inserting
           for Count1 := 1 to InputStrings.Count - 1 do
              begin
               TempString := InputStrings[Count1];
               Count2 := Count1 - 1;
               while InputStrings[Count2] < TempString do
                 begin
                      InputStrings[Count2 + 1] := InputStrings[Count2];
                      Dec(Count2)
                 end;
               InputStrings[Count2 + 1] := TempString
          end
      end
end;

end.

