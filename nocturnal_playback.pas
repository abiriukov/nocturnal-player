{
    Playback-related utilities for Nocturnal Player

    Nocturnal Player is a freeware audio player for Linux with focus on
    simplicity and functionality that an audiophile may need in everyday usage.
    Copyright (C) 2015-2016 NoSt

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
}

unit Nocturnal_Playback;

{$mode objfpc}{$H+}

interface

uses
  BASS, Classes, ExtCtrls, Forms, SysUtils;

type

  PTrack = ^TTrack;
  // Track type containing the audio file information
  TTrack = Record
    FileName: String;
    Title: String;
    Artist: String;
    Album: String;
    Year: String;
    Comment: String;
    Genre: String;
    Composer: String;
    Copyright: String;
    Subtitle: String;
    AlbumArtist: String;
    Track: String;
    Disc: String
    end;

  TTracks = Array of TTrack;

  TAlbum = record
    Name: String;
    Disc: String;
    Year: String;
    Tracks: TTracks
  end;

  TAlbums = Array of TAlbum;

  TArtist = record
    Name: String;
    Albums: TAlbums
  end;

  TArtists = Array of TArtist;

// Check for the floating point playback support
function FloatingPointCheck: Boolean;
// Get the string describing the error of BASS from its code
// ErrorCode - the code of the error
function GetBASSErrorFromCode(const ErrorCode: Integer): String;
// Get the cover image file name
// FilePath - the path to search for the cover image in
function GetCoverImageFile(const FilePath: String): String;
// Get cover image from the audio file tag
// InputFileName - the name of the audio file
function GetCoverImageTag(const InputFileName: String): String;
// Decode ID3v1 genre tag
// GenreCode - the code of the genre according to ID3v1 specification
function GetGenre(const GenreCode: Byte): String;
// Convert the length of the track(s) from QWORD into "HH:MM:SS" formatted string
// TrackStream - the stream to use
// LengthByte - the length as QWORD
function GetLengthFromByte(const TrackStream: HSTREAM; LenByte: QWORD): String;
// Get the lyrics out of the audio file
// InputFileName - the name of the audio file
function GetLyrics(const InputFileName: String): String;
// Get the value of OGG comments field
// Comments - a pointer to a series of null-terminated UTF-8 strings
// Field - the field to search for, e.g. 'ARTIST='
function GetOGGCommentsFieldValue(Comments: PChar; const Field: String): String;
// Get tags from an audio file
// InputFileName - the name of the audio file
function GetTags(const InputFileName: String): TTrack;
// Get the length of the track from the stream as "HH:MM:SS" formatted string
// TrackStream - the stream to get the length from
function GetTrackLength(const TrackStream: HSTREAM): String;
// Check, if a file is a CD track
// FileName - the name of the file
function IsCDTrack(FileName: String): Boolean;
// Check, if a file is a MIDI
// FileName - the name of the file
function IsMIDI(FileName: String): Boolean;
// Check, if a file is an URL
// FileName - the name of the file
function IsURL(FileName: String): Boolean;
// Resizes the cover image so that it fits into the form
// Image - an image to resize
// Form - a form to fit the image into
function ResizeCoverImage(const Image: TImage; const Form: TForm): TImage;
// Add CD to the playlist
// PlaylistFileName - the name of the playlist file
// TrackQuantity - the total quantity of tracks
procedure AddCDToPlaylist(const PlaylistFileName: String;
  TrackQuantity: Byte);
// Add a file to the specified playlist position
// FileName - the name of the file to be added to the playlist
// PlaylistFileName - the name of the playlist file
// Position - the position in the playlist file for the file to be added in
procedure AddFileToPlaylistPosition(const FileName: String;
  PlaylistFileName: String; Position: Integer);
// Add files to the playlist
// FileList - the list of file names to be added to the playlist file
// PlaylistFileName - the name of the playlist file
procedure AddFilesToPlaylist(const FileList: TStrings;
  PlaylistFileName: String);
// Add folders to the collection
// FolderList - the list of folder names to be added to the collection file
// CollectionFileName - the name of the collection file
procedure AddFoldersToCollection(const FolderList: TStrings;
  CollectionFileName: String);
// Add URL to the playlist
// PlaylistFileName - the name of the playlist file
procedure AddURLToPlaylist(const URL, PlaylistFileName: String);
// Remove a file from the playlist
// PlaylistFileName - the name of the playlist file
// FilePos - the position of a file to remove in the playlist file
procedure RemoveFileFromPlaylist(const PlaylistFileName: String;
  FilePos: Integer);
// Removes ID3v1 tag from audio file
// FileName - the name of the file
procedure RemoveID3v1FromFile(const FileName: String);
// Removes ID3v2 tag from audio file
// FileName - the name of the file
procedure RemoveID3v2FromFile(const FileName: String);

var
  // All the artists
  Artists: TArtists;
  // Equalizer parameters record
  Equalizer: BASS_DX8_PARAMEQ;
  // Equalizer effect
  EqualizerFX: HFX;
  // Equalizer On/Off
  EQOn: Boolean;
  // Playback stream
  PlayerStream: HSTREAM;
  // Playback volume
  PlayerVolume: Single;
  // Position in the playlist
  PlaylistPosition: Integer;
  // Selected CD/DVD drive
  SetDrive: Integer;
  // Temporarily stored playback volume
  TempPlayerVolume: Single;
  // All the tracks
  Tracks: TTracks;

implementation

uses
  AACfile, APEtag, BASSMIDI, FLACfile, Graphics, LazUTF8, LazUTF8Classes,
  LCLIntf, ID3v1, ID3v2, Nocturnal_Files, OggVorbis;

// Check for the floating point playback support
function FloatingPointCheck: Boolean;
var
  FloatingPointStream: DWORD;
begin
  Result := False;
  // Trying to create the floating point stream
  FloatingPointStream := BASS_StreamCreate(44100, 1, BASS_SAMPLE_FLOAT, nil,
  nil);
  // If the stream is created, free the stream
  if FloatingPointStream <> 0 then
     begin
       BASS_StreamFree(FloatingPointStream);
       Result := True
     end
end;

// Get the string describing the error of BASS from its code
// ErrorCode - the code of the error
function GetBASSErrorFromCode(const ErrorCode: Integer): String;
begin
  Result := '';
  case ErrorCode of
       -1: Result := 'BASS_ERROR_UNKNOWN';
       1: Result := 'BASS_ERROR_MEM';
       2: Result := 'BASS_ERROR_FILEOPEN';
       3: Result := 'BASS_ERROR_DRIVER';
       4: Result := 'BASS_ERROR_BUFLOST';
       5: Result := 'BASS_ERROR_HANDLE';
       6: Result := 'BASS_ERROR_FORMAT';
       7: Result := 'BASS_ERROR_POSITION';
       8: Result := 'BASS_ERROR_INIT';
       9: Result := 'BASS_ERROR_START';
       10: Result := 'BASS_ERROR_SSL';
       12: Result := 'BASS_ERROR_NOCD';
       13: Result := 'BASS_ERROR_CDTRACK';
       14: Result := 'BASS_ERROR_ALREADY';
       17: Result := 'BASS_ERROR_NOTAUDIO';
       18: Result := 'BASS_ERROR_NOCHAN';
       19: Result := 'BASS_ERROR_ILLTYPE';
       20: Result := 'BASS_ERROR_ILLPARAM';
       21: Result := 'BASS_ERROR_NO3D';
       22: Result := 'BASS_ERROR_NOEAX';
       23: Result := 'BASS_ERROR_DEVICE';
       24: Result := 'BASS_ERROR_NOPLAY';
       25: Result := 'BASS_ERROR_FREQ';
       27: Result := 'BASS_ERROR_NOTFILE';
       29: Result := 'BASS_ERROR_NOHW';
       31: Result := 'BASS_ERROR_EMPTY';
       32: Result := 'BASS_ERROR_NONET';
       33: Result := 'BASS_ERROR_CREATE';
       34: Result := 'BASS_ERROR_NOFX';
       37: Result := 'BASS_ERROR_NOTAVAIL';
       38: Result := 'BASS_ERROR_DECODE';
       39: Result := 'BASS_ERROR_DX';
       40: Result := 'BASS_ERROR_TIMEOUT';
       41: Result := 'BASS_ERROR_FILEFORM';
       42: Result := 'BASS_ERROR_SPEAKER';
       43: Result := 'BASS_ERROR_VERSION';
       44: Result := 'BASS_ERROR_CODEC';
       45: Result := 'BASS_ERROR_ENDED';
       46: Result := 'BASS_ERROR_BUSY';
       1000: Result := 'BASS_ERROR_WMA_LICENSE';
       1001: Result := 'BASS_ERROR_WMA';
       1002: Result := 'BASS_ERROR_WMA_DENIED';
       1004: Result := 'BASS_ERROR_WMA_INDIVIDUAL';
       1005: Result := 'BASS_ERROR_WMA_PUBINIT';
       6000: Result := 'BASS_ERROR_MP4_NOSTREAM'
  end;
end;

// Get the cover image file name
// FilePath - the path to search for the cover image in
function GetCoverImageFile(const FilePath: String): String;
var
  FileInfo: TSearchRec;
  ImageFilesList: TStringListUTF8;
  Iterator: Integer;
begin
  Result := '';
  ImageFilesList := TStringListUTF8.Create;
  try
    // Add all JPEG files to the list
    if FindFirst(FilePath + '*.jpg', faAnyFile and not faDirectory,
    FileInfo) = 0 then
     begin
       repeat
         ImageFilesList.Add(FileInfo.Name)
       until FindNext(FileInfo) <> 0;
       FindClose(FileInfo)
     end;
    if FindFirst(FilePath + '*.jpeg', faAnyFile and not faDirectory,
    FileInfo) = 0 then
     begin
       repeat
         ImageFilesList.Add(FileInfo.Name)
       until FindNext(FileInfo) <> 0;
     FindClose(FileInfo)
     end;
    // Add all PNG files to the list
    if FindFirst(FilePath + '*.png', faAnyFile and not faDirectory,
    FileInfo) = 0 then
     begin
       repeat
         ImageFilesList.Add(FileInfo.Name)
       until FindNext(FileInfo) <> 0;
     FindClose(FileInfo)
     end;
    // If any image files were found
    if ImageFilesList.Count > 0 then
     begin
       // Get the first image file's name
       Result := FilePath + ImageFilesList[0];
       // Search for file names containing "Cover" or "cover"
       for Iterator := 1 to ImageFilesList.Count - 1 do
        if (UTF8Pos('Cover', ImageFilesList[Iterator]) > 0) or
        (UTF8Pos('cover', ImageFilesList[Iterator]) > 0) then
           begin
             Result := FilePath + ImageFilesList[Iterator];
             Break
           end
     end
  finally
    ImageFilesList.Free
  end
end;

// Get cover image from the audio file tag
// InputFileName - the name of the audio file
function GetCoverImageTag(const InputFileName: String): String;
var
  AACTag: TAACfile;
  FLACTag: TFLACfile;
  FLACTagCoverArtInfo: TFLACTagCoverArtInfo;
  ID3v2Tag: TID3v2;
  PictureStream: TStream;
begin
  Result := '';
  Application.ProcessMessages;
  case UpperCase(ExtractFileExt(InputFileName)) of
       '.AAC', '.M4A', '.MP4':
                begin
                  // Try to read AAC, M4A or MP4 cover image
                  AACTag := TAACfile.Create;
                  try
                    if AACTag.ReadFromFile(WideString(InputFileName)) and
                    AACTag.ID3v2.Exists then
                      Result := AACTag.ID3v2.CDPicture;
                  finally
                    AACTag.Free
                  end
                end;
       '.FLAC':
                begin
                  // Try to read FLAC cover image
                  FLACTag := TFLACfile.Create;
                  try
                    if FLACTag.ReadFromFile(InputFileName) and
                    FLACTag.Exists then
                       begin
                         PictureStream := TMemoryStreamUTF8.Create;
                         try
                           if FLACTag.GetCoverArt(0, PictureStream,
                           FLACTagCoverArtInfo) then
                             Result := PictureStream.ReadAnsiString;
                         finally
                           PictureStream.Free
                         end
                       end
                  finally
                    FLACTag.Free
                  end
                end;
       '.MP3':
                begin
                  // Try to read ID3v2 cover image
                  ID3v2Tag := TID3v2.Create;
                  try
                    if ID3v2Tag.ReadFromFile(InputFileName) and
                    ID3v2Tag.Exists then
                      Result := ID3v2Tag.CDPicture
                  finally
                    ID3v2Tag.Free
                  end
                end
  end
end;

// Decode ID3v1 genre tag
// GenreCode - the code of the genre according to ID3v1 specification
function GetGenre(const GenreCode: Byte): String;
begin
  Result := 'Unknown';
  case GenreCode of
       0: Result := 'Blues';
       1: Result := 'Classic Rock';
       2: Result := 'Country';
       3: Result := 'Dance';
       4: Result := 'Disco';
       5: Result := 'Funk';
       6: Result := 'Grunge';
       7: Result := 'Hip-Hop';
       8: Result := 'Jazz';
       9: Result := 'Metal';
       10: Result := 'New Age';
       11: Result := 'Oldies';
       12: Result := 'Other';
       13: Result := 'Pop';
       14: Result := 'Rhythm and Blues';
       15: Result := 'Rap';
       16: Result := 'Reggae';
       17: Result := 'Rock';
       18: Result := 'Techno';
       19: Result := 'Industrial';
       20: Result := 'Alternative';
       21: Result := 'Ska';
       22: Result := 'Death Metal';
       23: Result := 'Pranks';
       24: Result := 'Soundtrack';
       25: Result := 'Euro-Techno';
       26: Result := 'Ambient';
       27: Result := 'Trip-Hop';
       28: Result := 'Vocal';
       29: Result := 'Jazz & Funk';
       30: Result := 'Fusion';
       31: Result := 'Trance';
       32: Result := 'Classical';
       33: Result := 'Instrumental';
       34: Result := 'Acid';
       35: Result := 'House';
       36: Result := 'Game';
       37: Result := 'Sound Clip';
       38: Result := 'Gospel';
       39: Result := 'Noise';
       40: Result := 'Alternative Rock';
       41: Result := 'Bass';
       42: Result := 'Soul';
       43: Result := 'Punk Rock';
       44: Result := 'Space';
       45: Result := 'Meditative';
       46: Result := 'Instrumental Pop';
       47: Result := 'Instrumental Rock';
       48: Result := 'Ethnic';
       49: Result := 'Gothic';
       50: Result := 'Darkwave';
       51: Result := 'Techno-Industrial';
       52: Result := 'Electronic';
       53: Result := 'Pop-Folk';
       54: Result := 'Eurodance';
       55: Result := 'Dream';
       56: Result := 'Southern Rock';
       57: Result := 'Comedy';
       58: Result := 'Cult';
       59: Result := 'Gangsta';
       60: Result := 'Top 40';
       61: Result := 'Christian Rap';
       62: Result := 'Pop/Funk';
       63: Result := 'Jungle';
       64: Result := 'Native American';
       65: Result := 'Cabaret';
       66: Result := 'New Wave';
       67: Result := 'Psychedelic';
       68: Result := 'Rave';
       69: Result := 'Showtunes';
       70: Result := 'Trailer';
       71: Result := 'Lo-Fi';
       72: Result := 'Tribal';
       73: Result := 'Acid Punk';
       74: Result := 'Acid Jazz';
       75: Result := 'Polka';
       76: Result := 'Retro';
       77: Result := 'Musical';
       78: Result := 'Rock & Roll';
       79: Result := 'Hard Rock';
       80: Result := 'Folk';
       81: Result := 'Folk-Rock';
       82: Result := 'National Folk';
       83: Result := 'Swing';
       84: Result := 'Fast Fusion';
       85: Result := 'Bebop';
       86: Result := 'Latin';
       87: Result := 'Revival';
       88: Result := 'Celtic';
       89: Result := 'Bluegrass';
       90: Result := 'Avantgarde';
       91: Result := 'Gothic Rock';
       92: Result := 'Progressive Rock';
       93: Result := 'Psychedelic Rock';
       94: Result := 'Symphonic Rock';
       95: Result := 'Slow Rock';
       96: Result := 'Big Band';
       97: Result := 'Chorus';
       98: Result := 'Easy Listening';
       99: Result := 'Acoustic';
       100: Result := 'Humour';
       101: Result := 'Speech';
       102: Result := 'Chanson';
       103: Result := 'Opera';
       104: Result := 'Chamber Music';
       105: Result := 'Sonata';
       106: Result := 'Symphony';
       107: Result := 'Booty Bass';
       108: Result := 'Primus';
       109: Result := 'Porn groove';
       110: Result := 'Satire';
       111: Result := 'Slow Jam';
       112: Result := 'Club';
       113: Result := 'Tango';
       114: Result := 'Samba';
       115: Result := 'Folklore';
       116: Result := 'Ballad';
       117: Result := 'Power Ballad';
       118: Result := 'Rhythmic Soul';
       119: Result := 'Freestyle';
       120: Result := 'Duet';
       121: Result := 'Punk rock';
       122: Result := 'Drum Solo';
       123: Result := 'A capella';
       124: Result := 'Euro-House';
       125: Result := 'Dance Hall';
       126: Result := 'Goa Trance';
       127: Result := 'Drum & Bass';
       128: Result := 'Club-House';
       129: Result := 'Hardcore Techno';
       130: Result := 'Terror';
       131: Result := 'Indie';
       132: Result := 'BritPop';
       133: Result := 'Afro-punk';
       134: Result := 'Polsk Punk';
       135: Result := 'Beat';
       136: Result := 'Christian Gangsta Rap';
       137: Result := 'Heavy Metal';
       138: Result := 'Black Metal';
       139: Result := 'Crossover';
       140: Result := 'Contemporary Christian';
       141: Result := 'Christian Rock';
       142: Result := 'Merengue';
       143: Result := 'Salsa';
       144: Result := 'Thrash Metal';
       145: Result := 'Anime';
       146: Result := 'Jpop';
       147: Result := 'Synthpop';
       148: Result := 'Abstract';
       149: Result := 'Art Rock';
       150: Result := 'Baroque';
       151: Result := 'Bhangra';
       152: Result := 'Big Beat';
       153: Result := 'Breakbeat';
       154: Result := 'Chillout';
       155: Result := 'Downtempo';
       156: Result := 'Dub';
       157: Result := 'EBM';
       158: Result := 'Eclectic';
       159: Result := 'Electro';
       160: Result := 'Electroclash';
       161: Result := 'Emo';
       162: Result := 'Experimental';
       163: Result := 'Garage';
       164: Result := 'Global';
       165: Result := 'IDM';
       166: Result := 'Illbient';
       167: Result := 'Industro-Goth';
       168: Result := 'Jam Band';
       169: Result := 'Krautrock';
       170: Result := 'Leftfield';
       171: Result := 'Lounge';
       172: Result := 'Math Rock';
       173: Result := 'New Romantic';
       174: Result := 'Nu-Breakz';
       175: Result := 'Post-Punk';
       176: Result := 'Post-Rock';
       177: Result := 'Psytrance';
       178: Result := 'Shoegaze';
       179: Result := 'Space Rock';
       180: Result := 'Trop Rock';
       181: Result := 'World Music';
       182: Result := 'Neoclassical';
       183: Result := 'Audiobook';
       184: Result := 'Audio Theatre';
       185: Result := 'Neue Deutsche Welle';
       186: Result := 'Podcast';
       187: Result := 'Indie Rock';
       188: Result := 'G-Funk';
       189: Result := 'Dubstep';
       190: Result := 'Garage Rock';
       191: Result := 'Psybient'
  end
end;

// Get the lyrics out of the audio file
// InputFileName - the name of the audio file
function GetLyrics(const InputFileName: String): String;
var
  APETag: TAPEtag;
  FLACTag: TFLACfile;
  ID3v1Tag: TID3v1;
  ID3v2Tag: TID3v2;
  OGGVorbisTag: TOggVorbis;
begin
  Result := '';
  Application.ProcessMessages;
  case UpperCase(ExtractFileExt(InputFileName)) of
       '.APE':
                begin
                  // Try to read APE lyrics
                  APETag := TAPEtag.Create;
                  try
                    if APETag.ReadFromFile(WideString(InputFileName)) and
                    APETag.Exists then
                      Result := APETag.SeekField('LYRICS')
                  finally
                    APETag.Free
                  end
                end;
       '.FLAC':
                begin
                  // Try to read FLAC lyrics
                  FLACTag := TFLACfile.Create;
                  try
                    if FLACTag.ReadFromFile(InputFileName) and
                    FLACTag.HasLyrics then
                      Result := FLACTag.Lyrics
                  finally
                    FLACTag.Free
                  end
                end;
       '.MP3':
                begin
                  // Try to read ID3v2 lyrics
                  ID3v2Tag := TID3v2.Create;
                  try
                    if ID3v2Tag.ReadFromFile(InputFileName) and
                    ID3v2Tag.Exists then
                      Result := ID3v2Tag.Lyric
                    else
                      // Try to read ID3v1 lyrics
                      begin
                        ID3v1Tag := TID3v1.Create;
                        try
                          if ID3v1Tag.ReadFromFile(WideString(InputFileName))
                          and ID3v1Tag.HasLyrics then
                            Result := ID3v1Tag.Lyrics
                        finally
                          ID3v1Tag.Free
                        end
                      end
                  finally
                    ID3v2Tag.Free
                  end
                end;
       '.OGG':
                begin
                  // Try to read OGG lyrics
                  OGGVorbisTag := TOggVorbis.Create;
                  try
                    if OGGVorbisTag.ReadFromFile(WideString(InputFileName)) then
                      Result := OGGVorbisTag.Lyrics
                  finally
                    OGGVorbisTag.Free
                  end
                end
  end
end;

// Get the value of OGG comments field
// Comments - a pointer to a series of null-terminated UTF-8 strings
// Field - the field to search for, e.g. 'ARTIST='
function GetOGGCommentsFieldValue(Comments: PChar; const Field: String): String;
var
  FieldPos, Iterator: Integer;
  TempString: String;
begin
  Result := '';
  for Iterator := 1 to 100 do
     begin
          TempString := '';
          while Comments^ <> #0 do
            begin
              Application.ProcessMessages;
              TempString := TempString + Comments^;
              Inc(Comments)
            end;
          FieldPos := UTF8Pos(Field, TempString);
          if FieldPos > 0 then
             begin
               UTF8Delete(TempString, FieldPos, UTF8Length(Field));
               Result := TempString;
               Break
             end;
          Inc(Comments);
          if Comments^ = #0 then
             Break
     end
end;

// Get tags from an audio file
// InputFileName - the name of the audio file
function GetTags(const InputFileName: String): TTrack;
var
  AACTag: TAACfile;
  APETag: TAPEtag;
  FLACTag: TFLACfile;
  ID3v1Tag: TID3v1;
  ID3v2Tag: TID3v2;
  OGGVorbisTag: TOggVorbis;
  TempInt: Integer;
  TempStream: HSTREAM;
  TempString: String;
  TempTrack: TTrack;
begin
  // Initial tag values
  with TempTrack do
       begin
         FileName := InputFileName;
         Artist := '';
         Title := GetShortFileName(InputFileName);
         Album := '';
         Year := '';
         Comment := '';
         Genre := '';
         Composer := '';
         Copyright := '';
         Subtitle := '';
         AlbumArtist := '';
         Track := '';
         Disc := ''
       end;
  Application.ProcessMessages;
  // Read the tag values
  case UpperCase(ExtractFileExt(InputFileName)) of
       '.AAC', '.M4A', '.MP4':
                begin
                  AACTag := TAACfile.Create;
                  try
                    if AACTag.ReadFromFile(WideString(InputFileName)) and
                    (AACTag.APEtag.Exists or AACTag.ID3v1.Exists or
                    AACTag.ID3v2.Exists) then
                     begin
                       if AACTag.ID3v2.Exists then
                         with TempTrack do
                            begin
                              Artist := AACTag.ID3v2.Artist;
                              Title := AACTag.ID3v2.Title;
                              Album := AACTag.ID3v2.Album;
                              Year := AACTag.ID3v2.Year;
                              Comment := AACTag.ID3v2.Comment;
                              Genre := AACTag.ID3v2.Genre;
                              Composer := AACTag.ID3v2.Composer;
                              Copyright := AACTag.ID3v2.Copyright;
                              Subtitle := AACTag.ID3v2.SubTitle;
                              // Album artist is read from TPE2
                              AlbumArtist := AACTag.ID3v2.Orchestra;
                              Track := IntToStr(AACTag.ID3v2.Track);
                              Disc := IntToStr(AACTag.ID3v2.Disc)
                            end
                       else
                           begin
                             if AACTag.APEtag.Exists then
                               with TempTrack do
                                  begin
                                   Artist := AACTag.APEtag.SeekField('Artist');
                                   Title := AACTag.APEtag.SeekField('Title');
                                   Album := AACTag.APEtag.SeekField('Album');
                                   Year := AACTag.APEtag.SeekField('Year');
                                   Comment :=
                                   AACTag.APEtag.SeekField('Comment');
                                   Genre := AACTag.APEtag.SeekField('Genre');
                                   Composer :=
                                   AACTag.APEtag.SeekField('Composer');
                                   Copyright :=
                                   AACTag.APEtag.SeekField('Copyright');
                                   Subtitle :=
                                   AACTag.APEtag.SeekField('SUBTITLE');
                                   AlbumArtist :=
                                   AACTag.APEtag.SeekField('ORCHESTRA');
                                   Track := AACTag.APEtag.SeekField('TRACK')
                                  end
                             else
                                 with TempTrack do
                                    begin
                                      Artist := AACTag.ID3v1.Artist;
                                      Title := AACTag.ID3v1.Title;
                                      Album := AACTag.ID3v1.Album;
                                      Year := AACTag.ID3v1.Year;
                                      Comment := AACTag.ID3v1.Comment;
                                      Genre := AACTag.ID3v1.Genre;
                                      Track := IntToStr(AACTag.ID3v1.Track)
                                    end
                           end
                     end
                  finally
                    AACTag.Free
                  end
                end;
       '.APE':
                begin
                  APETag := TAPEtag.Create;
                  try
                    if APETag.ReadFromFile(WideString(InputFileName)) and
                    APETag.Exists then
                      with TempTrack do
                       begin
                         Artist := APETag.SeekField('Artist');
                         Title := APETag.SeekField('Title');
                         Album := APETag.SeekField('Album');
                         Year := APETag.SeekField('Year');
                         Comment := APETag.SeekField('Comment');
                         Genre := APETag.SeekField('Genre');
                         Composer := APETag.SeekField('Composer');
                         Copyright := APETag.SeekField('Copyright');
                         Subtitle := APETag.SeekField('SUBTITLE');
                         AlbumArtist := APETag.SeekField('ORCHESTRA');
                         Track := APETag.SeekField('TRACK')
                       end
                  finally
                    APETag.Free
                  end
                end;
       '.FLAC':
                begin
                  FLACTag := TFLACFile.Create;
                  try
                    if FLACTag.ReadFromFile(InputFileName) and
                    FLACTag.Exists then
                      with TempTrack do
                         begin
                          Artist := FLACTag.Artist;
                          Title := FLACTag.Title;
                          Album := FLACTag.Album;
                          Year := FLACTag.Year;
                          Comment := FLACTag.Comment;
                          Genre := FLACTag.Genre;
                          Composer := FLACTag.Composer;
                          Copyright := FLACTag.Copyright;
                          Subtitle := FLACTag.SubTitle;
                          // Album artist is read from TPE2
                          AlbumArtist := FLACTag.Orchestra;
                          Track := FLACTag.TrackString;
                          Disc := FLACTag.DiscNumber
                         end
                  finally
                    FLACTag.Free
                  end
                end;
       '.KAR', '.MID', '.MIDI', '.RMI':
                begin
                  TempStream :=
                  BASS_MIDI_StreamCreateFile(False, PChar(InputFileName), 0, 0,
                  BASS_STREAM_DECODE, 0);
                  try
                    if TempStream <> 0 then
                     begin
                       TempString := String(BASS_ChannelGetTags(TempStream,
                       BASS_TAG_MIDI_TRACK));
                       if TempString <> '' then
                         TempTrack.Title := TempString
                     end
                  finally
                    if not BASS_StreamFree(TempStream) then
                      GetBASSErrorFromCode(BASS_ErrorGetCode)
                  end
                end;
       '.MP3':
                begin
                  // Try to read ID3v2 tags
                  ID3v2Tag := TID3v2.Create;
                  try
                    if ID3v2Tag.ReadFromFile(InputFileName) and
                    ID3v2Tag.Exists then
                      with TempTrack do
                         begin
                          Artist := ID3v2Tag.Artist;
                          Title := ID3v2Tag.Title;
                          Album := ID3v2Tag.Album;
                          Year := ID3v2Tag.Year;
                          Comment := ID3v2Tag.Comment;
                          Genre := ID3v2Tag.Genre;
                          Composer := ID3v2Tag.Composer;
                          Copyright := ID3v2Tag.Copyright;
                          Subtitle := ID3v2Tag.SubTitle;
                          // Album artist is read from TPE2
                          AlbumArtist := ID3v2Tag.Orchestra;
                          Track := IntToStr(ID3v2Tag.Track);
                          Disc := IntToStr(ID3v2Tag.Disc)
                         end
                  else
                      // Try to read ID3v1 tags
                      begin
                        ID3v1Tag := TID3v1.Create;
                        try
                          if ID3v1Tag.ReadFromFile(WideString(InputFileName)) and
                          ID3v1Tag.Exists then
                            with TempTrack do
                               begin
                                Artist := ID3v1Tag.Artist;
                                Title := ID3v1Tag.Title;
                                Album := ID3v1Tag.Album;
                                Year := ID3v1Tag.Year;
                                Comment := ID3v1Tag.Comment;
                                Genre := ID3v1Tag.Genre;
                                Track := IntToStr(ID3v1Tag.Track)
                               end
                        finally
                          ID3v1Tag.Free
                        end
                      end
                  finally
                    ID3v2Tag.Free
                  end
                end;
       '.OGG':
                begin
                  // Try to read OGG Vorbis tags
                  OGGVorbisTag := TOggVorbis.Create;
                  try
                    if OGGVorbisTag.ReadFromFile(WideString(InputFileName)) then
                     begin
                      with TempTrack do
                         begin
                          Artist := OGGVorbisTag.Artist;
                          Title := OGGVorbisTag.Title;
                          Album := OGGVorbisTag.Album;
                          Year := OGGVorbisTag.Date;
                          Comment := OGGVorbisTag.Comment;
                          Genre := OGGVorbisTag.Genre;
                          Composer := OGGVorbisTag.Composer;
                          Copyright := OGGVorbisTag.Copyright;
                          Subtitle := OGGVorbisTag.SubTitle;
                          AlbumArtist := OGGVorbisTag.Orchestra;
                          Track := IntToStr(OGGVorbisTag.Track)
                         end
                     end
                  finally
                    OGGVorbisTag.Free
                  end
                end
  end;
  TempInt := StrToIntDef(TempTrack.Disc, 0);
  if TempInt > 0 then
    begin
      if (TempInt < 10) and (TempTrack.Disc[1] <> '0') then
        TempTrack.Disc := '0' + TempTrack.Disc
    end
  else
      TempTrack.Disc := '';
  TempInt := StrToIntDef(TempTrack.Track, 0);
  if TempInt > 0 then
    begin
      if (TempInt < 10) and (TempTrack.Track[1] <> '0') then
        TempTrack.Track := '0' + TempTrack.Track
    end
  else
      TempTrack.Disc := '';
  Result := TempTrack
end;

// Convert the length of the track(s) from QWORD into "HH:MM:SS" formatted string
// TrackStream - the stream to use
// LengthByte - the length as QWORD
function GetLengthFromByte(const TrackStream: HSTREAM; LenByte: QWORD): String;
var
   LenSec: Double;
   LH, LM, LS: LongInt;
   TempString: String[8];
begin
  Result := 'Test';
  LH := 0;
  LM := 0;
  LS := 0;
  // Get the streaming track length in seconds
  LenSec := BASS_ChannelBytes2Seconds(TrackStream, LenByte);
  // If the length in seconds could be retrieved
  if LenSec >= 0 then
     begin
       LS := Round(LenSec);
       // Get the minutes out of the seconds
       if LS >= 60 then
          begin
            LM := LS div 60;
            LS := LS mod 60
          end;
       // Get the hours out of the minutes
       if LM >= 60 then
          begin
            LH := LM div 60;
            LM := LM mod 60
          end
     end;
  // Prepare the output string
  if LH < 10 then
     TempString := '0' + IntToStr(LH) + ':'
  else
    TempString := IntToStr(LH) + ':';
  if LM < 10 then
     TempString := TempString + '0' + IntToStr(LM) + ':'
  else
    TempString := TempString + IntToStr(LM) + ':';
  if LS < 10 then
     TempString := TempString + '0' + IntToStr(LS)
  else
    TempString := TempString + IntToStr(LS);
  Result := TempString
end;

// Get the length of the track from the stream as "HH:MM:SS" formatted string
// TrackStream - the stream to get the length from
function GetTrackLength(const TrackStream: HSTREAM): String;
var
   LenByte: QWORD;
   LenSec: Double;
   LH, LM, LS: LongInt;
   TempString: String[8];
begin
  LenByte := 0;
  LenSec := 0;
  LH := 0;
  LM := 0;
  LS := 0;
  // Get the streaming track length in bytes
  LenByte := BASS_ChannelGetLength(TrackStream, BASS_POS_BYTE);
  if LenByte <> -1 then
     begin
       // Get the streaming track length in seconds
       LenSec := BASS_ChannelBytes2Seconds(TrackStream, LenByte);
       // If the length in seconds could be retrieved
       if LenSec >= 0 then
          begin
               LS := Round(LenSec);
               // Get the minutes out of the seconds
               if LS >= 60 then
                  begin
                    LM := LS div 60;
                    LS := LS mod 60
                  end;
               // Get the hours out of the minutes
               if LM >= 60 then
                  begin
                    LH := LM div 60;
                    LM := LM mod 60
                  end
          end
     end;
  // Prepare the output string
  if LH < 10 then
     TempString := '0' + IntToStr(LH) + ':'
  else
    TempString := IntToStr(LH) + ':';
  if LM < 10 then
     TempString := TempString + '0' + IntToStr(LM) + ':'
  else
    TempString := TempString + IntToStr(LM) + ':';
  if LS < 10 then
     TempString := TempString + '0' + IntToStr(LS)
  else
    TempString := TempString + IntToStr(LS);
  Result := TempString
end;

// Check, if a file is a CD track
// FileName - the name of the file
function IsCDTrack(FileName: String): Boolean;
begin
  Result := False;
  if FileName <> '' then
    if UTF8Copy(GetShortFileName(FileName), 1, 8) = 'CD Track' then
      Result := True
end;

// Check, if a file is a MIDI
// FileName - the name of the file
function IsMIDI(FileName: String): Boolean;
var
   TempString: String[5];
begin
  Result := False;
  if FileName <> '' then
    begin
      TempString := UpperCase(ExtractFileExt(GetShortFileName(FileName)));
      if (TempString = '.KAR') or (TempString = '.MID') or
      (TempString = '.MIDI') or (TempString = '.RMI') then
        Result := True
    end
end;

// Check, if a file is an URL
// FileName - the name of the file
function IsURL(FileName: String): Boolean;
begin
  Result := False;
  if FileName <> '' then
    if (UTF8Copy(FileName, 1, 6) = 'ftp://') or
    (UTF8Copy(FileName, 1, 7) = 'http://') or
    (UTF8Copy(FileName, 1, 8) = 'https://') then
      Result := True
end;

// Resizes the cover image so that it fits into the form
// Image - an image to resize
// Form - a form to fit the image into
function ResizeCoverImage(const Image: TImage; const Form: TForm): TImage;
var
   AspectRatio: Single;
   HeightDif, HeightRes, WidthDif, WidthRes: LongInt;
   TempBitmap: TBitmap;
begin
  Result := Image;
  AspectRatio := Image.Picture.Width/Image.Picture.Height;
  HeightDif := Image.Picture.Height - Form.Height;
  WidthDif := Image.Picture.Width - Form.Width;
  if HeightDif > WidthDif then
     begin
       HeightRes := Form.Height;
       WidthRes := Round(HeightRes*AspectRatio)
     end
  else
      begin
        WidthRes := Form.Width;
        HeightRes := Round(WidthRes/AspectRatio)
      end;
  Result.Height := HeightRes;
  Result.Width := WidthRes;
  TempBitmap := TBitmap.Create;
  try
    TempBitmap.SetSize(WidthRes, HeightRes);
    StretchBlt(TempBitmap.Canvas.Handle, 0, 0, WidthRes, HeightRes,
    Image.Picture.Bitmap.Canvas.Handle, 0, 0, Image.Picture.Width,
    Image.Picture.Height, 0);
    Result.Picture.Bitmap.Assign(TempBitmap)
  finally
    TempBitmap.Free
  end
end;

// Add CD to the playlist
// PlaylistFileName - the name of the playlist file
// TrackQuantity - the total quantity of tracks
procedure AddCDToPlaylist(const PlaylistFileName: String;
  TrackQuantity: Byte);
const
  CDString = 'CD';
var
   Iterator: Byte;
   PlaylistFile: TFileStream;
begin
  PlaylistFile :=
  TFileStream.Create(PlaylistFileName, fmOpenWrite);
  try
    for Iterator := 0 to TrackQuantity - 1 do
      begin
        PlaylistFile.Position := PlaylistFile.Size;
        Application.ProcessMessages;
        PlaylistFile.WriteAnsiString(CDString + IntToStr(Iterator))
      end
  finally
    PlaylistFile.Free
  end
end;

// Add a file to the specified playlist position
// FileName - the name of the file to be added to the playlist
// PlaylistFileName - the name of the playlist file
// Position - the position in the playlist file for the file to be added in
procedure AddFileToPlaylistPosition(const FileName: String;
  PlaylistFileName: String; Position: Integer);
var
  Count: Integer;
  PlaylistFile: TFileStream;
  TempStream: TMemoryStream;
  TempString: String;
begin
  Count := 0;
  if FileExists(PlaylistFileName) then
    begin
      PlaylistFile := TFileStream.Create(PlaylistFileName, fmOpenReadWrite);
      try
        if PlaylistFile.Size > 0 then
        begin
          TempStream := TMemoryStream.Create;
          try
            while PlaylistFile.Position < PlaylistFile.Size do
              begin
                Application.ProcessMessages;
                TempString := PlaylistFile.ReadAnsiString;
                if Count <> Position then
                  TempStream.WriteAnsiString(TempString)
                else
                  begin
                    TempStream.WriteAnsiString(FileName);
                    TempStream.WriteAnsiString(TempString)
                  end;
                Inc(Count)
              end;
            if Position >= Count then
              TempStream.WriteAnsiString(FileName);
            PlaylistFile.Free;
            PlaylistFile := TFileStream.Create(PlaylistFileName, fmCreate);
            PlaylistFile.CopyFrom(TempStream, 0);
          finally
            TempStream.Free
          end
        end
      finally
        PlaylistFile.Free
      end
    end
end;

// Add files to the playlist
// FileList - the list of file names to be added to the playlist file
// PlaylistFileName - the name of the playlist file
procedure AddFilesToPlaylist(const FileList: TStrings;
  PlaylistFileName: String);
var
  Iterator: Integer;
  PlaylistFile: TFileStream;
begin
  PlaylistFile :=
  TFileStream.Create(PlaylistFileName, fmOpenReadWrite);
  try
    PlaylistFile.Position := PlaylistFile.Size;
    // Add all the selected files to the playlist
    for Iterator := 0 to FileList.Count - 1 do
     begin
       Application.ProcessMessages;
       PlaylistFile.WriteAnsiString(FileList[Iterator])
     end
  finally
    PlaylistFile.Free
  end
end;

// Add folders to the collection
// FolderList - the list of folder names to be added to the collection file
// CollectionFileName - the name of the collection file
procedure AddFoldersToCollection(const FolderList: TStrings;
  CollectionFileName: String);
var
  CollectionFile: TFileStream;
  Iterator: Integer;
begin
  if FileExists(CollectionFileName) then
    CollectionFile := TFileStream.Create(CollectionFileName, fmOpenWrite)
  else
    CollectionFile := TFileStream.Create(CollectionFileName, fmCreate);
  try
    if CollectionFile.Size > 0 then
      CollectionFile.Position := CollectionFile.Seek(0, soFromEnd);
    for Iterator := 0 to FolderList.Count - 1 do
      begin
        Application.ProcessMessages;
        CollectionFile.WriteAnsiString(FolderList[Iterator])
      end
  finally
    CollectionFile.Free
  end
end;

// Add URL to the playlist
// PlaylistFileName - the name of the playlist file
procedure AddURLToPlaylist(const URL, PlaylistFileName: String);
var
   PlaylistFile: TFileStream;
begin
  PlaylistFile :=
  TFileStream.Create(PlaylistFileName, fmOpenWrite);
  try
    PlaylistFile.Position := PlaylistFile.Size;
    Application.ProcessMessages;
    PlaylistFile.WriteAnsiString(URL)
  finally
    PlaylistFile.Free
  end
end;

// Remove a file from the playlist
// PlaylistFileName - the name of the playlist file
// FilePos - the position of a file to remove in the playlist file
procedure RemoveFileFromPlaylist(const PlaylistFileName: String;
  FilePos: Integer);
var
  Count: Integer;
  PlaylistFile: TFileStream;
  TempStream: TMemoryStream;
  TempString: String;
begin
  Count := 0;
  if FileExists(PlaylistFileName) then
    begin
      PlaylistFile := TFileStream.Create(PlaylistFileName, fmOpenReadWrite);
      try
        if PlaylistFile.Size > 0 then
          begin
            TempStream := TMemoryStream.Create;
            try
              while PlaylistFile.Position < PlaylistFile.Size do
                begin
                  Application.ProcessMessages;
                  TempString := PlaylistFile.ReadAnsiString;
                  if Count <> FilePos then
                    TempStream.WriteAnsiString(TempString);
                  Inc(Count)
                end;
              PlaylistFile.Free;
              PlaylistFile := TFileStream.Create(PlaylistFileName, fmCreate);
              PlaylistFile.CopyFrom(TempStream, 0);
            finally
              TempStream.Free
            end
        end
      finally
        PlaylistFile.Free
      end
    end
end;

// Removes ID3v1 tag from audio file
// FileName - the name of the file
procedure RemoveID3v1FromFile(const FileName: String);
var
  ID3v1Tag: TID3v1;
begin
  if FileExists(FileName) then
     begin
       ID3v1Tag := TID3v1.Create;
       try
         ID3v1Tag.RemoveFromFile(WideString(FileName))
       finally
         ID3v1Tag.Free
       end
     end
end;

// Removes ID3v2 tag from audio file
// FileName - the name of the file
procedure RemoveID3v2FromFile(const FileName: String);
var
  ID3v2Tag: TID3v2;
begin
  if FileExists(FileName) then
     begin
       ID3v2Tag := TID3v2.Create;
       try
         ID3v2Tag.RemoveFromFile(FileName)
       finally
         ID3v2Tag.Free
       end
     end
end;

end.

